function Z(){ /*# Zeroscript v4.10 'Ryouko' #*/
	/**
	 * Story version:
	 * 'Myata'  (v0-2)  => Created Zeroscript
	 * 'Karin'  (v2.65) => Adapted for Instant Kusaba fork (Nullchan.cc)
	 * 'Hitagi' (v3.14) => Adapted for Sakaba (Ni-chan)
	 * 'Ryouko' (v4.10) => Adapted for Katana X (Nullnyan)
	 * 'Eiren'  (v5.00) => Adapted for Katana X (Noosphere project)
	 */
	'use strict';
    var launch=true,
        debug=true,
        version=5.0, version_name='',
        root={
            name:'zero',
            link:null
        };
	var DOM={
		env:{wait:'z__wait',msg:'z__message'},
		mmi:{img:'z__image',aud:'z__audio',vid:'z__video',fla:'z__flash'},
		cia:{fpc:'z__relative'},
		atf:{}
	};
    var Data={
		Tag:{
			GEN:'[GEN]\t',
			MMI:'[MMI]\t',
			CIA:'[CIA]\t',
			ATF:'[ATF]\t',
			AJAX:'[AJAX]\t',
			IO:'[IO]\t'
		},
        Error:{
            Unknown:	'Unknown error occured',
            Network:	'Network error occured',
            Browser:	'This browser is not currently supported',
            Support:	'This filetype is not currently supported',
            Dimensions:	'Error fetching source image dimensions',
            Transfer:	'Error accessing event dataTransfer',
			Captcha:	'Captcha code required',
			File:		'The file is not supported',
            Send:		'Nothing to send',
            Load:		'Nothing to load',
		},
		Service:{
			YouTube:{
				link:	'https://youtube.com/embed/',
				args:	'?autoplay=1'
			},
			Coub:{
				link:	'https://coub.com/embed/',
				args:	'?muted=false&autostart=true&originalSize=false&hideTopBar=true&noSiteButtons=true&startWithHD=false'
			},
			Vimeo:{
				link:	'https://player.vimeo.com/video/',
				args:	'?badge=0'
			},
			SoundCloud:{
				link:	'https://w.soundcloud.com/player/?url=',
				args:	'&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&visual=true'
			}
		},
		Icon:{
			general: '<i class="icon icon-info"></i>',
			success: '<i class="icon icon-ok"></i>',
			warning: '<i class="icon icon-warning"></i>',
			failure: '<i class="icon icon-error"></i>',
			cancel:  '<i class="icon icon-cancel"></i>'
		},
		Message:{
			Type:{
				general: 0,
				success: 1,
				warning: 2,
				failure: 3
			},
			OK:			"OK",
			Sending:	"Sending...",
			Loading:	"Loading..."
		}
    };
	var MIME={
		invalid:0,
		image:1,
		audio:2,
		video:3,
		flash:4,
		text:5,
		source:6,
		archive:7,
		torrent:8,
		key:9,
		binary:10,
		external:128
	};
	var Environment={
		waiting:function(e,t){ DOM.env.wait.innerHTML=t; if(e) DOM.env.wait.show(); else DOM.env.wait.hide(); },
		loading:function(e){ return waiting(e, Data.Error.Load); },
		pending:function(e){ return waiting(e, Data.Error.Send); },
		message:function(t,k){
			DOM.env.msg.innerHTML=Data.Icon[k]+t;
			DOM.env.msg.style.display='block';
			setTimeout(function(){ DOM.env.msg.style.display='none'; },5000);
		},
		measure:function(callback){
			console.time('perf');
			callback();
			console.timeEnd('perf');
		}
	};
	function say(msg){ console.info('# '+msg); return false; }
	function log(msg){ return say('[note] '+msg); }
	function err(msg){ return say('[error] '+msg); }
	function die(msg){ return say('[failed] '+msg); }

	/*####################################################################################################################################################*/
    var Type={
        Media:function(link){
			this.context={
				link:link,
				type:Z.MMI.type(link.href),
				look:link.querySelector('img'),
				view:null,
				lock:false,
				zoom:1,
				stop:null
			};
			this.context.view=Factory.assemble(this);
			if(this.context.look) this.context.look.classList.add('preview');
			this.wait=function(state){
				if(state) link.appendChild(_.create('r'));
				else {
					var r=link.querySelector('r');
					if(null!==r){
						setTimeout(function(){ link.removeChild(r); },200);
					}
				}
			};
			this.size={
				unlock:function(){
					var img=link.querySelector('img'),div=link.parentNode,dd=div.parentNode;
					link.style.maxWidth
						=link.style.maxHeight
						=div.style.maxWidth
						=div.style.maxHeight
						=img.style.maxWidth
						=img.style.maxHeight
						='none';
					dd.style.maxHeight='none';
					//link.classList.add('open');
				},
				lock:function(){
					var img=link.querySelector('img'),div=link.parentNode,dd=div.parentNode;
					link.style.maxWidth
						=link.style.maxHeight
						=div.style.maxWidth
						=div.style.maxHeight
						=img.style.maxWidth
						=img.style.maxHeight
						=_.opt.server["prevsize"]+'px';
					dd.style.maxHeight='75vh';
					//link.classList.remove('open');
				}
			};
			this.dim=function(state){ if(state) link.style.opacity=0.7; else link.style.opacity=1; };
			this.open=Factory.open(this);
			this.close=Factory.close(this);
			this.context.stop=_.create('button');
			this.context.stop.classList.add('svc');
			this.context.stop.innerHTML=Data.Icon.cancel;
			this.context.stop.addEventListener('click',function(event){ link.click(); },false);
			this.destroy=function(){ delete this; };
		},
		Factory:function(){
			this.assemble=function(media){
				switch(media.context.type){
					case MIME.image:
						var image=_.create('img');
						image.setAttribute('draghandler', true);
						image.alt='';
						return image;
					case MIME.audio://@todo::constant player in the bottom
						var audio=_.create("audio"),
							format='audio/'+_.string.extension(media.context.link.href);
						audio.onerror=function(event){ _.remove(this); return false; };
						audio.autoplay=true;
						audio.controls=false;
						audio.loop=false;
						return audio;
					case MIME.video:
						var video=_.create("video");
						video.onerror=function(event){ _.remove(this); return false; };
						video.autoplay=true;
						video.controls=true;
						video.loop=true;
						video.poster=media.context.look.src;
						return video;
					case MIME.flash:
						var flash=_.create('object');
						flash.onerror=function(event){ _.remove(this); return false; };
						flash.setAttribute('type','application/x-shockwave-flash');
						var parameters=_.create('param');
						parameters.setAttribute('wmode','transparent');
						flash.appendChild(parameters);
						var container=_.create('div');
						container.appendChild(flash);
						var close=_.create('i');
						close.setAttribute('close',true);
						close.classList.add('icon-cancel');
						close.addEventListener('mouseup',function(event){ if(event.which==1) media.close(event); },false);
						container.appendChild(close);
						return container;
					case MIME.text: case MIME.source: case MIME.key:
						var paste=_.create('pre');
						_.ajax.perform(media.context.link.href,'GET',null,
							function(result){ paste.innerHTML=result; },
							function(status,error){ paste.innerHTML=status+"\n\n"+error; },
							null
						);
						return paste;
					case MIME.external:
						var frame=_.create("iframe");
						frame.width=640; frame.height=480;
						frame.allowfullscreen=true;
						frame.frameborder=0;
						frame.src=media.context.link.href;
						if(/youtu/i.exec(media.context.link.href)){
							frame.width=854;//_.w*0.75-60;//fullsize, yoba-kinoteatr
							frame.height=480;//frame.width*0.75;//0,5625 for 16:9
							frame.src=Data.Service.YouTube.link
								+/(youtu(?:\.be|be\.com)\/(?:.*v(?:\/|=)|(?:.*\/)?)([\w'-]+))/i.exec(media.context.link.href)[2]
								+Data.Service.YouTube.args;
						}
						if(/coub/i.exec(media.context.link.href)){
							frame.width=640;
							frame.height=480;
							frame.src=Data.Service.Coub.link
								+/(http|https)?:\/\/(www\.)?coub\.com\/view\/([a-zA-Z\d]+)/.exec(media.context.link.href)[3]
								+Data.Service.Coub.args;
						}
						if(/vimeo/i.exec(media.context.link.href)){
							frame.width=480;
							frame.height=320;
							frame.src=Data.Service.Vimeo.link
								+/(?:www\.|player\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/(?:[^\/]*)\/videos\/|album\/(?:\d+)\/video\/|video\/|)(\d+)(?:[a-zA-Z0-9_\-]+)?/i.exec(media.context.link.href)[1]
								+Data.Service.Vimeo.args;
						}
						if(/soundcloud/i.exec(media.context.link.href)){
							frame.width=300;
							frame.height=300;
							frame.src=Data.Service.SoundCloud.link+media.context.link.href+Data.Service.SoundCloud.args;
						}
						return frame;
					default:break;
				}
				return false;
			};
			this.open=function(media){
				switch(media.context.type){
					case MIME.image: return function(event){
						event.preventDefault();
						var context=this.context;
						if(context.lock) return false;
						context.lock=true;
						media.dim(true);
						media.wait(true);
						context.view.addEventListener('load',function(event){
							context.lock=false;
							media.dim(false);
							media.wait(false);
							media.size.unlock();
							if(_.opt.client.play && !event.ctrlKey){
								context.view.style.position='fixed';
								context.view.setAttribute('draghandler',true);
								Z.MMI.aspect(context.view,false);
								Z.MMI.center(context.view);
								context.view.addEventListener('mousedown',function(event){
									DOM.drag.e=context.view;
									DOM.drag.kill=media.close;
									DOM.drag.it(event);
									return false;
								},false);
								Z.MMI.wheel(context);
								DOM.mmi.img.appendChild(context.view);
							} else {
								Z.MMI.aspect(this,true);
								context.look.hide();
								context.link.appendChild(context.view);
							}
						},false);
						context.view.src=context.link.href;
					}; break;
					case MIME.audio: return function(event){
						event.preventDefault();
						DOM.mmi.aud.classList.add('play');
						var context=this.context;
						if(context.lock) return false;
						context.lock=true;
						var format='audio/'+_.string.extension(context.link.href);
						if(audio.canPlayType(format).length>0){
							audio.src=media.context.link.href;
							audio.type=format;
							audio.autoPlay=true;
						} else return Environment.message(Data.Error.Support, Data.Message.Type.Error);
					}; break;
					case MIME.video: return function(event){
						event.preventDefault();
						var context=this.context;
						if(context.lock) return false;
						context.lock=true;
						var format='video/'+_.string.extension(context.link.href);
						if(context.view.canPlayType(format).length>0){
							context.view.type=format;
							context.view.autoPlay=true;
						} else return Environment.message(Data.Error.Support, Data.Message.Type.Error);
						media.dim(true);
						media.wait(true);
						context.view.addEventListener('loadedmetadata',function(event){
							context.lock=false;
							media.dim(false);
							media.wait(false);
							media.size.unlock();
							context.view.width=context.view.videoWidth;
							context.view.height=context.view.videoHeight;
							if(_.opt.client.play && !event.ctrlKey){
								this.style.position='fixed';
								Z.MMI.aspect(this,false);
								Z.MMI.center(context.view);
								context.view.addEventListener('mousedown',function(event){
									if(event.which != 1) return false;
									DOM.drag.e=context.view;
									DOM.drag.kill=media.close;
									DOM.drag.it(event);
									return false;
								},false);
								Z.MMI.wheel(context);
								DOM.mmi.vid.appendChild(context.view);
							} else {
								Z.MMI.aspect(this,true);
								context.look.hide();
								context.link.appendChild(context.view);
							}
						},false);
						context.view.src=context.link.href;
					}; break;
					case MIME.flash: return function(event){
						event.preventDefault();
						var context=this.context;
						var object=context.view.querySelector('object');
						if(context.lock) return false;
						var dimensions=/(\d+)x(\d+)/i.exec(context.link.previousSibling.innerHTML);
						if(null===dimensions){
							object.width=640;
							object.height=480;
						}
						else {
							object.width=dimensions[1];
							object.height=dimensions[2];
						}
						context.view.width=context.view.style.width=object.width;
						context.view.height=context.view.style.height=object.height;
						//context.view.addEventListener('load',function(event){
							if(_.opt.client.play && !event.ctrlKey){
								context.view.style.position='fixed';
								Z.MMI.aspect(object,false);
								Z.MMI.center(context.view);
								Z.MMI.wheel(context);
								DOM.mmi.fla.appendChild(context.view);
							} else {
								Z.MMI.aspect(context.view,true);
								context.look.hide();
								context.link.appendChild(context.view);
							}
						//},false);
						object.setAttribute('data',context.link.href);
					}; break;
					case MIME.text: case MIME.source: case MIME.key:
						return function(event){};
						break;
					case MIME.external: return function(event){
						event.preventDefault();
						var context=this.context;
						if(context.lock) return false;
						context.lock=true;
						media.size.unlock();
						if(_.opt.client.play && !event.ctrlKey){
							// create close button
							DOM.MMI.svc.appendChild(context.view);
						} else {
							context.look.hide();
							context.link.appendChild(context.view);
							context.link.classList.remove('atc-svc');
							var info=context.link.parentNode.parentNode.firstChild;
							info.insertBefore(context.stop,info.firstChild);
						}
					}; break;
					default:break;
				}
				return false;
			};
			this.close=function(media){
				switch(media.context.type){
					case MIME.image: return function(event){
						event.preventDefault();
						media.size.lock();
						if(_.opt.client.play){
							_.remove(media.context.view);
							pool.rem(media.context.link);
							media.destroy();
						} else {
							_.remove(media.context.view);
							media.context.view=null;
							media.context.look.show();
						}
					};
					case MIME.audio: break;
					case MIME.video: return function(event){
						event.preventDefault();
						var context=this.context;
						media.size.lock();
						if(_.opt.client.play && !event.ctrlKey){
							_.remove(media.context.view);
							pool.rem(media.context.link);
							media.destroy();
						} else {
							_.remove(media.context.view);
							media.context.view=null;
							media.context.look.show();
						}
					}; break;
					case MIME.flash: return function(event){
						event.preventDefault();
						var context=this.context;
						if(_.opt.client.play && !event.ctrlKey){
							_.remove(media.context.view);
							pool.rem(media.context.link);
							media.destroy();
						} else {
							_.remove(media.context.view);
							media.context.view=null;
							media.context.look.show();
						}
					}; break;
					case MIME.text: case MIME.source: break;
					case MIME.external: return function(event){
						event.preventDefault();
						var context=this.context;
						media.size.lock();
						if(_.opt.client.play && !event.ctrlKey){
							DOM.mmi.svc.removeChild(context.view);
						} else {
							_.remove(context.view);
							context.view=null;
							context.link.classList.add('atc-svc');
							context.look.show();
							context.link.parentNode.parentNode.firstChild.removeChild(context.stop);
						}
					}; break;
					default:break;
				}
				return false;
			};
		},
        Pool:function(){
            this.set={};
            this.sid=function(href){ return _.string.pseudo(href); };
            this.chk=function(link){ return (undefined!==this.set[this.sid(link.href)]); };
            this.ptr=function(link){ if(!this.chk(link)) return false; return this.set[this.sid(link.href)]; };
            this.add=function(link,instance=true){ if(!this.chk(link)) this.set[this.sid(link.href)]=instance; };
            this.rem=function(link){ if(this.chk(link)) delete this.set[this.sid(link.href)]; };
            this.ram=function(){ console.info('Господин, вы много себе позволяете. (//_0)'); };
            this.clr=function(){ for(var k in this.set) delete this.set[k]; };
        }
	};
	var Module={
		GEN:null,
		MMI:null,
		CIA:null,
		ATF:null
	};
	var Factory=null;
	/*####################################################################################################################################################*/
	Module.GEN=function(){
		root.link=_.define(root.name);
		root.link.style.position='absolute';
		root.link.style.top='0px';root.link.style.left='0px';
		for(var Interface in DOM){ for(var Viewer in DOM[Interface]){ DOM[Interface][Viewer]=_.define(DOM[Interface][Viewer],root.name); } }

		Factory=new Type.Factory();
		
		DOM.drag={ix:null,iy:null,cx:null,cy:null,e:null,end:null,kill:null,moved:true,it:function(event){
			//if(/^(textarea|label|input|button|i|object)$/i.test(event.target.nodeName)){return;}
			if(event.which!=1){return;}
			switch(event.type){
				case 'mousedown':
					if(!event.target.hasAttribute('draghandler')){return;}
					event.preventDefault();
                    DOM.drag.e.style.position="absolute";
                    DOM.drag.moved=false;
					document.body.style.pointerEvents="none";
					DOM.drag.ix=event.clientX;
					DOM.drag.iy=event.clientY;
					document.addEventListener('mousemove',DOM.drag.it);
					document.addEventListener('mouseup',DOM.drag.it);
					document.body.style.cursor='move';
					return;
                case 'mousemove':
                    document.body.style.pointerEvents="none";
					DOM.drag.moved=true;
					DOM.drag.cx=event.clientX;
					DOM.drag.cy=event.clientY;
					DOM.drag.e.style.opacity="0.75";
					if((DOM.drag.cx!==DOM.drag.ix)||(DOM.drag.cy!==DOM.drag.iy)){
						DOM.drag.e.style.left=DOM.drag.e.offsetLeft+DOM.drag.cx-DOM.drag.ix+'px';
						DOM.drag.e.style.top=DOM.drag.e.offsetTop+DOM.drag.cy-DOM.drag.iy+'px';
						DOM.drag.ix=DOM.drag.cx;
						DOM.drag.iy=DOM.drag.cy;
					}
					return;
				case 'mouseup':
                    document.body.style.pointerEvents="auto";
					if(!DOM.drag.moved){
						if(typeof DOM.drag.kill == "function"){
							DOM.drag.kill(event);
						}
					}
					DOM.drag.moved=false;
					document.removeEventListener('mousemove',DOM.drag.it);
					document.removeEventListener('mouseup',DOM.drag.it);
					document.body.style.cursor='initial';
					DOM.drag.e.style.opacity="1";
					document.body.style.pointerEvents="auto";
					if(typeof DOM.drag.end == "function"){
						DOM.drag.end(event);
					}
					return;
				default:break;
			}
		}};

		DOM.env.msg.style.display='none';
		DOM.env.msg.onclick=function(){ DOM.env.msg.style.display='none'; };
		Z.MMI=new Module.MMI();
		Z.CIA=new Module.CIA();
		Z.ATF=new Module.ATF();
		if(debug) say(Data.Tag.GEN+Data.Message.OK);

		DOM.scroll=0;
		document.addEventListener('scroll',function(event){
			clearTimeout(DOM.scroll);
			document.body.style.pointerEvents="none";
			DOM.scroll=setTimeout(function(){document.body.style.pointerEvents="auto";},359);
		},false);
	};
	/*####################################################################################################################################################*/
	Module.MMI=function(){ /*# Media Manipulation Interface #*/
		var Pool=new Type.Pool();
		
		this.start=(function(){
			if(debug) say(Data.Tag.MMI+Data.Message.OK);
		})();

		this.init=function(area){
			_.listen('.file__preview > a','click',this.handler);
			//_.listen('.link__preview > a','click',this.handler);
		};
		this.handler=function(event){
			if(event.which===2 || event.which===3) return true;
			if(Pool.chk(this)){
				var port=Pool.ptr(this);
				port.close(event);
				port.destroy();
				Pool.rem(this);
			} else {
				var port=new Type.Media(this);
				Pool.add(this,port);
				Pool.ptr(this).open(event);
			}
			return false;
		};
		this.type=function(url){
			if(url===undefined) return MIME.invalid;
			var extension=/\.(\w+)(?:$|\?.*)/gi.exec(url),type=MIME.invalid;
			if(extension!==null) switch(extension[1].toLowerCase()){
				case 'jpg': case 'jpeg': case 'png': case 'gif': case 'apng': case 'webp':
					type=MIME.image; break;
				case 'mp3': case 'ogg': case 'aac': case 'flac': case 'wav': case 'weba':
				case 's3m': case 'mod': case 'it': case 'xm': case 'ptm': case 'ftm': case 'mid': case 'midi':
					type=MIME.audio; break;
				case 'webm': case 'mp4': case 'ogv':
					type=MIME.video; break;
				case 'swf': case 'flv':
					type=MIME.flash; break;
				case 'txt': case 'log': case 'nfo': case 'lst': case 'src':
					type=MIME.text; break;
				case 'c': case 'h': case 'cpp': case 'hpp': case 'cs': case 's': case 'i':
				case 'java': case 'pl': case 'asm': case 'ml': case 'tex':
				case 'py': case 'php': case 'rb': case 'js': case 'sh':
					type=MIME.source; break;
				case 'rar': case 'zip': case '7z':
				case 'tar': case 'gz': case 'bz2': 
					type=MIME.archive; break;
				case 'torrent':
					type=MIME.torrent; break;
				case 'asc': case 'sig': case 'crt':
					type=MIME.key; break;
				case 'exe': case 'bin':
					type=MIME.binary; break;
				default:break;
			} else {
				var external=/(youtu|vimeo|coub|soundcloud)/gi.exec(url);
				if(external) type=MIME.external;
			}
			return type;
		};
		this.wheel=function(context){
			if(context.view.addEventListener){
				if('onwheel' in document) // IE9+, FF17+
					context.view.addEventListener('wheel',function(event){ Z.MMI.resize(event,context); },false);
				else if('onmousewheel' in document) // устаревший вариант события
					context.view.addEventListener('mousewheel',function(event){ Z.MMI.resize(event,context); },false);
				else // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
					context.view.addEventListener('MozMousePixelScroll',function(event){ Z.MMI.resize(event,context); },false);
			} else // IE8-
				context.view.attachEvent('onmousewheel',function(event){ Z.MMI.resize(event,context); },false);
		};
		this.resize=function(event,context){
			event.preventDefault();
			var delta=event.deltaY||event.detail||event.wheelDelta;
			if(context.zoom<0.1) context.zoom=0.1;
			if(context.zoom>5) context.zoom=5;
			if(context.zoom>=0.1 && context.zoom<=5){
				if(delta>0) context.zoom-=0.1;
				else context.zoom+=0.1;
			}
			context.view.style.transform
				=context.view.style.WebkitTransform
				=context.view.style.MsTransform
				='scale('+context.zoom+')';
		};
		this.aspect=function(element,limit=true){
			var w=element.width, h=element.height,
				//mw=_.w, mh=_.h,
				mw=_.selector('body > main')[0].offsetWidth,
				mh=_.h-40,
				aspect=h/w, real=mh/mw;
			if(limit){ mw=_.w*0.75-60; real=mh/mw; }//поправка
			if((w>mw)||(h>mh)){ if(aspect>=real){ h=mh; w=h/aspect|0; } else { w=mw; h=w*aspect; } }
			element.width=w; element.height=h;
			//return {w:w,h:h,a:aspect,r:real};
		};
		this.center=function(e){
			e.style.left=0;
			if(e.width < _.w) e.style.left=(Math.floor(_.w/2-(e.width/2)))+"px";
			else e.style.left="0px";
			e.style.top=20;
			if(e.height < _.h-40) e.style.top=(Math.floor((_.h-40)/2-(e.height/2)))+20+"px";
			else e.style.top="20px";
		};
		this.loading=function(e,s){ if(s) e.classList.add('loading'); else e.classList.remove('loading'); };
	};
	/*####################################################################################################################################################*/
	Module.CIA=function(){ /*# Content Interactivity #*/
		this.start=(function(){
			_.listen('[draggable]','mousedown',function(event){ DOM.drag.e=this; DOM.drag.it(event); return false; });
			_.listen('[data-answer]','click',function(event){ Z.CIA.answer(event,this); });
			//_.listen('[data-post-close]','click',function(){ Z.CIA.close(this); });
			//_.listen('[data-post-open]','click',function(){ Z.CIA.open(this); });
			//_.listen('[data-thread-update]','click',function(event){ Z.CIA.update(event); });
			//_.listen('[data-thread-expand]','click',function(event){ Z.CIA.expand(); });
			if(debug) say(Data.Tag.CIA+Data.Message.OK);
		})();
		this.init=function(area){
			_.listen('.ref','mouseover',function(event){ Z.CIA.look(event,this); });
		};
		this.update=function(event){
			event.preventDefault();
			if(event.which!==1) return;
			log(Data.Tag.CIA+'test get new posts function callback');
		};
		this.expand=function(event){
			event.preventDefault();
			if(event.which!==1) return;
			log(Data.Tag.CIA+'test thread expand function callback');
		};
		this.seek=function(event){
			event.preventDefault();
			if(event.which!==1) return;
			log(Data.Tag.CIA+'test thread seek function callback');
		};
		this.answer=function(event,element){
			event.preventDefault();
			if(!DOM.atf.form) return false;
			var area=DOM.atf.form.text,
				wire=element.parentNode.parentNode,
				post=element.parentNode.parentNode;
			post=post.parentNode.dataset.post|post.dataset.post;
			wire=wire.parentNode.dataset.wire|wire.dataset.wire;
			DOM.atf.form.rl.value=wire;
			if(post != wire) area.value+=">>"+post+"\n";
			Z.ATF.counter(event);
			Z.ATF.float(1);
			area.focus();
		};
		this.close=function(e){ return false; };
		this.open=function(e){ return false; };
		this.look=function(event,context){
			if(null===context.rel){ console.error("No relative post bound was found, skipping"); }
			return false;
			_.ajax.perform(
				'/'+location.pathname.split('/')[1]+'/read/'+context.rel,
				'GET',
				null,
				function(psto){
					var container=_.create('div');
					container.innerHTML=psto;
					container.style.position="absolute";
					container.style.top=context.offsetTop+19+'px';
					container.style.left=context.offsetLeft+'px';
					//chainload(container);
					//DOM.cia.fpc.appendChild(container);
					var dd=context.parentElement.parentElement,
						posmatch=dd.querySelector(container.querySelector('article').id);
					if(posmatch!==null){
						_.remove(posmatch);
					}
					dd.appendChild(container);
					var T,cursor,
						mouseleave=function(event){ T=setTimeout(function(){ timeout(event); } ,500); },
						timeout=function(event){
							cursor=document.elementFromPoint(event.clientX, event.clientY);
							if(container.contains(cursor) || cursor==container || context.contains(cursor) || cursor==context){
								clearTimeout(T);
							}
							else _.remove(container);
						};
					_.once(context,'mouseleave',mouseleave);
					_.once(container,'mouseleave',mouseleave);
				},
				function(error,xhr){ console.error(error,xhr); },
				{'X-AJAX-Perform':true}
			);
		};
	};
	/*####################################################################################################################################################*/
	Module.ATF=function(){ /*# Fast Form Interface #*/
		var Dyad=function(file){
			this.file=file;
			this.node=null;
			this.prev=null;
			this.hash=_.string.pseudo(file.name);
		};
        var Record={
        	setup:function(id){
        		DOM.atf.file.host=_.id(id);
        		if(!_.exists(DOM.atf.file.host)){return;}
				_.clear(DOM.atf.file.host);
				DOM.atf.file.host.style.position="absolute";
				DOM.atf.file.host.style.visibility="hidden";
				DOM.atf.file.host.style.opacity=0;
				DOM.atf.file.prev=_.id("postform__file-preview");
				DOM.atf.file.fake=DOM.atf.form["filefake"];
				DOM.atf.file.fake.addEventListener('click',Record.add,false);
        	},
        	create:function(){
        		var num=Object.keys(DOM.atf.file.collection).length;
				if(num>=_.opt.server['maxfiles']){return null;}
				var file=_.create('input');
					file.type='file';
					file.name='files[]';
					file.id='postform__file_input'+num;
				file.addEventListener('change',function(event){ Record.change(event); },false);
				return file;
        	},
        	change:function(event){
        		var file=event.target.files[0];
        		if(!_.exists(file)){return;}
        		var dyad=new Dyad(file);
        		dyad.node=_.id('postform__file_input'+Object.keys(DOM.atf.file.collection).length);
				dyad.prev=Record.process(event,file);
				dyad.prev.id='p'+dyad.hash;
				dyad.prev.classList.add('postform__file-preview');
				DOM.atf.file.collection[dyad.hash]=dyad;
				DOM.atf.file.prev.appendChild(dyad.prev);
        	},
            add:function(event){
                event.preventDefault();
				var node=Record.create();
				if(_.exists(node)){
					DOM.atf.file.host.appendChild(node);
					node.click();
				}
			},
			del:function(file){
				var dyad=DOM.atf.file.collection[_.string.pseudo(file.name)];
				if(_.exists(dyad)){
					dyad.prev.remove();
					dyad.node.remove();
					delete DOM.atf.file.collection[_.string.pseudo(file.name)];
				}
			},
			check:function(file){ return file && (Z.MMI.type(file.name) > 0); },
            clear:function(event, file){
                if(file.value){
                    try { file.value=''; } catch(e){}
                    if(file.value){
                        var form=_.create('form'),
                            parentNode=file.parentNode,
                            ref=file.nextSibling;
                        form.appendChild(file);
                        form.reset();
                        parentNode.insertBefore(file, ref);
                    }
                }
                return false;
            },
            droppable:function(event){ return ((('draggable' in event)||('ondragstart' in event && 'ondrop' in event))&&('FormData' in window)&&('FileReader' in window)); },
            drop:function(event, file){
                if(!Record.droppable(event)){ return err(Data.Tag.ATF + Data.Error.Browser); }
                if(!event.dataTransfer){ return err(Data.Tag.ATF + Data.Error.Transfer); }
                Record.add(file);
                return false;
            },
            drag:function(event){
                if(!Record.valid(event)){ return false; }
                // todo
                return false;
            },
            valid:function(event){
                if(!event.dataTransfer){ return err(Data.Tag.ATF + Data.Error.Transfer); }
                var match = false;
                for(var item in event.dataTransfer.types){
                    if(/file/i.test(item)) return true;
                }
                return false;
            },
            preview:function(event,file){
            	var close=_.create('i'),
					container=_.create('output'),
					img=null;
                if((Z.MMI.type(file.name) == MIME.image) && ('FileReader' in window)){
					var reader=new FileReader();
					img=_.create('img');
					img.style.display='none';
					img.classList.add('img','postform__img-preview');
					reader.addEventListener('load',function(event){
						img.src=reader.result;
						setTimeout(function(){
							img.style.display='block';
						},265);
					},false);
					reader.readAsDataURL(file);
					
                    /*
                    	<button id="postform__save-name" class="button postform__label postform__label_wide postform__save-name">
							<input type="checkbox" class="checkbox" name="original-name" checked />
							<i class="icon icon-check highlight"></i><i class="icon icon-check-in highlight"></i>Save file name
						</button>
                    */
                } else {
                	img=Record.icon(Z.MMI.type(file.name));
                }
                close.classList.add('icon','icon-cancel','postform__remove-file','overlay');
				close.addEventListener('mouseup',function(event){ if(event.which==1){ Record.del(file); } },false);
				container.appendChild(close);
				container.appendChild(img);
                return container;
            },
            hrsize:function(size){
                if(!size) return '0 B';
                var i=Math.floor(Math.log(size)/Math.log(1024));
                return (size/Math.pow(1024,i)).toFixed(2)*1+' '+(['B','KiB','MiB'][i]||'Sempai, it`s too big ~');
            },
            icon:function(type){
                var icon=_.create('i'),
                    name='icon-file-';
                if(type==false){
                    name+="doc";
                } else switch(type){
                    case MIME.image: name+='img'; break;
                    case MIME.audio: name+='aud'; break;
                    case MIME.video: case MIME.flash: name+='vid'; break;
                    case MIME.text: name+='txt'; break;
                    case MIME.source: name+='cod'; break;
                    case MIME.archive: name+='arc'; break;
                    case MIME.torrent: name+='p2p'; break;
                    case MIME.key: name+='doc-inv'; break;
                    case MIME.container: name+='doc'; break;
                    default: name+='doc'; break;
                }
                icon.classList.add(name);
                return icon;
            },
            info:function(file){
                var info=_.create('span'),
                    size=Record.hrsize(file.size),
                    name=_.string.basename(file.name);
                if(name.length > 64) name = name.substring(0,64) + '…';
                info.innerHTML='<em>'+name+'</em>&nbsp;<dfn>('+size+')</dfn>';
                if(Z.MMI.type(file.name) == MIME.invalid){
                	info.innerHTML+="<span title='"+Data.Error.Support+"'>"+Data.Icon.warning+"</span>";
                }
                return info;
            },
            process:function(event, file){
                var container=_.create('div');
                container.appendChild(Record.preview(event,file));
                container.appendChild(Record.info(file));
                return container;
            }
        };
		this.start=(function(){
			DOM.atf={
				form:_.id('postform'),
				count:_.selector('#postform__textarea + kbd')[0],
				switch:_.id('open-postform'),
				code:_.id('code'),
				file:{}
			};

			_.usr.atf=new _.local.interface(_.lcl+'-atf');
			_.usr.atf.data={/*user-related secondary settings - not needed on server*/
				formPosition:{x:0,y:0},
				defaultSage:false,
				defaultNoko:false,
				savedText:""
			};

			if(DOM.atf.form){
				DOM.atf.file={
					host:null,
					fake:null,
					prev:null,
					collection:[]
				};
				_.usr.atf.extend.saveFormPosition=function(){
					_.usr.atf.data.formPosition.y=DOM.atf.form.parentNode.offsetTop;
					_.usr.atf.data.formPosition.x=DOM.atf.form.parentNode.offsetLeft;
					_.usr.atf.save();
				};
				_.usr.atf.extend.loadFormPosition=function(){
					DOM.atf.form.parentNode.style.top=_.usr.atf.data.formPosition.y+"px";
					DOM.atf.form.parentNode.style.left=_.usr.atf.data.formPosition.x+"px";
				};
				_.usr.atf.init();
				_.usr.atf.load();
				//_.usr.atf.extend.loadFormPosition(); // no sense to load position when form is sticky
				document.addEventListener('keydown',function(event){
					if(event.ctrlKey){
						if(event.keyCode == 32){ Z.ATF.toggle(!DOM.atf.switch.checked); }
						if(event.keyCode == 13){ Z.ATF.create(event); }
					}
				},false);
				DOM.atf.form.addEventListener('submit',function(event){ Z.ATF.create(event); },false);
				if(DOM.atf.count){
					DOM.atf.form.text.addEventListener('keydown',function(event){ Z.ATF.counter(event); },false);
					DOM.atf.form.text.addEventListener('keyup',function(event){ Z.ATF.counter(event); },false);
				}
				if(DOM.atf.form["files[]"]){
					Record.setup("postform__file-collection");
					//DOM.atf.file.addEventListener('change',function(){ Z.ATF.fset(); },false);
					//DOM.atf.file.addEventListener('drop',function(event){ Z.ATF.fdrop(event); },false);
					//DOM.atf.file.fake.addEventListener('click',Record.add,false);
				}
				DOM.atf.form.addEventListener('dragenter',function(event){ Record.drag(event); },false);
				DOM.atf.form.addEventListener('mousedown',function(event){
					DOM.drag.e = this.parentNode;
					DOM.drag.it(event);
					DOM.drag.kill=function(event){ _.usr.atf.extend.saveFormPosition(); };
					DOM.drag.end=function(event){ _.usr.atf.extend.saveFormPosition(); };
                },false);
			}
			if(debug) say(Data.Tag.ATF + Data.Message.OK);//i only live because my heart never wanter to stop beating
		})();
		this.counter=function(event){
			var wp=(DOM.atf.form.text.value.length/8192)*100;
			if(wp >= 100){ wp = 100; DOM.atf.count.style.backgroundColor="crimson"; }
			else { DOM.atf.count.style.backgroundColor="#43f7c7"; }
			DOM.atf.count.style.width=wp+'%';
		};
		this.reset=function(){
			DOM.atf.form.reset();
			if(DOM.atf.code) this.code();
		};
		this.toggle=function(state){ if(DOM.atf.switch) DOM.atf.switch.checked=state?true:false; };
		this.float=function(state){
			if(DOM.atf.form){
				Z.ATF.toggle(state);
				var drag=DOM.atf.form.parentNode;
				drag.style.position="absolute";
				drag.style.opacity=1;
				_.usr.atf.extend.loadFormPosition();
			}
		}
		this.wait=function(state){
			Environment.pending(state);
			DOM.atf.form.style.pointerEvents=state?"none":"auto";
			DOM.atf.form.style.cursor=state?"not-allowed":"auto";
		}
		this.create=function(event){
			if(!_.opt.client.ajax){
				console.info("in func");
				DOM.atf.form["send"].click();
				return true;
			}
			event.preventDefault();
			event.stopPropagation();
			if(!this.check()) return false;
			Z.ATF.wait(true);
			_.ajax.perform(
				'/'+location.pathname.split('/')[1]+'/create',
				'POST',
				new FormData(DOM.atf.form),
				function(response){
					console.log(response);
					Z.ATF.wait(false);
					if(!response){
						Environment.message(Data.Error.Unknown, Data.Message.Type.Error);
						if(DOM.atf.code) Z.ATF.code();
					} else {
						Z.ATF.reset();
						console.log(response);
					}
				},
				function(e,xhr){ Z.ATF.wait(false); err(Data.Error.Network + ' ('+err+')'); Z.ATF.code(); },
				{'X-AJAX-Perform':true}
			);
			return false;
		};
		this.check=function(){
			if(DOM.atf.code){
				if(DOM.atf.code.length < 1){
					Environment.message(Data.Error.Captcha, Data.Message.Type.Error);
					Environment.pending(0);
					this.code();
					return false;
				}
			}
			if(DOM.atf.file.collection.length > 0){
				for(var file in DOM.atf.file.collection){
					if(!Record.check(file)){
						Environment.message(Data.Error.Support, Data.Message.Type.Error);
						Environment.pending(0);
						return false;
					}
				}
			} else if(DOM.atf.form.text.length < 1){
				Environment.message(Data.Error.Send, Data.Message.Type.Error);
				Environment.pending(0);
				return false;
			}
			return true;
        };
	};
	/*####################################################################################################################################################*/
	this.init=function(){
		if(!launch) return log(Data.Tag.Z + "Stopped");
		say('Zeroscript! '+version);
		if(debug) console.time('init');
		if(undefined===_) return err('Missing Hakase.js');
		Module.GEN();
		if(debug) console.timeEnd('init');
	};
	this.al=function(){
		if(!launch) return log(Data.Tag.Z + "Stopped");
		if(debug) console.time('al');
		chainload(document);
		if(debug) console.timeEnd('al');
	};
	var chainload=function(area){
		Z.MMI.init(area);
		Z.CIA.init(area);
	};
}
document.addEventListener('DOMContentLoaded', function(){
	_.Zeroscript=new Z();
	_.Zeroscript.init();
	window.addEventListener('load', function(){ _.Zeroscript.al(); }, false);
}, false);
function Dots(){
	var canvas=null;
	var context=null;
	var mp={};
	var dots={};
	var width=0;
	var height=0;

	this.init=function(id,w,h){
		width=w;
		height=h;
		canvas=_.id(id);
		canvas.width=w;
		canvas.height=h;
		context=canvas.getContext('2d');
		context.lineWidth=0.3;
		strokeStyle=(new Color(150)).style;
		mp={x:30*canvas.width/100, y:30*canvas.height/100};
		dots={n:32,distance:160,d_radius:1000,array:[]};
		canvas.onmousemove=function(e){
			mp.x=e.pageX;
			mp.y=e.pageY;
		};
		createDots();
		requestAnimationFrame(animateDots);	
	};

	Dot.prototype={
		draw: function(){
			context.beginPath();
			context.fillStyle=this.color.style;
			context.arc(this.x,this.y,this.radius,0,Math.PI*2,false);
			context.fill();
		}
	};
	function Dot(){
		this.x=Math.random()*canvas.width;
		this.y=Math.random()*canvas.height;
		this.vx=-0.5+Math.random();
		this.vy=-0.5+Math.random();
		this.radius=Math.random()*2;
		this.color=new Color();
	}

	function style(r,g,b){ return 'rgba('+r+','+g+','+b+',0.8)'; }
	function mix(c1,w1,c2,w2){ return (c1*w1+c2*w2)/(w1+w2); }
	function averageColorStyles(d1,d2){
		var c1=d1.color,c2=d2.color,
			r=mix(c1.r,d1.radius,c2.r,d2.radius)|0,
			g=mix(c1.g,d1.radius,c2.g,d2.radius)|0,
			b=mix(c1.b,d1.radius,c2.b,d2.radius)|0;
		return style(r,g,b);
	}
	function Color(min){
		min=min||0;
		with(this) { r=49; g=194; b=151; }
		this.style=style(this.r,this.g,this.b);
	}
	function createDots(){ for(i=0;i<dots.n;i++) dots.array.push(new Dot()); }
	function moveDots() {
		for(i=0;i<dots.n;i++){
			var dot=dots.array[i];
			if(dot.y<0||dot.y>canvas.height){
				dot.vx =dot.vx;
				dot.vy = - dot.vy;
			}
			else if(dot.x<0||dot.x>canvas.width){
				dot.vx=-dot.vx;
				dot.vy=dot.vy;
			}
			dot.x+=dot.vx;
			dot.y+=dot.vy;
		}
	}
	function connectDots() {
		for(i = 0; i < dots.n; i++){
			for(j = 0; j < dots.n; j++){
				i_dot = dots.array[i];
				j_dot = dots.array[j];

				if((i_dot.x - j_dot.x) < dots.distance && (i_dot.y - j_dot.y) < dots.distance && (i_dot.x - j_dot.x) > - dots.distance && (i_dot.y - j_dot.y) > - dots.distance){
					if((i_dot.x - mp.x) < dots.d_radius && (i_dot.y - mp.y) < dots.d_radius && (i_dot.x - mp.x) > - dots.d_radius && (i_dot.y - mp.y) > - dots.d_radius){
						context.beginPath();
						context.strokeStyle = averageColorStyles(i_dot, j_dot);
						context.moveTo(i_dot.x, i_dot.y);
						context.lineTo(j_dot.x, j_dot.y);
						context.stroke();
						context.closePath();
					}
				}
			}
		}
	}
	function drawDots(){
		for(i=0;i<dots.n;i++){
			var dot=dots.array[i];
			dot.draw();
		}
	}
	function animateDots(){
		context.clearRect(0,0,canvas.width,canvas.height);
		moveDots();
		connectDots();
		drawDots();
		requestAnimationFrame(animateDots);	
	}
};

window.addEventListener('load',function(event){
	_.dots=new Dots();
	console.log(_.w,_.h);
	_.dots.init('dots',_.w,_.h);
},false);
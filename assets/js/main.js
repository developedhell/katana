'use strict';
/* prototype shortcuts */
HTMLElement.prototype.remove=function(){ this.parentElement.removeChild(this); }
HTMLElement.prototype.show=function(){ this.style.display=''; }
HTMLElement.prototype.hide=function(){ this.style.display='none'; }
HTMLElement.prototype.reveal=function(){ this.style.visibility='visible'; }
HTMLElement.prototype.cloak=function(){ this.style.visibility='hidden'; }
HTMLElement.prototype.toggle=function(){ this.style.display=(this.style.display=='none')?'':'none'; }
HTMLElement.prototype.lock=function(){ if(this){ this.setAttribute("locked",true); this.removeAttribute("unlocked"); } }
HTMLElement.prototype.unlock=function(){ if(this && this.hasAttribute("locked")) this.removeAttribute("locked"); }
HTMLElement.prototype.offset=function(){ return this.getBoundingClientRect ? _.offset.rect(this) : _.offset.sum(this); }
/* _ framework */
var _={
	w:null,h:null,
	arg:null,
	lcl:'lcl',
	cks:'cks',
	mob:false,
	opt:{},
	usr:{
		tmp:{},
		cmd:{}
	},
	Zeroscript:null
};
_.clear=function(e){ if(e) while(e.firstChild) e.removeChild(e.firstChild); }
_.remove=function(e){ e.parentNode.removeChild(e); }
_.id=function(id){ return document.getElementById(id); }
_.class=function(cl){ return document.getElementsByClassName(cl); }
_.selector=function(s){ return document.querySelectorAll(s); }
_.create=function(e,id){ var T=document.createElement(e); if(typeof id==='string') T.id=id; return T; }
_.define=function(id,scope){ return _.id(id)||((_.id(scope)||document.body).appendChild(_.create('div',id)));}
_.each=function(nodelist,callback){ Array.prototype.forEach.call(nodelist,callback); }
_.apply=function(selector,callback){ _.each(_.selector(selector),callback); }
_.listen=function(selector,type,callback){ _.apply(selector,function(element){ element.addEventListener(type,callback,false); }); }
_.once=function(target,type,listener){
    target.addEventListener(type,function fn(event){
        target.removeEventListener(type,fn);
        listener(event);
    });
}
_.exists=function(element){ return (null != element) && (undefined != element); };
_.ready=function(callback){ document.addEventListener('DOMContentLoaded', callback, false); };
_.string={
	empty:function(str){ return (str == "") || (str == null) || (str == undefined); },
	extension:function(str){ if(_.string.empty(str)) return str; return str.substr((~-str.lastIndexOf(".") >>> 0) + 2); },
	digits:function(str){ if(_.string.empty(str)) return str; if(i=str.match(/\d+/)) return i[0]; return false; },
    basename:function(str){ if(_.string.empty(str)) return str; if(/\//.test(str)) return str.split('/').reverse()[0]; return str.split('\\').reverse()[0]; },
    pseudo:function(str){ if(_.string.empty(str)) return str; return '::'+_.string.btoa(_.string.basename(str)); },
    btoa:function(str){ return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, function(match, p1) { return String.fromCharCode(parseInt(p1, 16)); })); },
    atob:function(str){ return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) { return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2); }).join('')); }
};
_.offset={
	sum:function(elem){
		var top=0,left=0;
		if(elem){ top=top+parseInt(elem.offsetTop); left=left+parseInt(elem.offsetLeft); elem=elem.offsetParent; }
		return {top:top,left:left};
	},
	rect:function(elem){
		var box=elem.getBoundingClientRect(),
			body=document.body,
			docElem=document.documentElement,
			scrollTop=window.pageYOffset||docElem.scrollTop||body.scrollTop,
			scrollLeft=window.pageXOffset||docElem.scrollLeft||body.scrollLeft,
			clientTop=docElem.clientTop||body.clientTop||0,
			clientLeft=docElem.clientLeft||body.clientLeft||0;
		return {top:Math.round(box.top+scrollTop-clientTop),left:Math.round(box.left+scrollLeft-clientLeft)};
	}
};
_.cookie={
	get:function(name){
		var c,ca=document.cookie.split(';');
		name+='=';
		for(var i=0;i<ca.length;i++){
			c=ca[i];
			while(c.charAt(0)==' ') c=c.substring(1);
			if(c.indexOf(name)==0) return decodeURIComponent(c.substring(name.length,c.length));
		}
		return '';
	},
	set:function(name, value, expires, path, domain, secure){
		var d=new Date(); d.setTime(d.getTime()+(expires*24*60*60*1000));
		document.cookie=
			name+"="+escape(value)+
			((expires)?"; expires="+d.toUTCString():"")+
			((path)?"; path="+path:"")+
			((domain)?"; domain="+domain:"")+
			((secure)?"; secure":"");
	},
	del:function(name){ _.cookie.set(name,"",{expires:-1},'/'); }
};
_.local={
	interface:function(name){
		this.name=name;
		this.data=null;
		this.init=function(){
			if(this.load){ return; }
			else this.save();
		};
		this.save=function(){
			localStorage.setItem(this.name,JSON.stringify(this.data));
		};
		this.load=function(){
			var t=localStorage.getItem(name);
			if(undefined!=t){
				t=JSON.parse(t);
				for(var o in t){
					if(this.data.hasOwnProperty(o)){
						this.data[o]=t[o];
					}
				}
				return true;
			}
			return false;
		};
		this.extend={};
	}
};
_.ajax={
	success:function(data){ console.info(data); },
	error:function(xhr,status){ console.error('HTTP '+xhr+' '+status); },
	create:function(){
		var XMLHttpFactories=[
			function(){return new XMLHttpRequest();},
			function(){return new ActiveXObject("Msxml2.XMLHTTP");},
			function(){return new ActiveXObject("Msxml3.XMLHTTP");},
			function(){return new ActiveXObject("Microsoft.XMLHTTP");}
		];
		var xmlhttp=false;
		for(var i=0;i<XMLHttpFactories.length;i++){ try { xmlhttp=XMLHttpFactories[i](); } catch (e) { continue; } break; }
		return xmlhttp;
	},
	perform:function(url,method,data,success,error,headers){
		if(!data && (method=='POST')) return false;
		var req=_.ajax.create();
		if(!req) return false;
		if(typeof success!='function') success=_.ajax.success;
		if(typeof error!='function') error=_.ajax.error;
		req.open(method,url,true);
		if(headers!==undefined) for(var i in headers) req.setRequestHeader(i,headers[i]);
		req.send(data);
		req.onreadystatechange=function(){
			if(req.readyState==4){
				if(req.status==200 || req.status==304) success(req.responseText);
				else error(req.status,req.statusText);
			}
		};
		return req.responseText;
	}
};

document.addEventListener('DOMContentLoaded',function(){
	_.w=document.documentElement.clientWidth||document.body.clientWidth;
	_.h=document.documentElement.clientHeight||document.body.clientHeight;
	_.mob=/mobile/i.test(navigator.userAgent);
	var tmp=_.cookie.get('server_settings');
	if(tmp.length > 0){ // load server environment settings
		_.opt.server=JSON.parse(atob(tmp));
	}
	if(!_.string.empty(_.opt.server.argument)){ // load client operational settings
		_.opt.client=JSON.parse(atob(_.cookie.get('client_settings-'+_.opt.server.argument)));
	}
	_.apply("[locked]", function(e){e.unlock()});
	_.apply("[unlocked]", function(e){e.lock()});
},false);
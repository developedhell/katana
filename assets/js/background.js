var iw=null,ih=null,
	background={
		container:_.id('background'),
		loader:new Image(),
		waspect:0,iaspect:0,
		shakalize:function(event){
			_.w=document.documentElement.clientWidth||document.body.clientWidth;
			_.h=document.documentElement.clientHeight||document.body.clientHeight;
			background.waspect=_.h/_.w;
			background.iaspect=ih/iw;
			background.container.style.backgroundSize = (background.waspect > background.iaspect ? "auto 100%" : "100% auto");
		}
	};
background.loader.onload=function(event){
	iw=this.width; ih=this.height;
	background.container.style.backgroundImage='url('+background.loader.src+')';
	background.shakalize();
	background.container.classList.add('ready');
};
window.addEventListener('resize',background.shakalize,false);
(function(){
	background.loader.src='/assets/i/wp/'+(0|Math.random()*15+1)+'.jpg';
})();
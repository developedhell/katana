var Flow=function(){
	var w,h,request=false,
		audioVisual,canvas,canvas_context,context,audio,gain,analyser,source,
		playerDOM='',color='#01FFA1',colors=['#90ee90','#7fffd4','#5f9ea0','#ed143d','#cd5c5c','#f0e68c','#e6e6fa','#87ceeb','#01FFA1','#dda0dd','#98fb98'];
	this.init=function(container){
		if(!container) return false;
		w=document.documentElement.clientWidth||document.body.clientWidth; h=document.documentElement.clientHeight||document.body.clientHeight;
		playerDOM='<span id="spectrum"><canvas id="canvas" width="'+w+'" height="100"></canvas></span>';
		container.innerHTML=playerDOM;
		color="#6cebbe";
		audioVisual=_.id('spectrum');
		canvas=_.id('canvas');
		if(!(canvas&&audioVisual)) return false;
		canvas_context=canvas.getContext('2d');
		context=new AudioContext(); audio=new Audio();
		gain=context.createGain();
		analyser=context.createAnalyser();
		source=context.createMediaElementSource(audio);
		with(window) AudioContext=AudioContext||webkitAudioContext;
		with(audio)
			src='',
			controls=autoplay=true,//loop=
			id='player';
		audioVisual.appendChild(audio);
		source.connect(analyser);
		source.connect(gain);
		gain.connect(context.destination);
		with(analyser) fftSize=4096, connect(context.destination);
		with(window) requestAnimFrame=(function(){return requestAnimationFrame||webkitRequestAnimationFrame||mozRequestAnimationFrame||function(callback){setTimeout(callback,1000/60)}})();
		window.addEventListener('blur',function(){
			if(request&&window.cancelAnimationFrame){
				window.cancelAnimationFrame(request);
				request=false;
			}
		},false);
		var T=this.exec;
		window.addEventListener('focus',function(){ if(!request){ request=window.requestAnimFrame(T); } },false);
        audio.src="/assets/media/ambience/specials/Dj Klubbingman feat. Trixi Delgado - We call it....mp3";
        audio.play();
		this.exec();
	};
	this.exec=function t(){
		canvas_context.stroke();
		window.requestAnimFrame(t);
		var i,j, sum, scaled_average, num_bars=canvas.width/4, data=new Uint8Array(2048), bin_size=0|(data.length/num_bars);
		analyser.getByteFrequencyData(data);
		with(canvas_context) fillStyle=color, clearRect(0,0,canvas.width,canvas.height);
		for(i=0;i<num_bars;++i){
			sum=0; for(j=0;j<bin_size;++j) sum+=data[(i*bin_size)+j];
			scaled_average=canvas.height*sum/(bin_size*256);
			sum=canvas.width/num_bars;
			canvas_context.fillRect(i*sum,canvas.height,sum-2,-scaled_average);
		}
	};
};
<?php

final class Environment {
	# Environment
	const page = "engine/environment/page";
	const data = "engine/environment/data";
	const note = "engine/environment/note";
	const seal = "engine/environment/seal";
	const spell = "engine/environment/spell";
	const graph = "engine/environment/graph";
	const chrona = "engine/environment/chrona";
}

final class Module {
	# Auxiliary
	const e = "engine/module/e";
	const xt = "engine/module/xt";
	const view = "engine/module/view";
	const regex = "engine/module/regex";
	# Configuration
	const server_config = "engine/module/cfg/server";
	const client_config = "engine/module/cfg/client";
	# KT classes
	const kt_handle = "engine/module/kt/handle";
	const kt_request = "engine/module/kt/request";
	const kt_generate = "engine/module/kt/generate";
	# IO classes
	const io_assembly = "engine/module/io/assembly";
	const io_commands = "engine/module/io/commands";
	const io_firewall = "engine/module/io/firewall";
	const io_strategy = "engine/module/io/strategy";
	const io_terminal = "engine/module/io/terminal";
}

final class Plugin {
	# KT traits
	const kt_ochoba = "engine/trait/kt/ochoba";
	const kt_unity = "engine/trait/kt/unity";
	const kt_mytischi = "engine/trait/kt/mytischi";
	const kt_ultra = "engine/trait/kt/ultra";
	const kt_hardcore = "engine/trait/kt/hardcore";
	# IO traits
	const io_info = "engine/trait/io/info";
	const io_user = "engine/trait/io/user";
	# API traits
	const api_client = "engine/trait/api/client";
	const api_server = "engine/trait/api/server";
	# AJAX traits
	const ajax_post = "engine/trait/ajax/post";
	# View traits
	const v_general = "engine/trait/view/general";
	const v_board = "engine/trait/view/board";
}
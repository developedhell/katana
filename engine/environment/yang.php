<?php # YANG # Predefined system layer

abstract class Protector {
	function __construct(){ return false; }
	function __destruct(){ return false; }
	function __clone(){ return false; }
	function __sleep(){ return false; }
	function __wakeup(){ return false; }
	function __invoke($directive=null){ return $directive; }
	function __set_state(){ return false; }
	function __toString(){ return false; }
}

function import(...$f){ foreach($f as $n => $g){ if(file_exists($h = ROOT.$g.".php")){ include_once $h; } } return true; }
function decode($f,$iv=null,$k=null){
	if(file_exists($g = ROOT."{$f}.json")){
		if(!((null == $iv) || (null == $k))){
			if(class_exists('NCC')){
				return json_decode(NCC::decrypt(file_get_contents($g), $k, $iv), true);
			}
		} else {
			return json_decode(file_get_contents($g),true);
		}
	}
    return null;
}
function encode($f,$h,$iv=null,$k=null){
	if(file_exists(ROOT."{$f}.json")){
		if(!((null == $iv) || (null == $k))){
			if(class_exists('NCC')){
				return commit($f.".json", NCC::encrypt(json_encode($h, JSON_UNESCAPED_UNICODE), $k, $iv));
			}
		} else {
			return commit($f.".json", json_encode($h, JSON_UNESCAPED_UNICODE));
		}
	}
}
function commit($path,$data,$append=false){
	$t = 'w'; if($append) $t = 'a';
	$f=fopen(ROOT.$path,$t);
	if($f===false) return debug("Unable to open file at '{$path}'");
	if(flock($f,LOCK_EX)){
		fwrite($f,$data);
		fflush($f);
		flock($f,LOCK_UN);
	} else return debug("Unable to perform exclusive write on '{$path}'");
	fclose($f);
	return true;
}
function escape($string){ return nl2br(addslashes(htmlspecialchars(trim($string)))); }
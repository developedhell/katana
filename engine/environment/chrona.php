<?php

final class Chrona { # Common Human-Readable Object Notation Automation
	static function bytes($bytes){
		if($bytes < 1024) return $bytes." B";
		$bytes = sprintf("%.2f", $bytes/1024);
		if($bytes < 1024) return $bytes." KiB";
		$bytes = sprintf("%.2f", $bytes/1024);
		if($bytes < 1024) return $bytes." MiB";
		$bytes = sprintf("%.2f", $bytes/1024);
		return $bytes." GiB";
	}
	static function bits($bits){
		if($bits < 1024) return $bits." bps";
		$bits = sprintf("%.2f", $bits/1024);
		if($bits < 1024) return $bits." Kbps";
		$bits = sprintf("%.2f", $bits/1024);
		return $bits." Mbps";
	}
	static function time($s){
		$H = floor($s / 3600);
		$i = ($s / 60) % 60;
		$s = $s % 60;
		$t = sprintf("%02d:%02d:%02d", $H, $i, $s);
		return $t;
	}
	static function binmode($a){ $r = 0; foreach($a as $f => $g){ $r <<= 1; $r |= $a[$f]; } return $r; }
	static function binarray($a,$m){ foreach($a as $f => $g){ $a[$f] = $m&1; $m>>=1; } return $a; }
}
<?php

final class Data {
	const LOCALE = "assets/data/lang/";
	const CONFIG = "assets/data/config";
	const ERRORS = "assets/data/errors";
	const COLORS = "assets/data/colors";
	const NODELIST = "assets/data/nodelist";
	const PAGELIST = "assets/data/pagelist";
	const HASHLIST = "assets/data/hashlist";
	const XT_REPOSITORY = "assets/data/extensions/repository";
	const XT_SETTINGS = "assets/data/extensions/settings";
}
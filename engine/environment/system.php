<?php # X_SYSTEM # X system namespace

final class System {
	const Q = "engine/system/q";
	const IO = "engine/system/io";
	const API = "engine/system/api";
	const SQL = "engine/system/sql";
	const AJAX = "engine/system/ajax";
	const CRYPT = "engine/system/ncc";
	const CLIENT = "engine/system/client";
}
<?php

final class Graph {
	const SPHERE = "assets/data/graph/sphere"; # announce list
	const SELECT = "assets/data/graph/select"; # selected list
	const NEARBY = "assets/data/graph/connected"; # connected
	const BANNED = "assets/data/graph/disconnected"; # disconnected

	public static $sphere = null;
	public static $select = null;

	/**
	 * Send a recall to the global node network to declare self-existence
	 */
	static function echo(){}

	/**
	 * Initiate an omnicall to connected nodes to obtain node network state
	 * or a direct call to a selected node
	 */
	static function call($node = null){}

	/**
	 * Send a notification to connected nodes
	 * or to a selected node
	 */
	static function kido($node = null){}

	/**
	 * Connect to a node
	 */
	static function conn($node){}

	/**
	 * Disconnect from a node (will add to the banned list)
	 */
	static function disc($node){}

	static function compile__list_sphere(){
		$nodes = SQL::kq("SELECT graph.id, graph.lt, graph.name, graph.desc, graph.rang, graph.bump, graph.mode FROM noosphere.graph ORDER BY graph.lt DESC", SQL::ARR | SQL::KEY);
		$list = [];
		foreach($nodes as $node){
			$list[$node['lt']] = [
				'id' => abs(intval($node['id'])),
				'name' => $node['name'],
				'desc' => $node['desc'],
				'rang' => abs(intval($node['rang'])),
				'bump' => abs(intval($node['bump'])),
				'mode' => abs(intval($node['mode']))
			];
		}
		if(encode(Graph::SPHERE, $list)){
			debug("[graph::compile__list_sphere] DONE");
			return true;
		}
		debug("[graph::compile__list_sphere] Error writing");
		return false;
	}
}
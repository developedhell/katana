<?php

final class Note extends Exception {
	const SUCCESS = 1;
	const WARNING = 2;
	const FAILURE = 4;

	function __construct($message,$flag = null){ return static::notify($message, $flag ? $flag: static::FAILURE); }
	static function notify($message,$flag = null){ $_SESSION['NT'].= static::output($message, $flag); return false; }
	static function divide($dat,$sep){ foreach(explode($sep,$dat) as $e) yield $e; }
	static function redeem(){
		if(empty($_SESSION['NT'])) return false;
		$_SESSION['NT'] = rtrim($_SESSION['NT'],PHP_EOL);
		$log = '';
		$parse = function($dat,$sep){ foreach(explode($sep,$dat) as $e) yield $e; };
		foreach(static::divide($_SESSION['NT'],PHP_EOL) as $message){
			$log.= $message;
		}
		$_SESSION['NT'] = null;
		return $log;
	}
	static function silent(){
		if(empty($_SESSION['NT'])) return [];
		$_SESSION['NT'] = rtrim($_SESSION['NT'],PHP_EOL);
		if(X::$ajax){
			$log = [];
			foreach(static::divide($_SESSION['NT'],PHP_EOL) as $message){
				$log[] = $message;
			}
		}
		$_SESSION['NT'] = null;
		return $log;
	}
	static function output($message,$flag = null){
		if($flag) switch($flag){
			case self::SUCCESS:
				$message = "<div class='message message_success popup popup_grey'>"
					."<div class='message__item'>{$message}</div></div>";
				break;
			case self::WARNING:
				$message = "<div class='message message_warning popup popup_grey'>"
					."<div class='message__item'>{$message}</div></div>";
				break;
			case self::FAILURE:
				$message = "<div class='message message_failure popup popup_grey'>"
					."<div class='message__item'>{$message}</div></div>";
				break;
			default:
				$message = "<div class='message message_failure popup popup_grey'>"
					."<div class='message__item'>{$message}</div></div>";
				break;
		}
		return "<label><input type='checkbox' checked />{$message}</label>";
	}
}
<?php

final class Page {
	const NONE = 0; # no type
	const IDLE = 1; # static page
	const FUNC = 2; # dynamic page (dynamic)
	const NODE = 3; # board page (dynamic)
	const WIRE = 4; # board thread (static)
	const NCIS = 5; # service page
}
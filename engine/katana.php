<?php # KT # Katana X engine (Sakaba fork), August 2017 - now

import(
	Plugin::kt_ochoba,		# core functions
	Plugin::kt_unity,		# layout functions
	Plugin::kt_mytischi,	# routing and navigation
	Plugin::kt_ultra,		# ?!+
	Plugin::kt_hardcore		# CRUD interactive
);

final class Katana extends Protector {
	
	use
		OCHOBA,
		UNITY,
		MYTISCHI,
		ULTRA,
		HARDCORE;

	/**
	 * Entry point
	 */
	public static function init(){
		static::config();
		if(X::$ajax){
			import(System::IO) and AJAX::init();
		} else {
			switch(X::$path[0]){
				case "io": case "modlog":
					import(System::IO) and IO::init();
					break;
				case "api":
					import(System::API) and API::init();
					break;
				default:
					static::route();
					break;
			}
		}
	}

	/**
	 * Routing schema
	 */
	private static function route(){
		$argn = y(X::$path[0], "main");
		switch($argn){
			case 'about':
			case 'help':
			case 'news':
			case 'faq':
				static::page__static();
				break;
			case 'all':
			case 'fav':
			case 'search':
				static::page__dynamic();
				break;
			case 'main':
				static::page__dynamic_main();
				break;
			case 'options':
				static::page__dynamic_options();
				break;
			default:
				static::page__node();
				break;
		}
	}

	/**
	 * Configuration
	 */
	private static function config(){
		static::init_cookies();
		static::init_session();
		static::load();
		X::$conf = new ServerConfig();
		X::$conf->expose();
		X::$opts = new ClientConfig();
		X::$sign = IO::sign();
		X::$anon = IO::guid();
		X::$mobi = isset($_SERVER["MOBILE"]);
		X::$path = array_slice(explode('/', mb_strtolower(urldecode($_SERVER['REQUEST_URI']), 'UTF-8')), 1);
		X::$ajax = AJAX::check();
		X::$type = Page::NONE;
	}

	/**
	 * Load systems and modules
	 */
	private static function load(){
		import(
			Environment::data,
			Environment::page,
			Environment::note,
			Environment::seal,
			Environment::spell,
			Environment::graph,
			Environment::chrona
		);
		import(
			System::CRYPT,
			System::IO,
			System::AJAX
		);
		import(
			Module::e,
			Module::view,
			Module::server_config,
			Module::client_config
		);
	}

	/**
	 * Aux cookie initialization
	 */
	private static function init_cookies(){
		ini_set("suhosin.session.cryptdocroot", "Off");
		ini_set("suhosin.cookie.cryptdocroot", "Off");
		ini_set('session.cookie_domain', '.'.HOST);
		session_set_cookie_params(0, '/', HOST);
	}

	/**
	 * Aux session initialization
	 */
	private static function init_session(){
		session_start();
		if(!isset($_SESSION['IO'])) $_SESSION['IO'] = false;
		if(!isset($_SESSION['NT'])) $_SESSION['NT'] = '';
	}
}
<?php # X # Katana behavior

final class X extends Protector {

	const VERSION = 5.5;	# X version
	const VERBOSE = true;	# enable to start logging system information

	# mutable internal variables (do not touch unless you know exactly what you're doing)
	static $temp = null;	# temporary variable
	
	static $anon = null;	# username if is logged in
	static $sign = null;	# user digital signature
	static $mobi = false;	# boolean if handheld device

	static $tree = null;	# all boards list
	static $cata = null;	# catalist for this board

	static $path = null;	# current path
	static $node = null;	# current board literal
	static $wire = 0;	    # current thread
	static $page = 0;		# current page
	static $type = 0;		# current page type (notype/static/dynamic/board/service)
	static $view = null; 	# current page view

	static $mode = null;	# permissions f/ nodes

	static $ajax = null;	# xmlhttp flag
	static $opts = null;	# user options
	static $conf = null;	# system config
}
<?php

final class Regex extends \Protector {

	const TYPE_MULTIMEDIA = '/(webp|jpe?g|gif|a?png|webm|mp4|swf)/i';
	const MIME_MULTIMEDIA = '/^(application\/(ogg|x-shockwave-flash|vnd.adobe.flash.movie))|((audio|video)\/(x-)?(aac|(vorbis\+)?og(g|v)|mpeg|webm|mp4))$/i';

	const TYPE_IMAGE = '/([^\/]+)\.(webp|jpe?g|gif|a?png)$/i';
	const TYPE_AUDIO = '/([^\/]+)\.(aac|mp3|wav|ogg|weba)$/i';
	const TYPE_VIDEO = '/([^\/]+)\.(mp4|webm|ogv)$/i';
	const TYPE_FLASH = '/([^\/]+)\.(swf|flv)$/i';
	const TYPE_PASTE = '/([^\/]+)\.(txt|log|src|sh|js|s|asm|c|cpp|h|hpp|i|php|pl|py|hs|lisp|java)$/i';
	const TYPE_ARCHIVE = '/([^\/]+)\.(tar|gz|bz|zip|rar|7z)$/i';
	const TYPE_BITTORRENT = '/([^\/]+)\.(torr|torrent)$/i';
	const TYPE_CIPHER = '/([^\/]+)\.(k|key|keystore|crt|cert|cer|pem|pki|csr|pfx|pkcs\d+|.p\d+|.deru?|ecc|ring|p7b|x509|spc|usr)$/i';
	const TYPE_CONTAINER = '/([^\/]+)\.(nd|node|apx|apxpa|arx|tcc?|cc|vault)/i';

	const MIME_IMAGE = '/image\//i';
	const MIME_IMAGE_STRICT = '/^image\/(webp|jpe?g|gif|a?png)$/i';
	const MIME_AUDIO = '/audio\//';
	const MIME_AUDIO_STRICT = '/^(application|audio)\/(x-)?(aac|(vorbis\+)?ogg|mpeg)$/i';
	const MIME_VIDEO = '/video\//';
	const MIME_VIDEO_STRICT = '/^(application|video)\/(x-)?((vorbis\+)?og(g|v)|mpeg|webm|mp4)$/i';
	const MIME_FLASH = '/(x-shockwave-flash|vnd.adobe.flash.movie)/i';
	const MIME_FLASH_STRICT = '/^application\/(ogg|x-shockwave-flash|vnd.adobe.flash.movie)$/i';
	const MIME_PASTE = '/^(application|text)\/(x-)?(plain|shellscript|javascript|asm|c|csrc|cplusplus|c\+\+src|css|php|perl|python|haskell|lisp|java)$/i';
	const MIME_ARCHIVE = '/^application\/(x-)?(zip|g?tar|compress|7z-compressed|rar(-compressed)?)$/i';
	const MIME_BITTORRENT = '/^application\/x-bittorrent$/i';
	const MIME_MP4 = '/video\/mp4/';
	const MIME_OGG = '/(application|audio)\/(x-)?(vorbis\+)?og(g|v)/';
	const MIME_WEBM = '/video\/webm/i';
	const MIME_WEBA = '/\/webm/i';

	const FFMPEG_DURATION = '/(?:^ +Duration: (?:00:)?([0-9:]+))/m';
	const FFMPEG_STREAM_VIDEO = '/(?:^ +Stream #.* Video: ([^,]+, [^,]+), (\d+)x(\d+),)/m';
	const FFMPEG_STREAM_AUDIO = '/(?:^ +Stream #.* Audio: ([^,]+, [^,]+, [^,]+), .*)/m';
	const FFMPEG_BLACKDETECT = '/(?:^\[blackdetect.*black_end:(\d+\.\d+))/m';

	const GENERIC_USERAGENT = '/^##useragent##$/i';
	const GENERIC_POSTCOUNT = '/^##postcount##$/i';
	const GENERIC_DICE = '/##(\d+)d(\d+)(d(\d+))?##/i';
	const GENERIC_DASH = '/\B--\B/';
	const GENERIC_LINK = '/(^|[\n (%>])([\w]*?)((ht|f)tp(s)?:\/\/[\w]+[^ \,\"\n\r\t<]*[^.:?!;) \,\"\n\r\t<])/is';
	const GENERIC_REF_NUM = '/\B(&gt;|>){2}(\d+)\b/Us';
	const GENERIC_REF_LIT = '/\B(&gt;|>){3}(\w+)\/(\d+)\b/Us';
	const GENERIC_QUOTE = '/^&gt;(.+)$/Um';

	const T_BB_SPOILER = '/\[(sp|spoiler)\](.+)\[\/\1\]/Us';
	const T_BB_STRIKEOUT = '/(\^{2})(.+)\1/Us';
	const T_BB_BOLD = '/(\*{2})(.+)\1/Us';
	const T_BB_ITALIC = '/(\*{1})(.+)\1/Us';
	const T_BB_FORMAT = '/\[(i|b|u|s|q|sup|sub)\](.+)\[\/\1\]/Us';
	const T_BB_TEXTWALL = '/\[(text|t|tw)\](.+)\[\/\1\]/Us';
	const T_BB_RED = '/\[(red)\](.+)\[\/\1\]/Uis';
	const T_BB_GREEN = '/\[(green)\](.+)\[\/\1\]/Uis';
	const T_BB_BLUE = '/\[(blue)\](.+)\[\/\1\]/Uis';

	const T_WM_SPOILER = '/(%{2})(.+)\1/Us';
	const T_WM_STRIKEOUT = '/(\^{2})(.+)\1/Us';
	const T_WM_BOLD = '/(\*{2})(.+)\1/Us';
	const T_WM_ITALIC = '/(\*{1})(.+)\1/Us';
	const T_WM_UNDERLINE = '/(_{2})(.+)\1/Us';
	const T_WM_HEADER = '/(={2})(.+)\1/U';

	const T_RAW = '/\[(ascii|raw)\](.+)\[\/\1\]/Us';
	const T_LATEX = '/\[(latex)\](.+)\[\/\1\]/Uis';
	const T_WM_CODE = '/(`{2})(.+)\1/Us';
	const T_BB_CODE = '/\[(code)\](.+)\[\/\1\]/Us';

	const FS_GOOGLE = '/google:\/\/([^\/]+)\//';
	const FS_DDG = '/ddg:\/\/([^\/]+)\//';
	const FS_WIKI = '/wiki:\/\/([^\/]+)\//';
	const FS_WOLFRAM = '/wolfram:\/\/([^\/]+)\//';
	const FS_GIT = '/git:\/\/([^\/]+)\//';
	const FS_LAIN = '/lain:\/\/([^\/]+)\//';

	const TRIPCODE = "/.+(#|!)(.+)$/";

	const TRIFORCE = '/^\/triforce$/m';

	const SERVICE_YT = '/youtu/i';
	const SERVICE_YT_ID = '/embed(\W+)([0-9A-Za-z-_]+)\?/';
	const SERVICE_VI = '/vimeo/i';
	const SERVICE_VI_ID = '/video(\/|%2F)(\d+)/i';
	const SERVICE_SC = '/soundcloud/i';
	const SERVICE_SC_ID = '/tracks(\/|%2F)(\d+)/i';

	const SPAM = '/(discou.rs|дискурс|anonum|анонум|\/\/t.me\/)/i';

	const EXTERNAL = '/https?:\/\/([\w\d\.,_-]+\.)?(mixtape\.moe|catbox\.moe|safe\.moe|0x0\.st)/i';
	const INTERNAL = '/https?:\/\/([\w\d\.,_-]+\.)?'.HOST.'/i';

	public static function escape($string){
		$_ = array(
			'/\'/' => '&#39;',
			'/%/'  => '&#37;',
			'/\*/' => '&#42;',
			'/\^/' => '&#94;',
			'/~/'  => '&#126;',
			'/_/'  => '&#95;',
			'/\[/' => '&#91;',
			'/\]/' => '&#93;',
			'/\\\/' => '&#92;',
			'/\//' => '&#47;',
			'/=/'  => '&#61;'
		);
		return preg_replace(array_keys($_),array_values($_),$string);
	}
	public static function has_preview($type){
		return preg_match(static::TYPE_IMAGE,$type) || preg_match(static::TYPE_VIDEO,$type);
	}
	public static function mime_has_preview($type){
		return preg_match(static::MIME_IMAGE,$type) || preg_match(static::MIME_VIDEO,$type);
	}
	public static function get_file_name($file){ preg_match('/(.+)\.[^\.]+$/', $file, $o); return y($o[1], ""); }
	public static function get_extension($file){ preg_match('/.+\.([^\.]+)$/', $file, $o); return y($o[1], ""); }
	public static function cut($subject){
		$repo = [
			'UFO',
			'MLP',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'[ДАННЫЕ УДАЛЕНЫ]',
			'&#9619;&#9619;&#9619;&#9619;&#9619;',
			'&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;',
			'&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;',
			'&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;',
			'&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;&#9619;',
			'(амнезиак визуального действия)',
			'(not kawaii enough)',
			'(not nyashno enough)',
			'(продам гараж)',
			'[p/r intensifies]',
			'(I secretly suck dicks)',
			'DICKBUTT',
			'[autistic screaming]',
			'AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
			'(nyan, motherfucker, do you speak it?)',
			'(у автора убежал чайник)',
			'(шутка про ICMP, дошло?)',
			'(slow mail service could not deliver this packet)',
			'NANI!?',
			'(omae wa, mou shindeiru)',
			'[modem sounds]',
			'(Ph`nglui mglw`nafh Cthulhu R`lyeh wgah`nagl fhtagn!)',
			'(oh shit~)',
			'?!+',
			'(I sexually identify myself as a helicopter)',
			'(uhm.. NYAN?!)',
			'(NIGGA THERE WAS SOME REALLY CREEPY SHIT RIGHT HERE)',
			'(fuck social networks)',
			'!q',
			'(we interrupt this message to bring you a special message from the new president of the world)',
			'(HEIL NYAN!)',
			'(gotcha!)',
			'(want some snickers?)',
			'(this is bait)',
			'(nyan cat ate this text)',
			'(VIRUS.EXE)',
			'(cyka, blyad)',
			'♪',
			'♫',
			'「 ● ● ● ● 」',
			'(IT`S ADVERTISING TIME! - NO DAD, NO!)',
			'(zero nyans were given to this post)',
			'<em style="color:gold">(this is a golden post)</em>',
			'<em style="color:#E5E4E2">(this is a platinum post)</em>',
			'(unlock this text for only $3.99)',
			'(nothing to look at)',
			'(move along)',
			'(нечего увидеть здесь. двиньте вперёд)',
			'6555',
			'(oops, divided by zero)',
			'(oops, divided by zero)',
			'(oops, divided by zero)',
			'(oops, divided by zero)',
			'(там ракуй, а здесь не надо)',
			'(не возвращайся никогда)',
			'(уходи с моих нульчей)',
			'(для того нужна сажа, чтоб ты съебался навсегда)',
			'ПШШШ',
			'ПШШШШ',
			'ПШШШШШ',
			'ПШШШШШШ',
			'ПШШШШШШШ',
			'(пыщъ)'
		];
		$replacement = '<span style="color:crimson" title="Only V.I.P. can see this text">'
			.$repo[mt_rand(0,count($repo)-1)]
			."</span>";
		return preg_replace(static::SPAM, $replacement, $subject);
	}
}
<?php # IO_FIREWALL # The filter between internal and external spaces

namespace IO;

use X\System as System;
use X\Module as Module;

final class Firewall extends \Protector {

	static function init(){
		if(!IO::granted()){
			return fail(403);
		}
		if(isset($_POST['io'])){
			static::action();
		} else {
			static::route();
		}
	}
	static function route(){
		switch(y(X::$path[2], "main")){
			case "main": IO::go("io/firewall/main"); break;
			default: fail(404); break;
		}
	}
	static function action(){
		switch($_POST["action"]){
			default: break;
		}
	}
}
<?php # IO_ASSEMBLY # Low-level physical works

namespace IO;

use X\System as System;
use X\Module as Module;

final class Assembly extends \Protector {

	static function init(){
		if(!IO::granted()){
			return fail(403);
		}
		if(isset($_POST['io'])){
			static::action();
		} else {
			static::route();
		}
	}
	static function route(){
		switch(y(X::$path[2], "main")){
			case "main": IO::go('io/assemble/main'); break;
			default: fail(404); break;
		}
	}
	static function action(){
		$literal = $_POST["target_board"];
		Katana::tree();
		if(!isset(X::$tree[$literal])){
			return deny("Invalid target board selected.");
		} else X::$node = $literal;
		Katana::cata($literal, true); # override catalist in memory
		$thread = abs(intval($_POST["target_thread"]));
		if(!isset(X::$cata[$thread])){
			return deny("Invalid target thread selected.");
		} else {
			X::$mode = X::$tree[X::$node]['mode'];
			X::$file = X::$tree[X::$node]['file'];
		}

		import(System::SQL, System::CRYPT);
		import(Module::BUILD, Module::GENERATE);

		switch($_POST["action"]){
			case "regenerate": switch($_POST["aim"]){
				case "boards": if(Generate::all()) new Note("Rebuilt all boards"); break;
				case "board": if(Generate::board($literal)) new Note("Rebuilt /{$literal}/"); break;
				case "threads": if(Generate::threads($literal)) new Note("Rebuilt all threads for /{$literal}/"); break;
				case "thread": if(Generate::thread($literal,$thread)) new Note("Rebuilt thread #{$thread} for /{$literal}/"); break;
				case "chunk": if(Generate::chunk($literal,$thread)) new Note("Rebuilt chunk #{$thread} for /{$literal}/"); break;
				case "catalogue": if(Generate::catalog($literal)) new Note("Rebuilt catalogue for /{$literal}/"); break;
				case "catalist": if(Generate::catalist($literal)) new Note("Rebuilt catalist for /{$literal}/"); break;
				case "info": if(Generate::info($literal)) new Note("Rebuilt info for /{$literal}/"); break;
				case "boardlist": if(Build::boardlist()) return new Note("Rebuilt board list"); break;
				case "pagelist": if(Build::pagelist()) new Note("Rebuilt page list"); break;
				default: new Note("Invalid request.", Note::FAILURE); break;
			} break;
			case "create": static::create(); break;
			case "delete":
				if($_SESSION["sure"] == "yes"){
					static::delete();
					$_SESSION["sure"] = null;
				} else static::areyousure();
				break;
			case "verify": $_SESSION["sure"] = $_POST["answer"]; jump(303); break;
			default: break;
		}
	}

	static function areyousure(){ IO::go('io/sure'); }

	static function create(){ # create new board space
		$lt = substr(Katana::escape($_POST['lt']),0,4);
		$rg = abs(intval($_POST['rg']));
		$anon = Katana::escape($_POST['anon']);
		$desc = Katana::escape($_POST['desc']);
		$bump = abs(intval($_POST['bump']));
		if(SQL::q("INSERT INTO `nodes` (`rg`,`lt`,`desc`,`bump`) VALUES ('{$rg}','{$lt}','{$desc}',{$bump})")){
			if(SQL::q("CREATE TABLE `node_{$lt}` (
				`id` int unsigned not null AUTO_INCREMENT primary key,
				`ip` varchar(60) not null default 'noip',
				`rl` int unsigned not null default 0,
				`dt` timestamp not null default CURRENT_TIMESTAMP,
				`bump` timestamp not null default CURRENT_TIMESTAMP,
				`spec` tinyint unsigned not null default 0,
				`trip` varchar(24) default null,
				`subj` varchar(64) default null,
				`text` text default null,
				`link` text default null,
				`file` text default null,
				`pass` varchar(64) not null
			)")){
				Build::chkfs($lt);
				Build::boardlist();
				Generate::board($lt);
				return;
			}
		}
		new Note("Unable to create /{$lt}/: something has gone wrong [CSL]");
	}

	static function delete(){
		import(System::SQL);
		import(Module::BUILD);
		$literal = escape($_POST["target_board"]);
		Katana::tree();
		if(!isset(X::$tree[$literal])){
			return deny("/{$literal}/ board does not exist");
		}
		SQL::q("DELETE FROM `nodes` WHERE `lt` = '{$literal}'");
		Build::chkfs($literal);
		Build::boardlist();
	}
}
<?php # IO_COMMANDS # High-level logical works

namespace IO;

use X\System as System;
use X\Module as Module;

final class Commands extends \Protector {

	static function init(){
		if(!IO::granted()){
			return fail(403);
		}
		if(isset($_POST['io'])){
			static::action();
		} else {
			static::route();
		}
	}
	static function route(){
		switch(y(X::$path[2], "main")){
			case "main": X::$view = new View("io/commands/main", "page"); break;
			default: fail(404); break;
		}
	}
	static function action(){
		$literal = $_POST["target_board"];
		Katana::tree();
		if(!isset(X::$tree[$literal])){
			return deny("Invalid target board selected.");
		} else X::$node = $literal;
		Katana::cata($literal, true); # override catalist in memory

		import(System::SQL, System::CRYPT);
		import(Module::BUILD, Module::GENERATE);
		
		switch($_POST["action"]){
			case "seal": switch($_POST["order"]){
				case "hi": static::seal($literal,$_POST["hi"]);
				case "lo": static::seal($literal,$_POST["lo"]);
				default: break;
			} break;
			case "cast":
				static::enchant($literal,$_POST["target_thread"],$_POST["spell"]);
				break;
			case "teleport":
				$dt = y($_POST["destination_thread"]);
				$db = y($_POST["destination_board"]);
				static::teleport($literal,$thread,$db,$dt);
				break;
			default: break;
		}
	}

	static function enchant($literal, $id, $spell){ # cast a spell on thread
		import(System::SQL);
		import(Module::GENERATE);

		$id = abs(intval($id));
		if(!isset(X::$cata[$id])){
			return deny("Invalid target thread selected.");
		} else {
			X::$mode = X::$tree[X::$node]['mode'];
			X::$file = X::$tree[X::$node]['file'];
		}

		$spell = X::$cata[$id][0];
		foreach($_POST["spell"] as $spell_fragment){
			if($spell&$spell_fragment) $spell -= $spell_fragment;
			else $spell += $spell_fragment;
		}

		if($spell < 0 || $spell > 0xFF){
			return deny("Could not seal /{$literal}/{$id}: fragments are broken.");
		}

		if(!SQL::q("UPDATE `node_{$literal}` SET `spec` = {$spell} WHERE `id` = {$id}")){
			return deny("Unable to cast spell on /{$literal}/{$id} (database issue).");
		}

		Generate::catalist($literal,$id,true); # pass id to rebuild specific thread and true to shift (e.g. thread is now invisible)
		Generate::thread($literal,$thread); # regenerate full version of thread
		Generate::chunk($literal,$thread); # regenerate thread head
		
		$data = SQL::kq("SELECT `id`,`rl`,`spec` FROM `node_{$literal}` WHERE `id` = {$id}", SQL::ONE|SQL::KEY);
		$thread = $data[1];

		$thread = intval($post[1]); if($thread == 0) $thread = $id;
		if($post[2]&$spell) $mode = $post[2] - $spell;
		else $mode = $post[2] + $spell;
		if(SQL::q("UPDATE `node_{$literal}` SET `spec` = {$mode} WHERE `id` = {$id}")){
			import(X::GENERATE);
			Generate::catalist($literal);
			Generate::thread($literal,$thread);
			Generate::chunk($literal,$thread);
			new Note("Enchanted >>/{$literal}/{$id} to {$spell}.");
		}
	}

	static function seal($literal, $seal_combine = false){ # recombine board seal
		import(System::SQL);
		import(Module::BUILD);

		$seal = X::$mode;
		$fragment = 0;
		foreach($seal_combine as $seal_fragment){
			$fragment = abs(intval($seal_fragment));
			if($seal&$fragment) $seal -= $fragment;
			else $seal += $fragment;
		}

		if($seal < 0 || $seal > 0xFFFF){
			return deny("Could not seal /{$literal}/: fragments are broken.");
		}

		if(!SQL::q("UPDATE `nodes` set `mode` = {$seal} where `lt` = '{$literal}'")){
			return deny("Unable to seal /{$literal}/ (database issue).");
		}
		
		Build::boardlist();

		new Note("Sealed /{$literal}/ to {$seal}.");
	}

	static function teleport($literal,$id,$destination_board,$destination_thread){ # move thread to another space
		if(!isset(X::$tree[$literal],X::$tree[$destination_board]))
			return deny("Illegal teleportation parameters");
		Katana::cata($literal);
		$id = abs(intval($id));
		$destination_thread = abs(intval($destination_thread));
		if(!isset(X::$cata[$id]))
			return deny("Thread #{$id} does not exist in /{$literal}/");
		import(System::SQL);

		$info = SQL::kq("SELECT * FROM `node_{$literal}` WHERE `id` = {$id}", SQL::ONE|SQL::NUM);
		if(empty($info)){
			return deny("Unable to find thread #{$id}");
		}
		SQL::q("DELETE FROM `node_{$literal}` WHERE `id` = {$id}");
		SQL::q("INSERT INTO `node_{$destination_board}` () VALUES ()");

		if(SQL::q("UPDATE `node_{$literal}` SET `nd` = '{$destination_board}', `rl` = {$destination_thread} WHERE `id` = {$id}")){
			Generate::catalist($literal) and Generate::pages($literal);
			Generate::catalist($destination_board) and Generate::pages($destination_board);
			new Note("Teleported >>/{$literal}/{$id} to >>/{$destination_board}/{$destination_thread}.");
			return;
		}
		return deny("Unable to teleport post.");
	}
}
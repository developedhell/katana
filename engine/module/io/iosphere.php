<?php # IO_WORLDNET # External social level gate

namespace IO;

use X\System as System;
use X\Module as Module;

final class Iosphere extends \Protector {

	static function init(){
		if(!IO::granted()){
			return fail(403);
		}
		if(isset($_POST['io'])){
			static::action();
		} else {
			static::route();
		}
	}
	static function route(){
		switch(y(X::$path[2], "main")){
			case "main": IO::go("io/iosphere/main"); break;
			default: fail(404); break;
		}
	}
	static function action(){
		switch($_POST["action"]){
			default: break;
		}
	}
}
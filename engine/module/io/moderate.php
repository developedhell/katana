<?php # IO_MODERATE # Internal social level operational works

namespace IO;

use X\System as System;
use X\Module as Module;
use X\Order as Order;

final class Moderate extends \Protector {

	# Content type identificators
	const POST = 1;
	const FILE = 2;
	const LINK = 4;
	const TEXT = 8;

	/**
	 * Initiation point
	 */
	static function init(){
		if(!IO::granted()){
			return fail(403);
		}
		if(isset($_POST['io'])){
			static::action();
		} else {
			static::route();
		}
	}

	/**
	 * Routing schema
	 */
	static function route(){
		switch(y(X::$path[2], "main")){
			case "main": IO::go("io/commands/main"); break;
			case "modlog": IO::go("io/moderate/log"); break;
			default: fail(404); break;
		}
	}

	/**
	 * Action schema
	 */
	static function action(){
		switch($_POST["action"]){
			case "ban":
				$method = 0;
				$target = "";
				$reason = "";
				switch($_POST["by"]){
					case "addr":
						$target = $_POST["target_ip"];
						$reason = $_POST["reason"];
						static::ban($target, $reason);
						break;
					case "post":
						$reason = $_POST["reason"];
						$literal = $_POST["target_board"];
						if(!isset(X::$tree[$literal])) break;
						$post = abs(intval($_POST["target_post"]));
						if($post <= 0) break;
						$target = SQL::kq("SELECT `ip` FROM `node_{$literal}` WHERE `id` = {$post}", SQL::ONE|SQL::NUM);
						if(empty($target)) break;
						$target = $target[0];
						static::ban($target, $reason);
						break;
					default: break;
				}
				static::ban($method, $target, $reason);
				break;
			case "cure": break;
			case "delete": import(Module::REQUEST); $id = intval($_POST["post"]); switch($_POST["element"]){
				case "post": if(Request::delete($id)) new Note("Deleted post #{$id}."); break;
				case "file": if(Request::delete($id,"file")) new Note("Deleted file at #{$id}."); break;
				case "link": if(Request::delete($id,"link")) new Note("Deleted link at #{$id}."); break;
				case "text": if(Request::delete($id,"text")) new Note("Deleted text at #{$id}."); break;
				default: break;
			} break;
			default: break;
		}
		return false;
	}
	static function log(){
		$q = "SELECT bh.id, bh.dt, bh.ends, bh.node, bh.post, bh.status, bh.reason, io.anon, io.call
			FROM `worlds`.`banhammer` as bh 
				LEFT JOIN `worlds`.`banhammer_io` as bhio ON bhio.bhid = bh.id
				LEFT JOIN `io`.`io` ON io.anon = bhio.guid
			ORDER BY `id` DESC LIMIT 16
		";
		foreach(SQL::sq($q, SQL::ARR|SQL::KEY) as $entry){ echo
			"<tr class='table__row table__label'>"
				."<td class='table__cell'><input type='checkbox' class='checkbox' name='B{$entry['id']}'></td>"
				."<td class='table__cell'><a href='/ban/{$entry['id']}' target='_blank'>{$entry['id']}</a></td>"
				."<td class='table__cell'><a href='/{$entry['node']}/read/{$entry['post']}' target='_blank'>#{$entry['post']}</a></td>"
				."<td class='table__cell'>".(intval($entry['status']) == 1 ? "Deleted": "Recovered")."</td>"
				."<td class='table__cell'>{$entry['reason']}</td>"
				."<td class='table__cell'>{$entry['dt']}</td>"
				."<td class='table__cell'>{$entry['ends']}</td>"
				."<td class='table__cell'><a href='/io/user/{$entry['anon']}'>{$entry['call']}</a></td>"
				."<td class='table__cell'><i class='icon icon_size_b icon-deny table__button' title='Undo'></i></td>"
			."</tr>";
		}
	}
	static function log_pagination(){
		$count = 0;
		$_count = SQL::kq("SELECT COUNT(*) FROM `worlds`.`banhammer`", SQL::ONE | SQL::NUM);
		if(!empty($_count)) $count = $_count[0];
		Katana::pagination(ceil($count/Order::MAXTHRDS), "io/moderation/log/page");
	}
	static function action_ban($target, $reason){
		if(SQL::q("INSERT INTO `banned` (`ip`, `reason`) VALUES ('{$target}', '{$reason}')")){
			new Note("Banned [{$target}]. Reason: {$reason}", Note::SUCCESS);
		} else {
			new Note("Unable to ban [{$target}]!", Note::FAILURE);
		}
	}
}
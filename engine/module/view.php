<?php

import(Plugin::v_general);
import(Plugin::v_board);

final class View extends Protector {

	use
		General,
		Board;
	
	private $type; 		# page type 	default: NONE
	private $layout; 	# page layout 	default: N2O
	private $content; 	# contents 		default: empty
	private $template;	# template 		default: empty
	public $assets; 	# assets 		default: empty

	public function __construct($content, $template, $type = null){
		$this->type = (null == $type) ? X::$type : $type;
		$this->layout = X::$opts['layt'];
		$this->content = $content;
		$this->template = $template;
		$this->assets = "";
		
		$this->load_base();
		$this->load_style();
		$this->load_scheme();
		$this->load_options();

		$this->template = ROOT."layout/{$this->layout}/tpl/{$template}.tpl";

		switch($this->type){
			case Page::IDLE:
			case Page::WIRE:
				$this->content = ROOT."static/{$content}.htm";
				break;
			case Page::FUNC:
			case Page::NCIS:
			case Page::NODE:
				$this->content = ROOT."dynamic/{$content}.php";
				break;
			case Page::NONE:
			default:
				fail(501);
				break;
		}
	}

	public function dump(){
		var_dump($this->content);
		var_dump($this->template);
		exit;
	}

	private function load_base(){
		$this->assets .= $this->css("assets/css/null");
		$this->assets .= $this->css("assets/css/nigram");
	}
	private function load_style(){
		if(X::$type != Page::NONE) switch(X::$type){
			case Page::IDLE: $this->assets .= $this->css("layout/{$this->layout}/css/struct/kt/page"); break;
			case Page::WIRE:
			case Page::NODE: $this->assets .= $this->css("layout/{$this->layout}/css/struct/kt/board"); break;
			case Page::FUNC: $this->assets .= $this->css("layout/{$this->layout}/css/struct/{$this->content}"); break;
			case Page::NCIS: $this->assets .= $this->css("layout/{$this->layout}/css/struct/{$this->template}"); break;
			default: break;
		}
	}
	private function load_scheme(){
		$theme = "layout/{$this->layout}/css/colors/"
            .(X::$mobi ? "mobile." : "")
            .X::$opts['schm'];
        $theme = file_exists(ROOT.$theme.".css") ? $theme : "layout/{$this->layout}/css/colors/default";
		$this->assets .= $this->css($theme);
		/*
		"nyanon":{"tone":"dim gray with strong turquoise","name":"Nyanon"},
		"heon":{"tone":"soft green tones with pastel beige and white","name":"Heon"},
		"nightfire":{"tone":"forest green with sky blue and wooden brown","name":"Nightfire"},
		"twilight":{"tone":"royal purple on dark base with reds and whites","name":"Twilight"},
		"shinonome":{"tone":"dark-gray, wheat, white and soft turquoise","name":"Shinonome"},
		"arcticice":{"tone":"light-gray with turquoise and marine","name":"Arctic Ice"},
		"moonlight":{"tone":"steel sky monotones with yellow","name":"Moonlight"},
		"mahoushoujo":{"tone":"soft rose and white","name":"Mahou Shoujo"}
		*/
	}
	private function load_options(){
		if(X::$opts['smth']){
			$this->assets .= '<style>[smooth]{'
				.'-moz-backface-visibility:hidden;'
				.'-moz-transition:all .10s ease-in-out;'
				.'-o-backface-visibility:hidden;'
				.'-o-transition:all .10s ease-in-out;'
				.'-webkit-backface-visibility:hidden;'
				.'-webkit-transition:all .10s ease-in-out;'
				.'backface-visibility:hidden;'
				.'transition:all .10s ease-in-out'
				.'}</style>';
		}
	}

	private function js($url){ return ""; }
	private function css($url){
		if(file_exists(ROOT.$url.".css")){
			return "<link rel='stylesheet' type='text/css' media='screen' href='/{$url}.css?v=".UPDATE."' />";
		} else if(X::VERBOSE){
			debug("Unable to locate asset: {$url}");
		}
		return null;
	}

	public function show(){
		if(empty($this->content)){
			fail(404);
		} else {
			if(!file_exists($this->template)){
				if(X::VERBOSE){
					debug("Unable to locate template: {$this->template}");
				}
				fail(404);
			}
			require_once $this->template;
		}
		return false;
	}

	/**
	 * Include content safely in the callee domain
	 */
	public function content(){
		if(file_exists($this->content)){
			switch($this->type){
				case Page::IDLE:
				case Page::WIRE:
					readfile($this->content);
					break;
				case Page::FUNC:
				case Page::NODE:
				case Page::NCIS:
					include_once $this->content;
					break;
				case Page::NONE:
				default:
					fail(501);
					break;
			}
		} else if(X::VERBOSE) {
			debug("Unable to locate content: {$this->content}");
		}
	}

	public function insert($path, $dynamic = false){
		if(file_exists($f = $dynamic
			? ROOT."dynamic/".$path.".php"
			: ROOT."static/".$path.".htm"
		)){
			include_once $f;
		} else if(X::VERBOSE){
			debug("Unable to locate inclusion: {$f}");
		}
	}

	public function menu(){
		$this->insert("kt/menu", true);
	}

	public function nodelist(){ $this->insert("node/nodelist", false); }

	public function pagelist(){ $this->insert("node/pagelist", false); }
}
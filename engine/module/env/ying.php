<?php # YING # Zero-level functions

function y($f,$g=null){ if(isset($f)&&!empty($f)) return $f; return $g; } # y-combinator basic logic
function dump($var){ echo "<pre>"; var_dump($var); echo "</pre>"; exit; }
function fail($code = null){ # go to HTTP error page w/ message
	$state = decode(Data::ERRORS);
	if(!isset($state[$code])) $code = 500;
	$status = $state[$code]['status'];
    $message = $state[$code]['message'];
    if(!headers_sent()){
        header($_SERVER["SERVER_PROTOCOL"]." {$code} {$status}");
	    header("Status: {$code} {$status}");
    }
	if(X::$ajax){
		AJAX::note_fail("{$code}! $message");
		exit;
	}
	include ROOT.'layout/error.tpl';
	exit;
}
function wait($percent, $status, $message){
	include ROOT.'layout/wait.tpl';
	exit;
}
function jump($code=303,$addr=null){ # redirect to some page
	if($_SERVER['REQUEST_URI'] == $addr){
		fail(508);
	}
	if($code == 303){
		if(empty($addr)){
			$addr = $_SERVER['HTTP_REFERER'];
		}
		if(X::$ajax){
			AJAX::action_redirect($addr);
			exit;
		}
		header($_SERVER["SERVER_PROTOCOL"]." 303 See Other");
		header("Location: {$addr}", false, 303);
		exit;
	} else {
		fail($code);
	}
	exit;
}
function deny($text, $code = 303, $addr = null){ # remember error and jump to another page immediatly
    new Note($text, Note::FAILURE);
    debug($text);
	jump($code, $addr);
	exit;
}
function stop($text, $error = "Something went wrong"){
	debug($text);
	deny(X::VERBOSE ? $text : $error);
	exit;
}
function debug($text){ # log message to file /system.log
	if(!X::VERBOSE) return false;
	$time = date('l jS \of F Y | h:i:s');
    $f = fopen(ROOT.'system.log','a+b');
    if(!$f){
        new Note("Unable to lock syslog", Note::FAILURE);
    }
    if(flock($f, LOCK_EX)){ fwrite($f, "[{$time}]\t".$text.PHP_EOL); fflush($f); flock($f, LOCK_UN); }
    else {
        if(class_exists('Note')){
            new Note("Unable to lock syslog", Note::FAILURE);
        } else {
            exit("Unable to lock syslog");
        }
    }
	fclose($f);
	return false;
}
function fatal(){
	if($E = error_get_last()){
		debug("Error {$E['type']} in {$E['file']} # {$E['line']}: {$E['message']}");
		fail(500);
	}
	exit;
}#register_shutdown_function('fatal');
function error($code,$text,$file,$line){
	if($code&error_reporting()){
		fatal();
		exit;
	}
}#set_error_handler('error');
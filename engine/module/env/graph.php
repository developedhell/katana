<?php

final class Graph {
	const SPHERE = "assets/data/graph/sphere"; # announce list
	const NEARBY = "assets/data/graph/nearby"; # neighbourhood
	const LINKED = "assets/data/graph/linked"; # connected
	const BANNED = "assets/data/graph/banned"; # disconnected

	static function echo(){}
	static function call(){}
	static function kido(){}
	static function conn(){}
	static function disc(){}
}
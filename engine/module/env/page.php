<?php

final class Page {
	const NONE = 0; # no type
	const IDLE = 1; # static page
	const FUNC = 2; # dynamic page
	const NODE = 3; # board page
	const NCIS = 4; # service page
}
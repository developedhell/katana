<?php

final class Chrona { # Common Human-Readable Object Notation Automation
	static function bytes($bytes){
		if($bytes < 1024) return $bytes." B";
		$bytes = sprintf("%.2f", $bytes/1024);
		if($bytes < 1024) return $bytes." KiB";
		$bytes = sprintf("%.2f", $bytes/1024);
		return $bytes." MiB";
	}
	static function binmode($a){ $r = 0; foreach($a as $f => $g){ $r <<= 1; $r |= $a[$f]; } return $r; }
	static function binarray($a,$m){ foreach($a as $f => $g){ $a[$f] = $m&1; $m>>=1; } return $a; }
}
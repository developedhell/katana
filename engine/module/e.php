<?php

class prototype__E implements \ArrayAccess {
	# Collection
    private $_ = null;
    private $value = null;

    # Constructor
    function __construct($value = null){
        $text = decode(Data::LOCALE.X::$conf['ln'].".lang");
        if(empty($text) || empty($text["e"])){
        	deny("Language module was not correctly loaded");
        } else {
        	$this->_ = $text["e"];
        }
        if(null != $value){
        	$this->value = $value;
        }
    }

    # Call override
    function __call($method_name, $args){
    	$cc = explode('__', $method_name);
    	$ns = $cc[0];
    	$fn = str_replace("_", "-", $cc[1]);
    	if(isset($this[$ns][$fn])){
    		return isset($this->value)
    			? $this->replace($this[$ns][$fn])
    			: $this[$ns][$fn];
    	}
    	return "Undefined error occured";
    }

    # Injector
    private function replace($e){ return str_replace("{VALUE}", $this->value, $e); }

    # ArrayAccess implementation
	function offsetSet($offset, $value){ if(is_null($offset)){ $this->_[] = $value; } else { $this->_[$offset] = $value; } }
	function offsetExists($offset){ return isset($this->_[$offset]); }
	function offsetUnset($offset){ unset($this->_[$offset]); }
	function offsetGet($offset){ return isset($this->_[$offset]) ? $this->_[$offset] : null; }
}

final class E {
	public static function __callStatic($method, $args){ return (new prototype__E($args[0]))->$method(); }
}
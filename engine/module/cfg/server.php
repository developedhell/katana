<?php # X_CONFIG # X global maintainable configuration

/**
 * Server-side configuration protocols
 */
final class ServerConfig implements \ArrayAccess {

    # Cookie anchor name and domain
    private $checkpoint = "server_settings";
    private $domain = ".".HOST;
    private $iv = "your-IV-goes-here";

    # Default configuration values
    private $_ = [
        "_lockdown" => false,
        "_verbose" => true,
        "ln" => "en_US",# system locale
        "gl" => "im",   # graphic library
        "ff" => [ # path to the ffmpeg/ffprobe (set null to invoke directly from shell)
            'probe' => null,
            'mpeg' => null 
        ],
        "db" => [
            "host" => "your-host",
            "name" => "your-db-name",
            "user" => "your-db-user",
            "pass" => "your-db-password",
            "port" => 99999,
            "sock" => "your-unix-socket",
            "type" => "your-db-driver-type"
        ],
        "kt" => [
            "argument" => XARG,     # version tag
            "prevsize" => 0xFF,     # image preview size (pixels)
            "filesize" => 0x1400000,# maximum allowed uploading file size (bytes)
            "textsize" => 0x2000,   # maximum allowed post text size (symbols)
            "interval" => 0x10,     # minimal delay between consequent posts from one ip address (seconds)
            "maxpages" => 0x0,      # maximum allowed page count for node
            "maxthrds" => 0x8,      # maximum allowed thread count per node index page
            "maxposts" => 0x4,      # maximum allowed post count per thread preview on node index page
            "maxfiles" => 0x4,      # maximum allowed file count per post
            "postcode" => false,    # C.A.P.T.C.H.A.
            "wiremaps" => true      # generate reflink maps for threads
        ],
        "io" => [
            "firewall" => [
                "spamfilter" => false
            ]
        ],
        "xt" => []
    ];
    
    /**
     * Constructor of configuration instance
     * Will automatically safely load configuration if it exists
     * or attempt to create a new file with default values
     */
    public function __construct(){
        if(file_exists(ROOT.Data::CONFIG.".json")){
            $this->load();
        } else {
            $this->save();
        }
    }

    /**
     * Save configuration to the file
     */
    public function save(){
        touch(ROOT.Data::CONFIG.".json");
        encode(Data::CONFIG, $this->_, base64_decode($this->iv), SEED);
    }
    
    /**
     * Load configuration from the file
     */
	public function load(){
		if(is_array($data = decode(Data::CONFIG, base64_decode($this->iv), SEED))){
			$this->apply($this->_, $data);
        }
    }

    /**
     * Expose general board configuration to the client side
     */
    public function expose(){
        setcookie($this->checkpoint,base64_encode(json_encode($this->_["kt"],true)),time()+31017600,'/',$this->domain,true,false);
    }
    
    /**
     * Apply one array values to another recursively (to $F from $G)
     */
    public function apply(array $F, array $G){
        foreach($G as $key => $val){
            if(isset($F[$key])){
                if(is_array($val)){
                    $this->apply($F[$key], $val);
                } else {
                    $F[$key] = $val;
                }
            }
        }
    }
    
    # ArrayAccess implementation
	public function offsetSet($offset, $value){ if(is_null($offset)){ $this->_[] = $value; } else { $this->_[$offset] = $value; } }
	public function offsetExists($offset){ return isset($this->_[$offset]); }
	public function offsetUnset($offset){ unset($this->_[$offset]); }
	public function offsetGet($offset){ return isset($this->_[$offset]) ? $this->_[$offset] : null; }
}
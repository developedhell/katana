<?php

/**
 * Client-side settings
 * (change appearance or l)
 */
final class ClientConfig extends \Protector implements \ArrayAccess {

	private $checkpoint = "client_settings-".XARG;
	private $domain = ".".HOST;

	private $_ = [
		'pass' => 'password',	# user password
		'layt' => 'n2o',		# layout name
		'schm' => 'hakase',		# layout colorscheme
		'smth' => 0,			# CSS3 smooth effects
		'play' => 0,			# floating player & gallery
		'infi' => 0,			# infinite scrolling
		'kbdi' => 0,			# keyboard hotkeys
		'ajax' => 0				# frontend interface
	];

	public function __construct(){
		if(!isset($_COOKIE[$this->checkpoint]) || empty($_COOKIE[$this->checkpoint])){
			$this->save();
		} else {
			$this->load();
		}
	}

	public function route(){
		switch(Katana::i(1,'main')){
			case 'main': X::$view = new View('kt/options', 'page'); break;
			case 'save': $this->post(); break;
			case 'export': fail(501); break;
			case 'import': fail(501); break;
			case 'toggle': $this->toggle(); break;
			default: fail(404); break;
		}
		return false;
	}
	public function save(){
		setcookie($this->checkpoint,base64_encode(json_encode($this->_,true)),time()+31017600,'/',$this->domain,true,false);
	}
	public function load(){
		if(is_array($data = json_decode(base64_decode($_COOKIE[$this->checkpoint]),true))){
			foreach($data as $key => $option){
				if(isset($this[$key])){
					$this[$key] = $option > 0 ? 1 : 0;
				}
			}
			if(isset($data['pass'])){ $this['pass'] = $data['pass']; } else { $this['pass'] = ""; }
			if(isset($data['layt'])){ $this['layt'] = escape($data['layt']); } else { $this['layt'] = "n2o"; }
			if(isset($data['schm'])){ $this['schm'] = escape($data['schm']); } else { $this['schm'] = "nyark"; }
		}
	}
	public function post(){
		import(System::CRYPT);
		if(empty($_POST['token']) || !NCC::token(session_id(), escape($_POST['token']))){
			new Note("Token mismatch.", Note::WARNING);
		} elseif(isset($_POST['send'])){
			foreach($this->_ as $set => $val){
				if(isset($_POST[$set])){ $this[$set] = 1; }
				else { $this[$set] = 0; }
			}
			$themes = join('|',array_keys(decode('assets/data/colorscheme')));
			if(preg_match("/^({$themes})$/i",$_POST['schm'])){ $this['schm'] = $_POST['schm']; }
			$this['layt'] = "n2o";
			$this->save();
			new Note("Settings applied", Note::SUCCESS);
		} else new Note("Invalid request", Note::WARNING);
		jump(303,(empty($_SERVER['HTTP_REFERER'])?'/options':$_SERVER['HTTP_REFERER']));
	}
	public function toggle(){
		$option = mb_strtoupper(X::$path[2]);
		if(isset($this[$option])){ $this[$option] = !$this[$option]; $this->save(); }
		else new Note("Invalid option `{$option}`");
		jump(303,(empty($_SERVER['HTTP_REFERER'])?'/options':$_SERVER['HTTP_REFERER']));
	}

	# ArrayAccess implementation
	public function offsetSet($offset, $value){ if(is_null($offset)){ $this->_[] = $value; } else { $this->_[$offset] = $value; } }
	public function offsetExists($offset){ return isset($this->_[$offset]); }
	public function offsetUnset($offset){ unset($this->_[$offset]); }
	public function offsetGet($offset){ return isset($this->_[$offset]) ? $this->_[$offset] : null; }
}
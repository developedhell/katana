<?php

class XT extends Protector {
    private static $repository = [];
    private static $available = [];

    /**
     * Initiation
     */
    public static function init(){
        static::$repository = decode(Data::XT_REPOSITORY);
        static::$available = static::list_available();
    }

    /**
     * Check whether the read manifest corresponds
     * to standart manifest schema "x-katana-xt-schema"
     */
    private static function normal($manifest) : bool {
        if(empty($manifest)){
            return false;
        }
        return isset(
            $manifest->url,
            $manifest->name,
            $manifest->author,
            $manifest->assets,
            $manifest->version,
            $manifest->dependencies,
            $manifest->configuration,
            $manifest->security,
            $manifest->security->sign
        );
    }

    /**
     * Checkes whether the read manifest is correctly signed
     */
    private static function signed($manifest) : bool {
        if(!NCC::match(static::$key.'#'.static::hash($xt_root), $manifest->security->sign)){
            return false;
        }
        return true;
    }

    /**
     * Reads the manifest according to the extension name
     */
    private static function manifest($M) : object {
        if(!file_exists($M)){
            return null;
        }
        libxml_use_internal_errors(true);
        $xml = simplexml_load_file($M);
        if(false === $xml){
            return null;
        }
        if(libxml_get_last_error()){
            if(X::VERBOSE){
                debug("[xt::manifest] Error parsing {$xt} manifest.")
                foreach(libxml_get_errors() as $err){
                    debug($err->message);
                }
            }
            return null;
        }
        libxml_clear_errors();
        return $xml;
    }

    /**
     * Checks whether the extension is currently installed and can be used.
     * @param $xt = Extension name
     */
    private static function valid($xt, $manifest) : bool {
        return isset(static::$repository[$xt]) # check if it is registered
            && static::normal($manifest) # has correct manifest
            && static::signed($manifest); # and it is signed
    }

    /**
     * Checks whether the extension exists in repository and is available
     */
    private static function exists($xt) : bool { return isset(static::$repository[$xt], static::$available[$xt]); }

    /**
     * Checks whether the server configuration class knows about this extension or not
     */
    private static function configured($xt) : bool { return isset(X::$conf['xt'][$xt]); }

    /**
     * Lists available extensions found in the extension directory that are valid by manifest
     * 
     */
    static function list_available() : array {
        $root = ROOT."engine/xt";
        $available = [];
        foreach (new FilesystemIterator($root) as $xt) {
            if($xt->isDir()){
                $xt_root = ROOT."engine/xt";
                $manifest = static::manifest("{$xt_root}/{$xt}.manifest");
                if(static::valid($xt, $manifest)){
                    $available[$xt] = $manifest->configuration;
                }
            }
        }
        return $available;
    }

    /**
     * Lists enabled extensions within available
     */
    static function list_enabled() : array {
        $enabled = [];
        if(!empty(X::$conf['xt'])){
            foreach(X::$conf['xt'] as $xt => $state){
                if($state && static::exists($xt)){
                    $enabled[] = $xt;
                }
            }
        }
        return $enabled;
    }

    /**
     * Lists disabled extensions within available
     */
    static function list_disabled() : array {
        $disabled = [];
        if(!empty(X::$conf['xt'])){
            foreach(X::$conf['xt'] as $xt => $state){
                if(!$state && static::exists($xt)){
                    $disabled[] = $xt;
                }
            }
        }
        return $disabled;
    }
    
    /**
     * Enables some extension if it is available
     * and was disabled
     */
    static function enable($xt) : void {
        if(static::$exists($xt)){
            X::$conf['xt'][$xt] = true;
            X::$conf->save();
        }
    }

    /**
     * Disables some extension if it is available
     * and was enabled
     */
    static function disable($xt) : void {
        if(static::$exists($xt)){
            X::$conf['xt'][$xt] = false;
            X::$conf->save();
        }
    }

    /**
     * Checks whether passed extension is enabled
     */
    static function enabled($xt) : bool { return (static::exists($xt) && static::configured($xt)) ? !!X::$conf['xt'][$xt] : false; }

    /**
     * Checks whether passed extension is disabled
     */
    static function disabled($xt) : bool { return !static::enabled($xt); }

    /**
     * Download and install the extension from a repository
     * (will be disabled by default)
     */
    static function install($xt) : void {
        if(static::exists($xt)){
            # curl here
            # unpack here
            static::$available = static::list_available();
            X::$conf['xt'][$xt] = false;
            X::$conf->save();
        }
    }

    /**
     * Uninstall and delete the extension
     */
    static function uninstall($xt) : void {
        if(static::exists($xt) && static::configured($xt)){
            # remove here
            static::$available = static::list_available();
            unset(X::$conf['xt'][$xt]);
            X::$conf->save();
        }
    }

}
<?php

/**
 * Default interface for any extension written for Katana X engine.
 * Provides some basic must-have operational functionality
 * that must be implemented for the sake of integration paradigm.
 * Any extension implementation must be static.
 * Aside from interface functions, extension can have any number of others.
 */
public interface IXT {
    public static function init() : void;
    public static function exec() : bool;
}
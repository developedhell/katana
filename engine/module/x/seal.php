<?php

namespace X;

final class Seal {

	/** default is: 3455
	 * 0xD7F
	 *
	 * this means POST+FILE+LINK=3328 + IMG+AUD+VID+FLA+TXT+ARC+DOC=127
	 * 3328+127=3455
	*/
	
	# board seals (higher order: global seals)
	const POST = 0x0100; # toggle posting on the board
	const NAME = 0x0200; # allow names (includes tripcodes)
	const FILE = 0x0400; # toggle file posting
	const LINK = 0x0800; # toggle link traitment (includes external links)
	const HIDE = 0x1000; # hide the board from visible index
	const TAGS = 0x2000; # there are tags on the board
	const GATE = 0x4000; # the board is actually a gate to Tor/Zeronet/etc.
	const XTRA = 0x8000; # this is customized board

	# board seals (lower order: file seals) (only applies with FILE flag enabled)
	const IMG = 0x0001; # image format (webp, jpeg, png, gif, bmp)
	const AUD = 0x0002; # audio container (weba, mp3, ogg, wav, flac + tracker modules)
	const VID = 0x0004; # video container (webm, mp4, ogv)
	const FLA = 0x0008; # macromedia flash (swf, flv)
	const TXT = 0x0010; # text data (text or source)
	const ARC = 0x0020; # compressed data (archives)
	const DOC = 0x0040; # documentary data (pdf, doc, rtf, xls)
	const ADD = 0x0080; # additional filetypes: torrent, apxpa, etc.

}
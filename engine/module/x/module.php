<?php

namespace X\Module;

namespace Module {
	# A/C
	const VIEW = "engine/module/view";
	const NOTE = "engine/module/env/note";
	const REGEX = "engine/module/env/regex";
	const CHRONA = "engine/module/env/chrona";
}

namespace Config {
	const SERVER = "engine/module/conf/server";
	const CLIENT = "engine/module/conf/client";
}

namespace Katana {
	# KT traits
	const OCHOBA = "engine/trait/kt/ochoba";
	const UNITY = "engine/trait/kt/unity";
	const MYTISCHI = "engine/trait/kt/mytischi";
	const ULTRA = "engine/trait/kt/ultra";
	const HARDCORE = "engine/trait/kt/hardcore";
	# KT classes
	const HANDLE = "engine/module/kt/handle";
	const REQUEST = "engine/module/kt/request";
	const GENERATE = "engine/module/kt/generate";
}

namespace IO {
	# IO traits
	const INFO = "engine/trait/io/info";
	const USER = "engine/trait/io/user";
	# IO classes
	const ASSEMBLY = "engine/trait/io/assembly";
	const COMMANDS = "engine/trait/io/commands";
	const FIREWALL = "engine/trait/io/firewall";
	const STRATEGY = "engine/trait/io/strategy";
	const IOSPHERE = "engine/trait/io/iosphere";
	const TERMINAL = "engine/trait/io/terminal";
}

namespace API {
	# API traits
	const CLIENT = "engine/trait/api/client";
	const SERVER = "engine/trait/api/server";
}

namespace AJAX {
	# AJAX traits
	const POST = "engine/trait/ajax/post";
}
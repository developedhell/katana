<?php

namespace X;

abstract class Extension {

    # GeSHi
    const GESHI = "engine/extension/geshi/geshi";

    # LaTeX
    const LATEX = "engine/extension/latex/latex";

    public static function enabled($xt_name){
        if(isset(X::$conf['xt'][$name])){
            return (X::$conf['xt'][$name] == true);
        }
        return false;
    }
	
}
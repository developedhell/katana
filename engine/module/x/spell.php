<?php

namespace X;

final class Spell {

	/** default is: 0
	 * 0x0
	*/

	# thread spells (spellcasted logic)
	const LOCKED = 0x1; # no one can answer in this thread
	const PINNED = 0x2; # thread is always at the top of the node index
	const CURSED = 0x4; # unbumpable curse
	const HIDDEN = 0x8; # one cannot see this thread on the node index
	const SHARDS = 0x10; # divided by pages
	const SPIRIT = 0x20; # only registered users can answer in this thread
	const UNDEAD = 0x40; # eternal thread
	const CUSTOM = 0x80; # this thread is customized special
	const ASTRAL = 0x100;

}
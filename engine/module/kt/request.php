<?php # KT_REQUEST # Interaction with database

use Katana\Seal as Seal;
use Katana\Spell as Spell;

import(Module::regex);

final class Request extends Protector {

	static function create(array $data){ #fail(423);
		if(empty(X::$node) || !isset(X::$cata, X::$mode)){
			deny(E::post__manifest());
		}

        $literal = X::$node;
        if(isset($data["lt"])){
            $literal = $data["lt"];
            if(!isset(X::$tree[$literal])){
                deny(E::post__literal($literal));
            }
        }

		$rl = $data["rl"];
		if(!($rl == 0 || isset(X::$cata[$rl]))){
			deny(E::post__thread($rl));
		}

		if(empty($data["text"]) && empty($data["file"]["name"]) && empty($data["link"])){
			deny(E::post__empty());
		}

		$mode = X::$cata[$data['rl']][0];
		if(!(X::$mode&Seal::POST)){
			deny(E::post__forbidden($literal));
		}

		import(System::CRYPT);
		if(!NCC::token(session_id(), escape($_POST["token"]))){
			deny(E::post__token());
		}

		if($rl == 0){ # enter thread after creating new one
			$data["noko"] = true;
		}

		$subj = "";
		$name = "";
		$trip = "";
		$info = "";
		$file = "";
		$link = "";
		$orig = "";
        $text = "";
        
		if(!empty($data["subj"])) $subj = mb_strcut($data["subj"],0,64);
		if(!empty($data["name"]) && X::$mode&Seal::NAME) list($name,$trip) = Handle::name($data["name"]);
		if(!empty($data["text"])){
			$text = Handle::text($data['text'],X::$node);
		}
		if(!empty($data["pass"])) $pass = $data["pass"];
		else if(!empty($data["link"]) && X::$mode&Seal::LINK){
			$temp = Handle::link($data["link"]);
			$link = addslashes($temp["link"]);
			$orig = addslashes($temp["name"]);
			$info = addslashes($temp["info"]);
		}
		
        if(empty($pass)) $pass = NCC::rstr(16); # warning, irreversible entropy
        import(System::CLIENT);
		$ip = NCC::hash(Client::ip().$pass);
		$pass = NCC::hash($pass);

		$pid = Q::post__push($literal, $ip, $rl, $name, $trip, $subj, $text, $pass);
		
		$tid = ($rl == 0 ? intval($pid) : $rl);
		Handle::references(X::$node, $tid, $pid, $data["text"]);

		if(X::$mode&Seal::FILE){
			Handle::files($literal, $tid, $pid, $data["file"]);
		}		
		
		$mpc = X::$tree[X::$node]["bump"];
		preg_match("/^\d+/", X::$cata[$tid][1], $pc);
		$pc = $rl ? y(intval($pc[0])+1,$mpc) : 1;
		$t_shift = !(($data["sage"] || $pc >= $mpc || $mode&Spell::CURSED) && $rl);

		if($t_shift){
			Q::wire__bump($literal, $tid);
			Generate::catalist_order(X::$node);
        }
        Generate::catalist_entry(X::$node, $tid);
		if($rl){
            Generate::append(X::$node, $tid, $pid);
        } else {
            Generate::thread(X::$node, $tid);
        }
		Generate::chunk(X::$node, $tid);

		return $pid;
	}
	static function read($literal, $pid){
		if(empty($literal) || !isset(X::$tree[$literal], X::$mode)){
			deny("[request::read] Invalid prerequisites ({$literal}/{$pid})");
		}
		$tid = self::parent($literal, $pid);
		Katana::node($literal);
		if(!isset($tid, X::$cata[$tid])){
			deny("Post #{$pid}::{$tid} does not exist");
		}
		$post = Q::post__read($literal, $pid);
		if($post){
			$files = Q::post__files($literal, $tid, $pid);
			return Generate::prepare($literal, $tid, $post);
		}
		deny("(4004) Unable to find the post ({$literal}/{$pid})");
	}
	static function update($id, $mode=0){ fail(501); return false; }
    static function delete($id, $mode=0){ fail(501); return false; }
	static function parent($literal, $id){
		if(!isset(X::$tree[$literal])){
            deny(E::post__literal($literal));
        }
		$search = Q::post__find($literal, $id);
		if(empty($search)){
            deny(E::post__fail($id));
        }
		return ((intval($search[1]) == 0) ? $search[0] : $search[1]);
	}
}
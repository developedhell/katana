<?php

use Katana\Seal as Seal;

import(Plugin::ajax_post);

final class AJAX extends Protector {

	use Post;

	const MESSAGE = "notes";
	const ACTION = "action";
	const DATA = "response";

	/**
	 * Message pool
	 */
	private static $pool = array(
		"notes" => array(),
		"action" => array(),
		"response" => array()
	);

	/**
	 * Check headers
	 */
	static function check(){ if($H = apache_request_headers()){ return isset($H['X-AJAX-Perform']); } }

	/**
	 * Message processing
	 */
	static function push($node, $info){ if(isset(static::$pool[$node])) static::$pool[$node][] = $info; return false; }
	static function eject(){ echo json_encode(static::$pool, JSON_UNESCAPED_UNICODE); return false; }
	static function escape($string){ return nl2br(addslashes(htmlspecialchars(trim($string)))); }

	/**
	 * Note simplex
	 */
	static function note($text, $flag = Note::SUCCESS){ return static::push(static::MESSAGE, [ "type" => $flag, "text" => $text ]); }
	static function note_fail($text){ return static::note($text, Note::FAILURE); }
	static function note_warn($text){ return static::note($text, Note::WARNING); }

	/**
	 * Actions
	 */
	static function action_redirect($address){ return static::push(static::ACTION, [ "type" => "redirect", "address" => urlencode($address) ]); }
	static function action_response($data){ return static::push(static::DATA, [ "data" => $data ]); }
	static function action_log($text){ return static::push(static::ACTION, [ "type" => "log", "data" => $text ]); }
	static function action_log_array(array $text){ foreach($text as $note){ static::action_log($note); } return false; }

	/**
	 * Entry point
	 */
	static function init(){
		if(isset($_POST)){
			static::action();
		} else {
			static::route();
		}
	}

	/**
	 * Routing schema
	 */
	static function route(){
		switch(X::$path[0]){
			case 'ping': static::note_fail("pong"); break;
			case 'options': exit('got options'); break;
			case 'control': break;
			default: if(Katana::tree() && isset(X::$tree[X::$path[0]])){ # welcome on board
				X::$node = X::$path[0];
				X::$file = X::$tree[X::$node]['file'];
				X::$mode = X::$tree[X::$node]['mode'];
				Katana::cata(X::$node);
				switch(X::$path[1]){
					case 'create': static::action_response(static::post_create()); break;
					case 'delete': static::note_fail("Not implemented"); break;
					case 'update': static::note_fail("Not implemented"); break;
					case 'read': static::action_response(static::post_read(intval(X::$path[2]))); break;
					default: break;
				}
			} break;
		}
		static::eject();
		return false;
	}
}
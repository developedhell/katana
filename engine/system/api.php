<?php

use X\Seal as Seal;

import(Module::api_client);
import(Module::api_server);

final class API extends Protector {

    use
        CLIENT,
        SERVER;

    private $nodeinfo;

    private static $request = [
        "method" => null,
        "callme" => null,
        "expect" => null,
        "caller" => [
            "server" => null,
            "client" => null
        ],
        "callee" => [
            "intent" => null,
            "target" => null,
            "demand" => null
        ],
        "result" => null
    ]
    private static $response = null;

    /**
     * Initiation point
     */
    static function init(){
        Katana::tree();
        $nodeinfo = decode(Data::RAID);
        if(!empty($_POST["token"])){
            static::$request["method"] = "POST";
        } elseif(!empty($_GET["token"])) {
            static::$request["method"] = "GET";
        } else {
            /* drop error */ return null;
        }
        static::route();
    }

    /**
	 * Routing schema
	 */
	static function route(){
        static::analyze();
        switch(static::$request["callme"]){
            case "server":
                static::$response = static::server_route()
                break;
            case "client":
                static::$response = static::client_route();
                break;
            default: /* drop error */ return false;
        }
        return empty(static::$response) 
            ? null 
            : json_encode(static::$response, JSON_UNESCAPED_UNICODE);
    }

    static function node__literal($node_id){
        foreach (X::$tree as $lt => $info) {
            if($info['id'] == $node_id){
                $literal = $info['lt'];
                if(isset($literal, static::$nodeinfo[$literal])){
                    return $literal;
                }
            }
        }
        return null;
    }

    /**
     * Analyze the variables
     */
    static function analyze(){
        $callme = null;
        $expect = null;
		switch(static::$request["method"]){
            case "POST":
                $callme = $_POST["callme"];
                $expect = y($_POST["expect"], "json");
                static::$request["callee"]["intent"] = y($_POST["intent"],"");
                static::$request["callee"]["target"] = y($_POST["target"],"");
                static::$request["callee"]["demand"] = y($_POST["demand"],"");
                break;
            case "GET": # isle.domain/intent/target/demand?callme=server&expect=json
                $callme = $_GET["callme"];
                $expect = y($_GET["expect"], "json");
                static::$request["callee"]["intent"] = y(X::$path[2],"");
                static::$request["callee"]["target"] = y(X::$path[3],"");
                static::$request["callee"]["demand"] = y(X::$path[4],"");
                break;
            default: /* drop error */ break;
        }
        switch($callme){
            case "html":
            case "json":
                static::$request["callme"] = $callme;
                break;
            default: /* drop error */ break;
        }
        switch($expect){
            case "client":
            case "server"
                static::$request["expect"] = $expect;
                break;
            default: /* drop error */ break;
        }
    }
}
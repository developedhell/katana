<?php # SQL # Database toolkit

final class SQL extends Protector {
	const ARR  = 0b0001;
	const NUM  = 0b0010;
	const KEY  = 0b0100;
	const ONE  = 0b1000;

	private static $instance = false;
	static function flush(){ while(static::$instance->more_results()) if($_result=static::$instance->store_result()) $_result->free(); }
	static function check(){
		if(!static::$instance)
			static::$instance = static::auto_connect();
		if(!static::$instance)
			throw new Note('SQL Exception: unable to instantiate a connection.');
	}
	static function last(){ return static::$instance->insert_id; }
	private static function internal_connect($host, $user, $pass, $name){
		if(static::$instance) return static::$instance;
		mysqli_report(MYSQLI_REPORT_ERROR|MYSQLI_REPORT_STRICT);
		try {
			static::$instance = new mysqli($host, $user, $pass, $name);
		} catch (Exception $E) {
			if(static::$instance->connect_error)
				throw new Note('SQL Exception ('.static::$instance->connect_errno.'): '.static::$instance->connect_error);
			else throw new Note($E->getMessage());
		}
		if(!static::$instance->set_charset("utf8"))
			throw new Note('SQL Exception: error loading UTF-8 charset.');
		return static::$instance;
	}
	static function auto_connect(){
		return static::internal_connect(X::$conf['db']['host'],X::$conf['db']['user'],X::$conf['db']['pass'],X::$conf['db']['name']);
	}
	static function connect($host, $user, $pass, $name){
		return static::internal_connect($host, $user, $pass, $name);
	}
	static function q($query){
		if(!is_string($query) || empty($query)) return [];
		static::check();
		try { $result = static::$instance->query($query); } catch (Exception $E) {
			throw new Note('SQL Exception ('.$E->getCode().')'
				.(X::VERBOSE ? ' at '.$E->getFile().'#'.$E->getLine() : '')
				.': '.$E->getMessage());
		}
		return $result;
	}
	static function kq($query, $key){
		if(!is_string($query) || empty($query)) return [];
		static::check();
		try { $result = static::$instance->query($query); } catch (Exception $E) {
			throw new Note('SQL Exception ('.$E->getCode().')'
				.(X::VERBOSE ? ' at '.$E->getFile().'#'.$E->getLine() : '')
				.': '.$E->getMessage());
		} finally { if(!$result) return []; }
		if($result && $key){
			if($key&static::ARR){
				$return = array();
				if($key&static::KEY){ while($data = $result->fetch_array(MYSQLI_ASSOC)) $return[] = $data; return $return; }
				if($key&static::NUM){ while($data = $result->fetch_array(MYSQLI_NUM)) $return[] = $data; return $return; }
			}
			if($key&static::ONE){
				if($key&static::KEY) return $result->fetch_array(MYSQLI_ASSOC);
				if($key&static::NUM) return $result->fetch_array(MYSQLI_NUM);
			}
		}
		static::flush();
		return $result;
	}
	static function sq($query, $key){ foreach(SQL::kq($query, $key) as $E) yield $E; }
	static function transact($stack){
		if(!is_array($stack)) return [];
		$results=array();
		static::check();
		static::$instance->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
		static::$instance->autocommit(FALSE);
		try { foreach($stack as $q) $results[]=static::$instance->query($q); } catch (Exception $E) { throw new Note($E->getMessage()); }
		static::$instance->commit();
		static::flush();
		return $results;
	}
}
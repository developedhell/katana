<?php # IO # w00t w00t g0t r00t

import(Plugin::io_user); # authentication and user information
import(Plugin::io_info); # system information and analysis

class IO extends Protector {

	use
		User,
		Info;

	public static $API = "https://io.nigma.city"; # ngmt.systems

	/**
	 * Initiation point
	 */
	public static function init(){
		if(!empty($_POST["io"])){
			static::action();
		} else {
			static::route();
		}
	}

	/**
	 * @internal Routing schema
	 */
	private static function route(){
		X::$type = Page::NCIS;
		$argn = y(X::$path[1],'main');
		if(X::$path[0] == "modlog"){
			$argn = "moderate";
			X::$path[2] = "log";
		}
		switch($argn){
			case "in":
				X::$view = new View('io/in', 'io/page');
				break;
			case "out":
				static::out();
				break;
			case "main":
				if(static::granted()){
					X::$view = new View('io/main', 'io/page');
				} else {
					X::$view = new View('io/in', 'io/page');
				}
				break;
			case "assembly":
				import(Module::ASSEMBLY);	# NC/NS - Assembly (construction material)
				Assembly::init();
				break;
			case "commands":
				import(Module::COMMANDS);	# NC/NS - Commands (nerve center)
				Commands::init();
				break;
			case "firewall":
				import(Module::FIREWALL);	# NC/NS - Firewall (digital precision)
				Firewall::init();
				break;
			case "strategy":
				import(Module::STRATEGY);	# NC/NS - Strategy (natural decisions)
				Strategy::init();
				break;
			case "terminal":
				import(Module::TERMINAL);	# NCIS/IO - L.A.I.N. (Local Access Interface of Noosphere)
				Terminal::init();
				break;
			default: fail(404); break;
		}
		X::$view->show();
	}

	/**
	 * Action schema
	 */
	private static function action(){
		switch(y($_POST["io"], "fuck")){
			case "in": static::in(); break;
			case "out": static::out(); break;
			case "register": jump(303, static::$API.'/register'); break;
			case "edit": jump(303, static::$API.'/edit'); break;
			case "fuck":
				new Note('А ну ублюдок мать твою иди сюда думал меня выебать да я сам тебя трахну', Note::FAILURE);
			default: static::route(); break;
		}
	}

	private static function api($data){
		$postdata = ['data' => static::api_pack($data)];
		$postvars = '';
		$sep = '';
		foreach($postdata as $key => $value){
			$postvars.= $sep.urlencode($key).'='.urlencode($value);
			$sep = '&';
		}

		$ch = curl_init();

		curl_setopt($ch,CURLOPT_URL,static::$API.'/api');
		curl_setopt($ch,CURLOPT_POST,count($postdata));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

		$result = curl_exec($ch);

		curl_close($ch);

		return static::api_unpack($result);
	}

	private static function api_unpack($data){
		if(empty($data)) return false;
		return json_decode(base64_decode($data), true);
	}

	private static function api_pack($data){
		return base64_encode(json_encode($data, JSON_UNESCAPED_UNICODE));
	}
}
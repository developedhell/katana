<?php

final class Q extends Protector {
	private static $dsn = "";
	private static $opt = [];
	private static $pdo = null;
	private static $set = null;
	private static $str = [
		'nt_node',
		'nt_file',
		'nt_data'
	];
	private static $fft = "`files`";

	private static $store = [
		"post" => [
			"push" => "INSERT INTO `{nt_node}` 
				(`ip`,`rl`,`name`,`trip`,`subj`,`text`,`pass`) 
				VALUES 
				(:ip, :rl, :name, :trip, :subj, :tx, :pass)",
			"read" => "SELECT {nt_data} 
				FROM `{nt_node}` 
				WHERE `id` = :post_id",
			"find" => "SELECT `id`, `rl` 
				FROM `{nt_node}` 
				WHERE `id` = :post_id"
		],
		"file" => [
			"push" => "INSERT INTO {nt_file} 
				(`node`, `wire`, `post`, `name`, `orig`, `info`, `size`, `hash`) 
				VALUES 
				(:node_id, :wire_id, :post_id, :name, :orig, :info, :size, :hash)",
			"read" => "SELECT `name`, `orig`, `info`, `size` 
				FROM {nt_file} 
				WHERE `node` = :node_id 
					AND `wire` = :wire_id 
					AND `post` = :post_id",
			"find" => "SELECT `name`, `orig`, `info`, `size` 
				FROM {nt_file}
				WHERE `node` = :node_id
					AND `hash` = :hash_id"
		],
		"wire" => [
			"bump" => "UPDATE {nt_node} 
				SET `bump` = CURRENT_TIMESTAMP 
				WHERE `id` = :wire_id",
			"rlim" => "SELECT `id`, `spec`, `bump` 
				FROM {nt_node} 
				WHERE `rl` = 0 
				ORDER BY `bump` DESC 
				LIMIT :skip,:take",
			"read" => "SELECT `id`, `spec`, `bump` 
				FROM {nt_node} 
				WHERE `rl` = 0 
				ORDER BY `bump` DESC",
			"resp" => "SELECT `id`, `spec`, `bump` 
				FROM {nt_node} 
				WHERE `rl` = 0 AND `spec` & :cast != 0 
				ORDER BY `bump` DESC",
			"psid" => "SELECT `id` 
				FROM {nt_node} 
				WHERE `rl` = :post_id",
			"psto" => "SELECT {nt_data} 
				FROM {nt_node} 
				WHERE `rl` = :post_id",
			"text" => "SELECT `id`,`text` 
				FROM {nt_node} 
				WHERE `rl` = :post_id",
			"part" => "SELECT `chunk`.* 
				FROM ( 
					SELECT {nt_data} 
					FROM {nt_node} 
					WHERE `rl` = :wire_id 
					ORDER BY `id` DESC 
					LIMIT :skip,:take 
				) as `chunk` 
				ORDER BY `id` ASC"
		],
		"cata" => [
			"post" => "SELECT COUNT(*) as count 
					FROM {nt_node} 
					WHERE `rl` = :wire_id",
			"file" => "SELECT COUNT(*) as count 
					FROM {nt_file} 
					WHERE `name` != '' 
						AND `node` = :nt_node 
						AND (
							`wire` = :wire_id_wire 
							OR 
							`post` = :wire_id_post
						)",
			"solo" => "
				SELECT cp.count_post FROM (
					SELECT COUNT(*) as count_post 
					FROM {nt_node} 
					WHERE `rl` = :wire_id
				) as cp, cf.count_file FROM (
					SELECT COUNT(*) as count_file 
					FROM {nt_file} 
					WHERE `name` != '' 
						AND `node` = :nt_node 
						AND (
							`wire` = :wire_id_wire 
							OR 
							`post` = :wire_id_post
						)
				) as cf
			"
		]
	];

	public static function init($literal = null){
		if(!isset($literal)){
			throw new Note(E::db__literal());
		}

		$user = "";
		$pass = "";
		$host = "";
		$port = -1;
		$db = "";
		if(isset(Graph::$select['private'][$literal])){
			$user = X::$conf['db']['user'];
			$pass = X::$conf['db']['pass'];
			$host = X::$conf['db']['host'];
			$port = X::$conf['db']['port'];
			$db = X::$conf['db']['name'];
		} elseif(isset(Graph::$select['public'][$literal])){
			# TODO replace with graph NAC decoded
			throw new Note("Noosphere access denied");
			$user = "";
			$pass = "";
			$host = "";
			$port = 99999;
			$db = "";
		} else {
			throw new Note(E::db__node($literal));
		}

		static::$dsn = "mysql:"
			." host=".$host
			.":port=".$port
			.";dbname=".$db
			.";charset=utf8";
		static::$opt = [
			PDO::ATTR_ERRMODE			 => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
			PDO::ATTR_EMULATE_PREPARES	 => false
		];
		try {
			static::$pdo = new PDO(static::$dsn, $user, $pass, static::$opt);
			static::$set = "`id`,unix_timestamp(`dt`) as `unix`,`rl`,`subj`,`text`,`spec`,`name`";
		} catch (PDOException $PE){
			throw new Note("Unable to instantiate a DB connection. ".(X::VERBOSE ? $PE->getMessage() : ""));
			fail(500);
		}
	}

	private static function replace($q, array $repl){
		foreach(static::$str as $demo){
			if(isset($repl[$demo])){
				$q = str_replace("{{$demo}}", $repl[$demo], $q);
			}
		}
		return $q;
	}

	private static function prepare($q,$r) : PDOStatement {
		return self::$pdo->prepare(self::replace($q,$r));
		/*try {
			
		} catch (PDOException $e){
			deny(E::post__fail());
		}*/
		return null;
	}

	private static function identify__node($literal) : int {
		if(isset(X::$tree[$literal])){
			return X::$tree[$literal]['id'];
		}
		return 0;
	}

	static function lastid(){ return static::$pdo->lastInsertId(); }

	static function wire__chunk($literal, $tid, $limit) : Iterator {
		$stm = self::prepare(self::$store['wire']['part'],[
			'nt_node' => "node_{$literal}",
			'nt_data' => self::$set
		]);
		$stm->bindValue(':wire_id', abs(intval($tid)), PDO::PARAM_INT);
		$stm->bindValue(':skip', 0, PDO::PARAM_INT);
		$stm->bindValue(':take', abs(intval($limit)), PDO::PARAM_INT);
		if($stm->execute()){
			while($row = $stm->fetch()){
				yield $row;
			}
		}
		return null;
	}

	static function wire__bump($literal, $tid) : bool {
		$stm = self::prepare(self::$store['wire']['bump'],[
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':wire_id', abs(intval($tid)), PDO::PARAM_INT);
		return $stm->execute();
	}

	static function wire__posts_text($literal, $tid) : Iterator {
		$stm = self::prepare(self::$store['wire']['text'], [
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':post_id', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			while($row = $stm->fetch(PDO::FETCH_NUM)){
				yield $row[0];
			}
		}
		return null;
	}

	static function wire__posts($literal, $tid) : Iterator {
		$stm = self::prepare(self::$store['wire']['psid'], [
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':post_id', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			while($row = $stm->fetch(PDO::FETCH_NUM)){
				yield $row[0];
			}
		}
		return null;
	}

	static function wire__posts_all($literal, $tid) : Iterator {
		$stm = self::prepare(self::$store['wire']['psto'], [
			'nt_data' => self::$set,
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':post_id', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			while($row = $stm->fetch()){
				yield $row;
			}
		}
		return null;
	}

	static function wire__posts_enumerate($literal, $tid) : Iterator {
		$stm = self::prepare(self::$store['post']['read'], [
			'nt_data' => self::$set,
			'nt_node' => "node_{$literal}"
		]);
		foreach(static::wire__posts($literal, $tid) as $post_id){
			$stm->bindValue(':post_id', $post_id, PDO::PARAM_INT);
			if($stm->execute()){
				yield $stm->fetch();
			}
		}
		return null;
	}

	static function wire__all_limited($literal, $limit) : array {
		$limit = abs(intval($limit));
		if($limit > 0){
			$stm = self::prepare(self::$store['wire']['rlim'],[
				'nt_node' => "node_{$literal}"
			]);
		} else {
			$stm = self::prepare(self::$store['wire']['read'],[
				'nt_node' => "node_{$literal}"
			]);
		}
		if($stm->execute()){
			return $stm->fetchAll();
		}
		return null;
	}

	static function wire__all_enchanted($literal, $spell) : array {
		$stm = self::prepare(self::$store['wire']['resp'],[
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':cast', intval($spell), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetchAll();
		}
		return null;
	}

	static function post__push($literal, $ip, $rl, $name, $trip, $subj, $text, $pass) : int {
		$stm = self::prepare(self::$store['post']['push'], [
			'nt_node' => "node_{$literal}",
		]);
		$stm->bindValue(':ip',   $ip,   PDO::PARAM_STR);
		$stm->bindValue(':rl',   $rl,   PDO::PARAM_INT);
		$stm->bindValue(':name', $name, PDO::PARAM_STR);
		$stm->bindValue(':trip', $trip, PDO::PARAM_STR);
		$stm->bindValue(':subj', $subj, PDO::PARAM_STR);
		$stm->bindValue(':tx',   $text, PDO::PARAM_STR);
		$stm->bindValue(':pass', $pass, PDO::PARAM_STR);
		if($stm->execute()){
			return static::lastid();
		}
		/*try {
			
		} catch (PDOException $e){
			deny(E::post__submission());
		}*/
		return 0;
	}

	static function post__read($literal, $pid) : array {
		$stm = self::prepare(self::$store['post']['read'], [
			'nt_data' => self::$set,
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':post_id', abs(intval($pid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetch();
		}
		return null;
	}

	static function post__find($literal, $pid) : array {
		$stm = self::prepare(self::$store['post']['find'], [
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':post_id', abs(intval($pid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetch(PDO::FETCH_NUM);
		}
		return null;
	}

	static function post__files($literal, $tid, $pid) : array {
		$stm = self::prepare(self::$store['file']['read'], [
			'nt_file' => self::$fft
		]);
		$stm->bindValue(':node_id', intval(X::$tree[$literal]['id']), PDO::PARAM_INT);
		$stm->bindValue(':wire_id', abs(intval($tid)), PDO::PARAM_INT);
		$stm->bindValue(':post_id', abs(intval($pid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetchAll();
		}
		return null;
	}

	static function file__push($literal, $lid, $tid, $pid, $unique_name, $original_name, $finfo, $size, $hash) : bool {
		$stm = self::prepare(self::$store['file']['push'], [
			'nt_file' => self::$fft
		]);
		$stm->bindValue(':node_id', $lid, PDO::PARAM_INT);
		$stm->bindValue(':wire_id', $tid, PDO::PARAM_INT);
		$stm->bindValue(':post_id', $pid, PDO::PARAM_INT);
		$stm->bindValue(':name', $unique_name, PDO::PARAM_STR);
		$stm->bindValue(':orig', $original_name, PDO::PARAM_STR);
		$stm->bindValue(':info', $finfo, PDO::PARAM_STR);
		$stm->bindValue(':size', $size, PDO::PARAM_INT);
		$stm->bindValue(':hash', $hash, PDO::PARAM_STR);
		return $stm->execute();
	}

	static function file__find_hash($literal, $lid, $hash) : array {
		$stm = self::prepare(self::$store['file']['find'], [
			'nt_file' => self::$fft
		]);
		$stm->bindValue(':node_id', $lid, PDO::PARAM_INT);
		$stm->bindValue(':hash_id', $hash, PDO::PARAM_STR);
		if($stm->execute()){
			return $stm->fetchAll();
		}
		return null;
	}

	static function cata__post($literal, $tid) : array {
		$stm = self::prepare(self::$store['cata']['post'], [
			'nt_node' => "node_{$literal}"
		]);
		$stm->bindValue(':wire_id', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetch();
		}
		return null;
	}

	static function cata__file($literal, $tid) : array {
		$stm = self::prepare(self::$store['cata']['file'], [
			'nt_file' => self::$fft
		]);
		$stm->bindValue(':nt_node', $literal, PDO::PARAM_STR);
		$stm->bindValue(':wire_id_wire', abs(intval($tid)), PDO::PARAM_INT);
		$stm->bindValue(':wire_id_post', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetch();
		}
		return null;
	}

	static function cata__solo($literal, $tid) : array {
		$stm = self::prepare(self::$store['cata']['solo'], [
			'nt_node' => "node_{$literal}",
			'nt_file' => self::$fft
		]);
		$stm->bindValue(':wire_id', abs(intval($tid)), PDO::PARAM_INT);
		$stm->bindValue(':nt_node', $literal, PDO::PARAM_STR);
		$stm->bindValue(':wire_id_wire', abs(intval($tid)), PDO::PARAM_INT);
		$stm->bindValue(':wire_id_post', abs(intval($tid)), PDO::PARAM_INT);
		if($stm->execute()){
			return $stm->fetchAll();
		}
		return null;
	}
}
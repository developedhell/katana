<?php # YANG # Predefined system layer

abstract class Protector {
	function __construct(){ return false; }
	function __destruct(){ return false; }
	function __clone(){ return false; }
	function __sleep(){ return false; }
	function __wakeup(){ return false; }
	function __invoke($directive=null){ return $directive; }
	function __set_state(){ return false; }
	function __toString(){ return false; }
}
<?php # YING # Zero-level functions

function y($f,$g=null){ if(isset($f)&&!empty($f)) return $f; return $g; } # y-recombinator basic logic
function dump($var){ echo "<pre>"; var_dump($var); echo "</pre>"; exit; }
function insert($path, $dynamic = false){
	$f = $dynamic
		? ROOT."dynamic/".$path.".php"
		: ROOT."content/".$path.".htm";
	if(file_exists($f)) include_once $f;
}
function import(...$f){ foreach($f as $n => $g){ if(file_exists($h = ROOT.$g.".php")){ include_once $h; } } return true; }
function decode($f,$k=null){
	if(file_exists($g = ROOT."{$f}.json")){
		if(null == $k){
			return json_decode(file_get_contents($g),true);
		} else {
			if(class_exists('NCC')){
				return json_decode(NCC::decrypt(file_get_contents($g), $k, NCC::num(16)), true);
			}
		}
	}
    return null;
}
function encode($f,$h,$k=null){
	if(file_exists(ROOT."{$f}.json")){
		if(null == $k){
			return commit($f.".json", json_encode($h, JSON_UNESCAPED_UNICODE));
		} else {
			if(class_exists('NCC')){
				return commit($f.".json", NCC::encrypt(json_encode($h, JSON_UNESCAPED_UNICODE), $k, NCC::num(16)));
			}
		}
	}
}
function commit($path,$data,$append=false){
	$t = 'w'; if($append) $t = 'a';
	$f=fopen(ROOT.$path,$t);
	if($f===false) return debug("Unable to open file at '{$path}'");
	if(flock($f,LOCK_EX)){
		fwrite($f,$data);
		fflush($f);
		flock($f,LOCK_UN);
	} else return debug("Unable to perform exclusive write on '{$path}'");
	fclose($f);
	return true;
}
function escape($string){ return nl2br(addslashes(htmlspecialchars(trim($string)))); }
function fail($code = null){ # go to HTTP error page w/ message
	$state = decode(Data::ERRORS);
	if(!isset($state[$code])) $code = 500;
	$status = $state[$code]['status'];
    $message = $state[$code]['message'];
    if(!headers_sent()){
        header($_SERVER["SERVER_PROTOCOL"]." {$code} {$status}");
	    header("Status: {$code} {$status}");
    }
	if(X::$ajax){
		AJAX::note_fail("{$code}! $message");
		exit;
	}
	include ROOT.'layout/error.tpl';
	exit;
}
function wait($percent, $status, $message){
	include ROOT.'layout/wait.tpl';
	exit;
}
function jump($code=303,$addr=null){ # redirect to some page
	if($_SERVER['REQUEST_URI'] == $addr){
		fail(508);
	}
	if($code == 303){
		if(empty($addr)){
			$addr = $_SERVER['HTTP_REFERER'];
		}
		if(X::$ajax){
			AJAX::action_redirect($addr);
			exit;
		}
		header($_SERVER["SERVER_PROTOCOL"]." 303 See Other");
		header("Location: {$addr}", false, 303);
		exit;
	} else {
		fail($code);
	}
	exit;
}
function deny($text, $code = 303, $addr = null){ # remember error and jump to another page immediatly
    new Note($text, Note::FAILURE);
    debug($text);
	jump($code, $addr);
	exit;
}
function stop($text, $error = "Something went wrong"){
	debug($text);
	deny(X::VERBOSE ? $text : $error);
	exit;
}
function debug($text){ # log message to file /system.log
	if(!X::VERBOSE) return false;
	$time = date('l jS \of F Y | h:i:s');
    $f = fopen(ROOT.'system.log','a+b');
    if(!$f){
        new Note("Unable to lock syslog", Note::FAILURE);
    }
    if(flock($f, LOCK_EX)){ fwrite($f, "[{$time}]\t".$text.PHP_EOL); fflush($f); flock($f, LOCK_UN); }
    else {
        if(class_exists('Note')){
            new Note("Unable to lock syslog", Note::FAILURE);
        } else {
            exit("Unable to lock syslog");
        }
    }
	fclose($f);
	return false;
}
function fatal(){
	if($E = error_get_last()){
		debug("Error {$E['type']} in {$E['file']} # {$E['line']}: {$E['message']}");
		fail(500);
	}
	exit;
}#register_shutdown_function('fatal');
function error($code,$text,$file,$line){
	if($code&error_reporting()){
		fatal();
		exit;
	}
}#set_error_handler('error');
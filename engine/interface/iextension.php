<?php

namespace XT;

interface IExtension {

    /**
     * Send the data to prepare it for processing if needed
     */
    public function xt_prepare($data);

    /**
     * Process the data and return the result
     */
    public function xt_process();

}
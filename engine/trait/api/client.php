<?php

trait API_CLIENT {

    /**
     * Date of creation of the node
     * @todo implement DOC in the database
     */
    private static function client_info_node_doc($node_id){
        return [ "doc" => time() ];
    }

    /**
     * Status of the node
     * @todo implement visibility in the database
     */
    private static function client_info_node_status($node_id){
        $status = [
            "v" => true, // default: visible
            "l" => true, // default: public
            "a" => true // default: posting enabled
        ];
        $literal = static::node__literal($node_id);
        $_status = static::$nodeinfo[$literal]["status"];
        $status["v"] = $_status["v"];
        $status["l"] = $_status["l"];
        $status["a"] = $_status["a"];
        return $status;
    }

    private static function client_info_node_permissions($node_id){
        $literal = static::node__literal($node_id);
        return Katana::binarray([
            "post" => 0,
            "name" => 0,
            "file" => 0,
            "___0" => 0,
            "___1" => 0,
            "___2" => 0,
            "custom" => 0
        ], intval(static::$nodeinfo[$literal]['mode']) >> 8);
        return null;
    }

    private static function client_info_node_statistics($node_id){
        if($literal = static::node__literal($node_id)){

        }
        return null;
    }

    private static function client_info_node_size($node_id){
        if($literal = static::node__literal($node_id)){
            $path = ROOT."static/node/{$literal}";
            return [
                "size" => `du -sh {$path}`
            ];
        }
        return null;
    }

    private static function client_info_wire_doc($node_id, $wire_id){
        return static::client_info_post_doc($node_id, $wire_id);
    }

    private static function client_info_wire_nof($node_id, $wire_id){
        if($literal = static::node__literal($node_id){
            import(System::SQL);
            $_q1 = SQL::kq("SELECT COUNT(`id`) FROM `node_{$literal}` WHERE `rl` = {$wire_id}", SQL::ONE | SQL::NUM);
            $posts = intval($_q1[0]) + 1;
            $_q2 = SQL::kq("SELECT COUNT(*) FROM `files` WHERE `node` = $node_id AND `wire` = $wire_id");
            $files = intval($_q[0]);
            if(!empty($wire)){
                return [ "nof" => [ "p" => $posts, "f" => $files ] ]
            }
        }
        return null;
    }

    private static function client_info_wire_spells($node_id, $wire_id){
        if($literal = static::node__literal($node_id){
            Katana::cata($literal);
            if(!empty(X::$cata[$wire_id])){
                return [ "spells" => X::$cata[$wire_id][0] ];
            }
        }
        return null;
    }

    /**
     * Calculate the size of a wire
     * with all contents
     * @todo implement catalist size
     */
    private static function client_info_wire_size($node_id, $wire_id){
        if($literal = static::node__literal($node_id){
            Katana::cata($literal);
            if(!empty(X::$cata[$wire_id])){
                $size = 0;
                $path = ROOT."static/node/{$literal}";
                $size += filesize("{$path}/thread/{$wire_id}.htm");
                $size += filesize("{$path}/chunk/{$wire_id}.htm");
                $size += filesize("{$path}/head/{$wire_id}.htm");
                import(System::SQL);
                foreach(SQL::sq("SELECT `name`,`size` FROM `files` WHERE `wire` = {$wire_id}", SQL::ONE | SQL::NUM) as $file){
                    $size += $file[1];
                    if(file_exists($f = "{$path}/pre/{$file[0]}")){
                        $size += filesize($f);
                    }
                }
                return [ "size" => $size ];
            }
        }
        return null;
    }

    private static function client_info_post_doc($node_id, $post_id){
        if($literal = static::node__literal($node_id){
            import(System::SQL);
            $post = SQL::kq("SELECT unix_timestamp(`dt`) FROM `node_{$literal}` WHERE `id` = {$post_id}", SQL::ONE | SQL::NUM);
            if(!empty($post)){
                return [ "doc" => $post[0] ]
            }
        }
        return null;
    }

    private static function client_read_node_last($node_id, $number){
        if($number <= 0){
            return null;
        }
        if ($number > 10) {
            return [ "error" => "Too large zone of request" ];
        }
        if($literal = static::node__literal($node_id){
            import(System::SQL);
            $collectable = "`id`,unix_timestamp(`dt`) as `unix`,`rl`,`subj`,`text`,`spec`,`name`";
            switch(static::$request["expect"]){
                case "json": return [ "json" => SQL::kq("SELECT {$collectable} FROM `node_{$literal}` WHERE `rl` = 0 ORDER BY `dt` DESC LIMIT {$number}", SQL::ONE | SQL::KEY); ];
                case "html":
                import(Module::GENERATE);
                    $html = "";
                    foreach(SQL::sq("SELECT {$collectable} FROM `node_{$literal}` WHERE `rl` = 0 ORDER BY `dt` DESC LIMIT {$limit}", SQL::ARR | SQL::KEY) as $wire){
                        $files = Generate::files($lid, $wire['id'], $wire['id']);
                        $html .= Generate::prepare(false, $literal, $wire['id'], $wire['id'], $wire, $files, false);
                    }
                    return [ "html" => $html ];
                    break;
                default: /* drop error */ break;
            }
        }
        return null;
    }

    static function client_route(){
        $callee = static::$request["callee"];
        $node_id = abs(intval($_POST["node_id"]));
        $wire_id = abs(intval($_POST["wire_id"]));
        $post_id = abs(intval($_POST["post_id"]));
        $number = abs(intval($_POST["number"]));
        $literal = escape($_POST["literal"]);
        $keyword = escape($_POST["keyword"]);
        $keysubj = escape($_POST["keysubj"]);
        if(empty($callee["intent"])){
            /* drop error */ return false;
        } else switch($callee["intent"]){
            case "info": switch($callee["target"]){
                case "node": switch($callee["demand"]){
                    case "doc": return static::client_info_node_doc($node_id); break;
                    case "status": return static::client_info_node_status($node_id); break;
                    case "permissions": return static::client_info_node_permissions($node_id); break;
                    case "statistics": return static::client_info_node_statistics($node_id); break;
                    case "size": return static::client_info_node_size($node_id); break;
                    default: break;
                } break;
                case "wire": switch($callee["demand"]){
                    case "doc": return static::client_info_wire_doc($node_id, $wire_id); break;
                    case "nof": return static::client_info_wire_nof($node_id, $wire_id); break;
                    case "spells": return static::client_info_wire_spells($node_id, $wire_id); break;
                    case "size": return static::client_info_wire_size($node_id, $wire_id); break;
                    default: break;
                } break;
                case "post": switch($callee["demand"]){
                    case "doc": return static::client_info_post_doc($node_id, $post_id); break;
                    case "meta": return static::client_info_post_meta($node_id, $post_id); break;
                    case "size": return static::client_info_post_size($node_id, $post_id); break;
                    default: break;
                } break;
                default: break;
            } break;
            case "read": switch($callee["target"]){
                case "node": switch($callee["demand"]){
                    case "last": return static::client_read_node_last($node_id, $number); break;
                    default: break;
                } break;
                case "wire": switch($callee["demand"]){
                    case "last": return static::client_read_wire_last($node_id, $wire_id, $number); break;
                    default: break;
                } break;
                case "post": switch($callee["demand"]){
                    case "contents": return static::client_read_post_contents($node_id, $post_id); break;
                    default: break;
                } break;
                default: break;
            } break;
            case "search": switch($callee["target"]){
                case "node": return static::client_search_node(); break;
                case "wire":
                case "post": switch($callee["demand"]){
                    case "text": return static::client_search_post_text($keyword); break;
                    case "file": return static::client_search_post_file($_FILES["keyfile"]); break;
                    case "subj": return static::client_search_post_subj($keysubj); break;
                    default: break;
                } break;
                default: break;
            } break;
            default: break;
        }
        /* drop error */ return false;
    }
}
<?php

trait API_SERVER {

    static function server_route(){
        $callee = static::$request["callee"];
        if(empty($callee["intent"])){
            /* drop error */ return false;
        } else switch($callee["intent"]){
            case "info": switch($callee["target"]){
                case "engine": switch($callee["demand"]){
                    case "doc": return static::server_info_engine_doc(); break;
                    case "revision": return static::server_info_engine_revision(); break;
                    default: break;
                } break;
                default: break;
            } break;
            case "action": switch($callee["target"]){
                case "graph": switch($callee["demand"]){
                    case "call": return static::server_action_graph_call(); break;
                    case "echo": return static::server_action_graph_echo(); break;
                    case "conn": return static::server_action_graph_conn(); break;
                    case "disc": return static::server_action_graph_disc(); break;
                    default: break;
                } break;
                default: break;
            } break;
            default: break;
        }
        /* drop error */ return false;
    }
    
}
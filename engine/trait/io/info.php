<?php

trait Info {

    public static function info__hello(){
        return `neofetch`;
    }

    public static function info__instance_size(){
        $kt_root = ROOT;
        $kt_size = `du -B1 -s "{$kt_root}" | awk '{ print $1 }'`;
        return Chrona::bytes($kt_size);
    }

    public static function info__disk_usage($d){
        return Chrona::bytes(`df -B1 "{$d}" | grep -vE '^Filesystem|tmpfs|cdrom|udev' | awk '{ print $3 }'`);
    }
    
    public static function info__disk_total($d){
        return Chrona::bytes(`df -B1 "{$d}" | grep -vE '^Filesystem|tmpfs|cdrom|udev' | awk '{ print $2 }'`);
    }

    public static function info__updates(){
        # todo: CURL request to the NCI/NPC interface resulting with
        # the last version available on public server repository
        $version_available = X::VERSION;
        $update_available = $version_available > X::VERSION;
        if(file_exists($f = ROOT."dynamic/io/info/update.php"))
			include_once $f;
    }

    public static function info__hostname(){
        return trim(`hostname`);
    }

    public static function info__date(){
        return trim(`date`);
    }

    public static function info__uptime(){
        return `uptime | cut -f1 -d"," | sed 's/^ *//' | cut -f3- -d" "`;
    }

    public static function info__users(){
        return `uptime | cut -f2 -d"," | sed 's/^ *//'| cut -f1 -d" "`;
    }

    public static function info__load_average(){
        return `awk '{print "Load average:" $1 " " $2 " " $3}' < /proc/loadavg`;
    }

    public static function info__memory_usage_total(){
        return Chrona::bytes(`cat /proc/meminfo | awk '/MemTotal/ {print $2*1024}'`);
    }

    public static function info__memory_usage_used(){
        return Chrona::bytes(`cat /proc/meminfo | awk '/MemFree/ {print $2*1024}'`);
    }

    public static function info__heavy_process(){
        $proc = [];
        for($i = 2; $i < 7; $i++){
            $proc[]= `ps ax -o pid,rss,pcpu,ucmd --sort=-cpu,-rss | sed -n "$i,$i p" | awk '{printf "%s: %smo:  %s%%" , $4, $2/1024, $3 }'`;
        }
        return implode('<br/>', $proc);
    }

    public static function info__kernel_msg(){
        exec('dmesg | tail -n 10 > /tmp/ns.katana.sys.dmesg');
        $kern = [];
        $f = fopen("/tmp/ns.katana.sys.dmesg","r");
        while(!feof($f)){
            $kern[]= fgets($f);
        }
        fclose($f);
        exec('rm -f /tmp/ns.katana.sys.dmesg');
        return implode('<br/>', $kern);
    }

    public static function info__inet_ifs(){
        $_ifaces = `$(for inet in $(ifconfig) | cut -f1 -d " " | sed -n "/./ p"; do ifconfig $inet | awk 'BEGIN{printf "%s", "'"$inet"'"} /adr:/ {printf ":%s\n", $2}'|sed 's/adr://'; done)`;
        
    
    }

}
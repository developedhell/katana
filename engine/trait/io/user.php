<?php

trait User {

	private static $io_rights = [
		0 => "Damned",
		1 => "Soul",
		2 => "Spirit",
		3 => "God"
	];

	public static function guid(){ if(static::granted()) return key($_SESSION['IO']); return "anon"; }
	public static function sign(){
		if(isset($_SESSION['IO'])){
			if(is_array($_SESSION['IO'])){
				$anon = key($_SESSION['IO']);
				if(!empty($anon)){
					return hash('ripemd160', session_id().$anon);
				}
			}
		}
		return hash('ripemd160', session_id());
	}
	public static function granted($admin = false){
		if(empty($_SESSION['IO']) || $_SESSION['IO'] === false) return false;
		if(is_array($_SESSION['IO'])){
			$anon = key($_SESSION['IO']);
			if($_SESSION['IO'][$anon]["sign"] === X::$sign)
				return $anon 
					? $admin
						? $_SESSION['IO'][$anon]["rank"] == 255
						: $_SESSION['IO'][$anon]["rank"] >= 100
					: false;
		}
		return false;
	}
	private static function in(){
		import(System::CRYPT);

		if(!NCC::token(session_id(), escape($_POST["token"])))
			deny("Token mismatch");
		if(empty($_POST["user"]) || empty($_POST["pass"]))
			deny("No local data received");

		$response = static::api([
			'io' => 'in',
			'sign' => X::$sign,
			'ssid' => session_id(),
			'token' => $_POST['token'],
			'user' => $_POST['user'],
			'pass' => $_POST['pass']
		]);

		if(!is_array($response))
			deny("Invalid data received");
		if(!isset($response['status'], $response['message'], $response['data']))
			deny("Malformed data structure");
		switch($response['status']){
			case Note::SUCCESS:
				$anon = key($response['data']);
				$_SESSION['IO'] = $response['data'];
				$_SESSION['IO'][$anon]['sign'] = static::sign();
				new Note("Welcome, #{$anon}!", Note::SUCCESS);
				jump(303, '/io/main');
				break;
			case Note::WARNING:
			case Note::FAILURE:
			default:
				new Note(y($response['message'],"Error occured and no details were provided"), $response['status']);
				jump(303);
				break;
		}
	}
	private static function out(){
		$_SESSION['IO'] = false;
		static::api(['io' => 'out']);
		jump(303, "/io/in");
	}
	public static function user__name_code(){
		return is_array($_SESSION['IO'])
			? key($_SESSION['IO'])
			: '';
	}
	public static function user__right_level(){
		$anon = static::user__name_code();
		if(!empty($anon)){
			return static::$io_rights[$_SESSION['IO'][$anon]['rights']];
		}
	}
	public static function user__rank(){
		$anon = static::user__name_code();
		if(!empty($anon)){
			return $_SESSION['IO'][$anon]['rank'];
		}
	}
}
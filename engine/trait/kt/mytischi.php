<?php # KT_MYTISCHI # Routing and navigation functions

trait MYTISCHI {

	static function page__static(){
		X::$type = Page::IDLE;
		X::$temp = decode(Data::PAGELIST);
		X::$node = X::$temp[$argn];
		X::$view = new View($argn, "page");
		X::$view->show();
	}

	static function page__dynamic(){
		X::$type = Page::FUNC;
		X::$temp = decode(Data::PAGELIST);
		X::$node = X::$temp[$argn];
		$func = "page__dynamic_{$argn}";
		self::$func();
	}

	static function page__dynamic_main(){
		X::$type = Page::FUNC;
		X::$view = new View("kt/main", "clear");
		X::$view->show();
	}

	static function page__dynamic_options(){
		X::$type = Page::FUNC;
		X::$temp = decode(Data::PAGELIST);
		X::$node = X::$temp[$argn];
		X::$opts->route();
		X::$view->show();
	}

	static function page__dynamic_search(){
		X::$type = Page::FUNC;
		fail(501);
		return false;
	}

	static function page__node(){
		X::$type = Page::NODE;
		if(self::tree() && isset(X::$tree[X::$path[0]])){ # welcome on board
			X::$node = X::$path[0];
			X::$mode = X::$tree[X::$node]['mode'];
			self::cata(X::$node);

			import(System::Q);
			Q::init(X::$node);

			#import(Module::kt_generate);
			#foreach(X::$cata as $t => $v){ Generate::chunk(X::$node, $t); }
			#Generate::catalist_entry(X::$node, 17);

			#set_time_limit(9000);
			#import(System::SQL, Module::kt_generate);
			
			#Generate::thread(X::$node, 680);
			$page = self::i(1, 'page');
			switch($page){
				case 'page':	self::page__node_index(abs(intval(self::i(2,0))));	break;
				case 'thread':	self::page__node_wire(abs(intval(self::i(2,0))));	break;
				case 'post':	self::page__node_post(abs(intval(self::i(2,0))));	break;
				case 'catalog':	self::page__node_catalog();	break; # board catalog
				case 'media':	self::page__node_media(); 	break; # media listing
				case 'create':	self::post__create();	break; # new post
				case 'read':	self::post__read();		break; # get post
				case 'delete':	self::post__delete();	break; # delete
				case 'update':	self::post__update();	break; # edit
				case 'check':	self::check();	break; # check for any changes
				default: fail(404); break;
			}
			X::$view->show();
		} else fail(404);
	}

	static function page__node_index($n = 0){
		X::$type = Page::NODE;
        X::$page = $n;
		X::$view = new View('kt/index', 'kt/index');
		X::$view->show();
	}

	static function page__node_wire($id){
		X::$type = Page::WIRE;
		if(empty($id)){
            jump(303, '/'.X::$node);
        }
		X::$wire = $id;
		X::$view = new View('node/'.X::$node.'/thread/'.X::$wire, 'kt/thread');
		X::$view->show();
	}

	static function page__node_post($id){
		X::$type = Page::NODE;
		if(empty($id)){
			return jump(303, '/'.X::$node);
		}
		import(Module::kt_request);
		$thread = Request::parent(X::$node, $id);
		if($thread){
			jump(303, '/'.X::$node."/thread/{$thread}#P{$id}");
		} else {
			new Note(E::post__thread($id));
			self::page__node_index();
		}
	}

	static function page__node_catalog(){
		X::$type = Page::NODE;
		X::$view = new View('kt/catalog', 'kt/catalog');
		X::$view->show();
	}

	static function page__node_media(){
		X::$type = Page::NODE;
		import(System::SQL);
		$location = "kt/media";
		$argn = y(X::$path[2],"all");
		switch($argn){
			case 'all': X::$view = new View("{$location}/all", "board"); break;
			case 'album':
			case 'video':
			case 'music': X::$view = new View("{$location}/{$argn}", "board"); break;
			default: fail(404); break;
		}
		X::$view->show();
	}

	static function page__node_all(){
		X::$type = Page::NODE;
		import(System::SQL);
		X::$view = new View('kt/all', 'kt/all');
		X::$view->show();
	}

	static function page__node_fav(){
		X::$type = Page::NODE;
		X::$view = new View('kt/fav', 'kt/fav');
		X::$view->show();
	}
}
<?php # KT_UNITY # Layout functions

use Katana\Seal as Seal;
use Katana\Spell as Spell;

trait UNITY {
		
	static function pagination($total, $it_address) { if(file_exists($f = ROOT."dynamic/kt/pagination.php")) include_once $f; }

	static function pagination_index(){ static::pagination(ceil(count(X::$cata)/X::$conf['kt']['maxthrds']), X::$node."/page"); }
	
	static function address(){
		$path = "<a href='/'>root</a>";
		$f = '';
		foreach(X::$path as $p){
			$f.= '/'.$p; $path.= "/<a href='{$f}'>{$p}</a>";
		}
		return $path;
	}	
	
	static function osaka(){ printf("<!-- Osaka used %db and %.5fs -->", memory_get_usage(), microtime(true)-TIME); return false; }
}
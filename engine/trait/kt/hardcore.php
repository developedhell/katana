<?php # KT_HARDCORE # CRUD functions

trait HARDCORE {

	/**
	 * Create post
	 */
	static function post__create(){
		if(!isset($_POST["token"])){
            deny("Invalid request");
        }
        self::node(X::$node);
		$data = array(
            'lt'   => $_POST['lt'],
			'rl'   => intval($_POST['rl']),
			'sage' => isset($_POST['sage']),
			'noko' => intval($_POST['noko']),
			'subj' => escape($_POST['subj']),
			'text' => escape($_POST['text']),
			'pass' => escape($_POST['pass']),
			'file' => $_FILES["files"],
			'spec' => null
		);
		if(X::$conf['io']['firewall']['spamfilter']){
            // do something with spam
		}
		import(Module::kt_handle, Module::kt_request, Module::kt_generate);
		if($pid = Request::create($data)){
			header_remove("Expires");
			header_remove("Cache-Control");
			header_remove("Pragma");
			header_remove("Last-Modified");
			jump(303, "/".X::$node.($data['noko'] ? "/post/" : "#P").$pid);
		} else jump(303, $_SERVER['HTTP_REFERER']);
	}

	/**
	 * Get post
	 */
	static function post__read($num = 0){ import(System::SQL, Module::kt_request); return Request::read(X::$node,$num); }

	/**
	 * Edit post
	 */
	static function post__update(){ return fail(501); }

	/**
	 * Delete post
	 */
	static function post__delete(){ return fail(501); }
	
	/**
	 * Check for updates in conditional field
	 */
	static function check(){ fail(501); return false; }
}
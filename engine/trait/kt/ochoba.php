<?php # KT_OCHOBA # Core basic functions

trait OCHOBA {

	/**
	 * Load information about the node tree
	 */
	static function tree(){
		if(empty(X::$tree)){
			Graph::$select = decode(Graph::SELECT);
			X::$tree = array_merge(Graph::$select["public"], Graph::$select["private"]);
		}
		return true;
	}

	/**
	 * Load the catalist for the current node
	 */
	static function cata($literal, $override = false){
		if(empty(X::$cata) || $override){
			if(isset(X::$tree[$literal])){
				X::$cata = decode("static/node/{$literal}/catalist");
			} else {
				X::$cata = false;
			}
		}
		return true;
	}

	/**
	 * Force internal node change
	 */
	static function node($literal){
		self::tree();
		if(!isset(X::$tree[$literal])){
			new Note("Unable to change node: /{$literal}/ does not exist", Note::FAILURE);
			return false;
		}
		X::$node = $literal;
		self::cata($literal, true);
		X::$mode = X::$tree[X::$node]['mode'];
	}

	/**
	 * Address sections cursor function
	 */
	static function i($n, $m){ return isset(X::$path[$n]) ? empty(X::$path[$n]) ? $m : X::$path[$n] : $m; }

	/**
	 * Check node file system for incostincency 
	 * and try to repair if found any issues
	 */
	static function chkfs($literal){
		$board = ROOT."static/node/{$literal}/";
		if(!file_exists($board)){
			mkdir($board, 0770);
		}
		$paths = [
			$board,
			$board.'pre',
			$board.'src',
			$board.'map',
			$board.'head',
			$board.'chunk',
			$board.'thread'
		];
		foreach($paths as $path){
			if(!file_exists($path)){
				mkdir($path, 0770);
			}
		}
		import(Module::kt_generate);
		self::node($literal);
		if(!file_exists($f = $board.'catalist.json')){
			touch($f, 0770);
			Generate::catalist(X::$node);
		}
		if(!file_exists($f = $board."info.htm")){
			touch($f, 0770);
			Generate::nodeinfo(X::$node);
		}

		if(!empty(X::$node)){
			self::node(X::$node);
		}
        
        debug("[build::chkfs] FS OK");
		return false;
	}
}
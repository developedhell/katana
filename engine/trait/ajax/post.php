<?php

trait Post {

    private static function post_read($pid){
		$literal = X::$node;
		$lid = intval(X::$tree[$literal]['id']);
		if($pid <= 0){
			static::note_fail("Error fetching post");
			return "";
		}

		import(System::SQL);
		$collectable = "`id`,unix_timestamp(`dt`) as `unix`,`rl`,`subj`,`text`,`spec`,`name`";
		$post = SQL::kq("SELECT {$collectable} FROM `node_{$literal}` WHERE `id` = {$pid}", SQL::ONE | SQL::KEY);
		$tid = intval($post['rl']) == 0 ? $post['id'] : $post['rl'];
		$tid = intval($tid);
		$files = SQL::kq("SELECT `name`, `orig`, `info`, `size` FROM `files` WHERE `node` = {$lid} AND `wire` = {$tid} AND `post` = {$pid}", SQL::ARR | SQL::KEY);

		if(empty($post)){
			static::note_fail("The post #{$pid} does not exist");
			return "";
		}

		import(Module::GENERATE);
		$post = Generate::prepare(false, X::$node, $pid, $tid, $post, $files, false);
		return $post;
	}

	private static function post_create(){
		if(!isset($_POST["create"])){
			static::note_fail("Invalid query");
			return false;
		}
		self::tree(); self::cata(X::$node);
		preg_match('/(\!.+\r?(\n|$))?((.*\n*)*)/',$_POST['text'],$M);
		$subj = empty($M[1]) ? '' : mb_substr($M[1],1);
		$text = $M[3];
		$data = array(
			'rl'   => intval($_POST['rl']),
			'sage' => isset($_POST['sage']),
			'noko' => isset($_POST['noko']),
			'subj' => self::escape($subj),
			'link' => self::escape($_POST['link']),
			'text' => self::escape($text),
			'pass' => self::escape($_POST['pass']),
			'file' => $_FILES["file"],
			'spec' => null
		);
		import(Module::HANDLE, Module::REQUEST, Module::GENERATE);
		if($pid = Request::create($data)){
			if($data['noko']){
				static::action_redirect("/".X::$node."/post/".$pid);
				return false;
			} else {
				return static::post_read($pid);
			}
		} else {
			static::note_fail("Unable to create post");
			static::action_log_array(Note::silent());
		}
		return false;
	}

}
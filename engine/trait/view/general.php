<?php

trait General {
	static function loop(){
		if(!isset($_SESSION['FT']) || mt_rand(0,99) > 90){
			$_SESSION['FT'] = true;
			return '/assets/flash/ngslc/'.[
				'havanagila',
				'beznogim',
				'duck',
				'mind',
				'space'
			][mt_rand(0,4)];
		}
		$LO = array(
			'heart',
			'coloris',
			'earth',
			'jettison',
			'wandering',
			'love',
			'heart2',
			'dream',
			'life'
		);
		if(empty($_SESSION['LO'])){
			$_SESSION['LO'] = $LO;
		} else $LO = $_SESSION['LO'];
		$I = mt_rand(0, count($LO) - 1);
		$L = $LO[$I];
		array_splice($LO, $I, 1);
		$_SESSION['LO'] = $LO;
		return '/assets/flash/'.$L;
	}

	static function banner(){
		$x = array_diff(scandir(ROOT.'assets/i/boards',SCANDIR_SORT_NONE),array('.','..'));
		$b = "<a class='advice' href='/'><img src='/assets/i/none.gif' alt='?' width='333' height='111' /></a>";
		if(!empty($x)){
			shuffle($x);
			$x = $x[0];
			preg_match('/^((\D+)\d*)\.\w+/i',$x,$y);
			$b = "<a class='advice' href='/{$y[2]}/'><img src='/assets/i/boards/{$x}' alt='{$y[1]}' width='333' height='111' /></a>";
		}
		echo $b;
	}

	static function motto(){
		if($m = file_get_contents(Data::MOTTOS)){
			return $m[mt_rand(0, count($m) - 1)];
		}
		return null;
	}

	static function bckg(){
		return '<aside bckg locked id="background" style="background-image:url("/assets/i/wp/'.random_int(1,16).'.jpg")"></aside>'
			.'<script async defer type="text/javascript" src="/assets/js/background.js"></script>';
	}
}
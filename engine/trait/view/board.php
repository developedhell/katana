<?php

use Katana\Seal as Seal;
use Katana\Spell as Spell;

trait Board {
	public function footer(){
		$this->insert("kt/footer", true);
	}

	public function disclaimer(){ $this->insert("kt/disclaimer", true); }

	public function helper(){ $this->insert("kt/helper", true); }

	public function postform(){
		$works = X::$mode&Seal::POST;
		$locked = true;
		if(X::$wire > 0){
			$locked = X::$cata[X::$wire][0]&Spell::LOCKED;
		} else $locked = false;
		if($works && !$locked){
			import('dynamic/kt/form');
		}
	}
}
<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL ^ E_NOTICE);
setlocale(LC_TIME,"ru_RU.utf8");
date_default_timezone_set('Europe/Moscow');
ob_implicit_flush(); while(ob_get_level()) ob_end_flush();

define('VERBOSE', false);
define('NAME', 'Katana X Nightly Build');
define('HOST', 'test.nullnyan.net');
define('FQDN', 'https://'.HOST);
define('SEED', '23wrsfsfsfaq');
define('ROOT', __DIR__.'/');
define('TIME', microtime(true));
define('XARG', 174131599);
define('UPDATE', TIME);
define('NT', 'NT'.TIME);
define('IO', 'IO'.TIME);

# Hakase technology
require_once 'engine/environment/ying.php';
require_once 'engine/environment/yang.php';

import(
	'engine/environment/system',
	'engine/environment/module'
);

try { import('engine/x','engine/katana'); Katana::init(); } catch (Note $N) { fail(500); }
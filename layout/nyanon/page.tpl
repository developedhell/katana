<!DOCTYPE html><html><head><title><?=NAME?></title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
	<base href="<?=HOST?>/" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<?=X::$ccss?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
</head><body>
	<header>
		<address><?=Katana::address()?></address>
		<h5><?=X::$node?></h5>
		<nav><?php Katana::boardlist(); ?></nav>
		<label for="options"><i class="icon-settings"></i></label>
	</header>
	<main>
		<div><?php Katana::locate($content); ?></div>
		<hr/>
		<aside info><input switch type="checkbox" name="options" id="options" /><div switch-zone>
			<label for="options"><i class="icon-close"></i></label>
			<input switch type="radio" name="t_options" id="options1" checked />
			<label tabindex for="options1">User settings</label>
			<hr />
			<div switch-zone tab="options1"><?php include_once ROOT.'dynamic/options.php'; ?></div>
		</div></aside>
		<?php Katana::disclaimer(); ?>
		<aside notes><?=Note::redeem()?></aside>
	</main>
</body></html>
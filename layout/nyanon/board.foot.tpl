		<hr class="bottom" />
		<aside form><?php Katana::postform(); ?></aside>
		<aside info><input switch type="checkbox" name="options" id="options" /><div switch-zone>
			<label for="options"><i class="icon-close"></i></label>
			<input switch type="radio" name="t_options" id="options1" checked />
			<label tabindex for="options1">User settings</label>
			<input switch type="radio" name="t_options" id="options2" />
			<label tabindex for="options2">About /<?=X::$node?>/</label>
			<hr />
			<div switch-zone tab="options1"><?php include_once ROOT.'dynamic/options.php'; ?></div>
			<div switch-zone tab="options2"><?php include_once ROOT.'content/'.X::$node.'/info.htm'; ?></div>
		</div></aside>
		<?php Katana::disclaimer(); ?>
		<aside control hidden>
			<form name="control" method="post" action="/control/submit">
				<input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
				<input id="actions" name="actions" type="checkbox" switch /><div switch-zone>
					<span>
						<input type="text" name="pass" value="<?=X::$opts['PASS']?>" />
						<button name="send" value="delete" type="submit"></button>
					</span>
					<span hidden>
						<button name="send" value="love" type="submit">Love</button>
						<button name="send" value="hate" type="submit">Hate</button>
					</span>
					<?php if(IO::granted()): ?>
					<span><button name="send" value="priority_pin" type="submit">Pin</button></span>
					<span><button name="send" value="priority_close" type="submit">Close</button></span>
					<span><button name="send" value="priority_curse" type="submit">Curse</button></span>
					<span><button name="send" value="priority_delete" type="submit">Delete</button></span>
					<?php endif; ?>
				</div>
			</form>
		</aside>
		<aside notes><?=Note::redeem()?></aside>
	</main>
	<footer>
		<?php if(X::$thrd){ ?>
			<a href="/<?=X::$node?>/"><i class="icon-left"></i>Return</a>
			<label for="form"><?php if(!X::$mobi): ?>Answer to <?php endif; ?>&gt;&gt;<?=X::$thrd?></label>
			<a refresh href="<?=$_SERVER['REQUEST_URI']?>"><i class="icon-refresh"></i>Refresh</a>
		<?php } else { ?>
			<nav><?=Katana::pagination()?></nav>
			<label for="form"><?php if(X::$mobi): ?><i class="icon-plus"></i><?php else: ?>Create a thread<?php endif; ?></label>
			<a href="/<?=X::$node?>/catalog"><i class="icon-catalist"></i><?php if(!X::$mobi): ?> Catalog<?php endif; ?></a>
		<?php } ?>
	</footer>
<a name="bottom"></a></body></html>
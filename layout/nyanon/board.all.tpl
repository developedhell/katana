<?php
	use X\Seal as Seal;
	use X\Spell as Spell;
	use X\Directive as Directive;
?>

<!DOCTYPE html><html><head><title><?=NAME?> ~ /all/</title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
	<base href="<?=HOST?>" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<?=X::$ccss?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
	<script defer type='text/javascript' src='/assets/js/zeroscript.js?v=<?=UPDATE?>'></script>
</head>
<body><a name="top"></a>
	<header>
		<address><a href='/'>root</a>/<a href="/all">all</a></address>
		<h5>/all/&nbsp;&mdash;&nbsp;All boards</h5>
		<nav><?php include_once ROOT.'content/boardlist.htm'; ?></nav>
		<label for="options"><i class="icon-settings"></i></label>
	</header>
	<main><div flow><?php

Katana::tree();

$offset = X::$page * 25;
$limit = $offset + 25;
$hidden = Spell::HIDDEN;

$boards = SQL::kq("SELECT `lt` from `nodes` where `mode` & {$hidden} = 0 AND `lt` != 'e'", SQL::ARR|SQL::NUM);
$boardposts = array();
$literal = '';
foreach ($boards as $_literal){
	$literal = $_literal[0];
	$boardposts[] = "( select `id`, `bump`, '{$literal}' as `nd` from `node_{$literal}` where `rl` = 0 order by `bump` desc )";
}
$boardunion = implode(" union ", $boardposts);
$selected = SQL::kq("SELECT feed.* from ( {$boardunion} ) as `feed` order by `bump` desc limit {$offset}, {$limit}", SQL::ARR|SQL::NUM);
X::$node = "";
foreach ($selected as $thread){
	if (file_exists($chunk = ROOT.'content/'.$thread[2].'/chunk/'.$thread[0].'.htm')){
		if(X::$node != $thread[2]){
			Katana::cata($thread[2], true);
		}
		X::$node = $thread[2];
		X::$temp = explode('.',X::$cata[intval($thread[0])][1]);
		echo "<a href='/{$thread[2]}'>/{$thread[2]}/</a>"
			."<samp>".X::$temp[0]." posts (".X::$temp[1]." files, ".X::$temp[2]." links) &mdash; "
			.X::$tree[$thread[2]]["desc"]
			."<a href='/{$thread[2]}/thread/{$thread[0]}'>Open <i class='icon-right'></i></a>"
			."</samp>"
			."<section id='{$thread[2]}{$id}' class='flow' data-id='{$thread[0]}' data-node='{$thread[2]}'>";
		include $chunk;
		echo "</section>";
	}
}

		?></div><hr class="bottom" />
		<aside form><?php if((X::$mode&Seal::POST)||(X::$thrd&&(X::$cata[X::$thrd][0]&Spell::LOCKED))) import('dynamic/form'); ?></aside>
		<aside info><input switch type="checkbox" name="options" id="options" /><div switch-zone>
			<label for="options"><i class="icon-close"></i></label>
			<input switch type="radio" name="t_options" id="options1" checked />
			<label tabindex for="options1">User settings</label>
			<hr />
			<div switch-zone tab="options1"><?php include_once ROOT.'dynamic/options.php'; ?></div>
		</div></aside>
		<aside advice><?=Katana::banner()?></aside>
		<aside bottom>
			<address><a href="/news">News</a><a href="/faq">FAQ</a><a href="/options">Options</a></address>
			<p>All trademarks, copyrights, text and media content on this page are owned by and are the responsibility of their respective parties, if they exist at the moment of the posting.</p>
			<p>&mdash; <a href="?" title="(&#x2500;&#x203F;&#x203F;&#x2500;)">Katana X v<?=Directive::VERSION?></a> (<a href="/todo">nightly</a>) &mdash;</p>
			<?=Katana::info()?>
		</aside>
		<aside control hidden>
			<form name="control" method="post" action="/control/submit">
				<input type="hidden" name="token" value="<?=Katana::token(session_id())?>" />
				<input id="actions" name="actions" type="checkbox" switch /><div switch-zone>
					<span>
						<input type="text" name="pass" value="<?=X::$opts['PASS']?>" />
						<button name="send" value="delete" type="submit"></button>
					</span>
					<span hidden>
						<button name="send" value="love" type="submit">Love</button>
						<button name="send" value="hate" type="submit">Hate</button>
					</span>
					<?php if(Katana::granted()): ?>
					<span><button name="send" value="priority_pin" type="submit">Pin</button></span>
					<span><button name="send" value="priority_close" type="submit">Close</button></span>
					<span><button name="send" value="priority_curse" type="submit">Curse</button></span>
					<span><button name="send" value="priority_delete" type="submit">Delete</button></span>
					<?php endif; ?>
				</div>
			</form>
		</aside>
		<aside notes><?=Note::redeem()?></aside>
	</main>
<a name="bottom"></a></body></html>
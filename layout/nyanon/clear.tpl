<!DOCTYPE html><html><head><title><?=NAME?></title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
	<base href="<?=HOST?>/" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<?=X::$ccss?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
</head><body>
	<?php include_once $content; ?>
	<?php Katana::disclaimer(); ?>
	<aside notes><?=Note::redeem()?></aside>
</body></html>
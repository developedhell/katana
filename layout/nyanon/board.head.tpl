<!DOCTYPE html><html><head><title><?=NAME?> ~ /<?=X::$tree[X::$node]['desc']?>/</title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
	<base href="<?=HOST?>" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<?=X::$ccss?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
	<script defer type='text/javascript' src='/assets/js/zeroscript.js?v=<?=UPDATE?>'></script>
</head><body><a name="top"></a>
<?php if(X::$mobi): ?>
	<header><h2><?php
		if(X::$thrd){ echo 'Thread <a href="/'.X::$node.'/thread/'.X::$thrd.'">&gt;&gt;'.X::$thrd.'</a>'; }
		else { echo '/'.X::$node.'/ &mdash; '.X::$tree[X::$node]['desc']; }
	?></h2></header>
<?php else: ?>
	<header>
		<address><a href='/'>root</a><?php
			$f = ''; $g = ''; foreach(X::$path as $p){ $f.= '/'.$p; $g.= "/<a href='{$f}'>{$p}</a>"; } echo $g;
		?></address>
		<h5><?php
			if(X::$thrd){ echo 'Thread <a href="/'.X::$node.'/thread/'.X::$thrd.'">&gt;&gt;'.X::$thrd.'</a>'; }
			else { echo '/'.X::$node.'/ &mdash; '.X::$tree[X::$node]['desc']; } ?></h5>
		<nav><?php include_once ROOT.'content/boardlist.htm'; ?></nav>
		<label for="options"><i class="icon-settings"></i></label>
	</header>
<?php endif; ?>
	<main>
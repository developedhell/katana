<!DOCTYPE html><html><head><title>Oops, divided by <?=$code?></title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<style type="text/css">
		@import '/assets/css/null.css';
		html,body{width:100%;height:100%;margin:0;padding:0}html{background:#111217;color:#bfbfbf}body{padding-top:96px;box-sizing:border-box}
		div{padding:16px 0 32px 15%;background:#0d0e12}em{display:block}output{font-size:26px}samp{font-size:52pt}
		div,em,output,samp{font-family:"Trebuchet MS",Helvetica,sans-serif}
		img{position:absolute;bottom:0;right:0;max-width:50%;max-height:50%}
		input{display:none}
		.message {
			display: block;
			margin: 16px;
			box-sizing: border-box;
			height: auto;
			padding: 16px 32px;
			background: crimson;
			color: #111;
		}
		.message div {
			padding: 0;
			background: transparent;
		}
		.message__wrapper {
			position: fixed;
			top: 0; right: 0; bottom: 0;
			width: 384px;
			overflow: auto;
		}
	</style>
</head><body><div><samp><?=$code?></samp><output><?=$status?></output><em><?=$message?></em></div><img src="/assets/i/qts/r/<?=mt_rand(1,73)?>.png" alt /><aside class="message__wrapper"><?=Note::redeem()?></aside></body></html>
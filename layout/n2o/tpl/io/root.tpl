<!DOCTYPE html><html><head><title><?=NAME?></title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
    <base href="<?=FQDN?>" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<?=X::$ccss?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
</head><body>
	<aside class="mobile-bar menu" hidden>
		<label for="open-nav">
			<i class="icon icon_size_b icon-navicon mobile-bar-icon">≡</i>
		</label>
		<a href="/" class="mobile-bar-logo icon icon_size_b mobile-bar-icon icon-logo">∅</a>
		<label for="open-search">
			<i class="icon icon_size_b icon-search mobile-bar-icon">⚲</i>
		</label>
	</aside>
	<input type="checkbox" id="open-nav" class="open-nav switch" hidden>
	<label for="open-nav" class="overlay switch-zone" hidden></label>
	<nav class="nav menu">
		<header class="nav__header">
			<a href="/" class="nav__logo title">Nullnyan</a>
			<label for="open-nav">
				<i class="nav__close icon icon_size_b icon-close"></i>
			</label>
		</header>
		<main class="nav__main menu__item switch-zone"><?php insert("io/menu", true); ?></main>
		<footer class="nav__footer menu__item switch-zone">
			<a href="/settings" class="nav__link menu__link">Settings</a>
			<a href="/help" class="nav__link menu__link">Help</a>
			<?php if(IO::granted()): ?>
				<a href="/io/out" class="nav__link menu__link">Exit</a>
			<?php else: ?>
				<a href="/io/in" class="nav__link menu__link">Enter</a>
			<?php endif; ?>
		</footer>
		<i class="icon icon_size_b nav__open" hidden>///</i>
	</nav>
	<main class="main">
		<aside class="message__wrapper" hidden><?=Note::redeem()?></aside>
		<?php Katana::locate($content); ?>
	</main>
	<footer>
		<aside class="aside__wrapper"><?=Katana::banner()?></aside>
		<hr />
		<aside class="aside__disclaimer"><?=Katana::disclaimer()?></aside>
	</footer>
</body></html>
<!DOCTYPE html><html><head>
	<meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>">
	<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1">
	<meta name="robots" content="noindex"/> 
	<meta name="theme-color" content="#111217" />
	<title><?=NAME?> &mdash; <?=X::$tree[X::$node]['desc']?></title>
	<link rel="shortcut icon" href="/favicon.ico">
	<?=X::$view->assets?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
	<script defer type='text/javascript' src='/assets/js/zeroscript.js?v=<?=UPDATE?>'></script>
</head><body>
	<?php X::$view->menu(); ?>
	<main class="main">
		<header class="header"><h1 class="title"><?=X::$tree[X::$node]['desc']?></h1>
			<a href="/<?=X::$node?>/catalogue" class="icon icon_size_b icon-catalist drown header__catalog" title="Open catalogue"></a>
		</header>
		<aside class="message__wrapper"><?=Note::redeem()?></aside>
		<aside form><?php X::$view->postform(); ?></aside>
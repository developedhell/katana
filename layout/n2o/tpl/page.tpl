<!DOCTYPE html><html><head>
	<meta charset="utf-8" />
	<meta http-equiv="content-type" content="text/html;charset=UTF-8">
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>">
	<meta name="viewport" content="height=device-height, width=device-width, initial-scale=1">
	<meta name="robots" content="noindex"/> 
    <meta name="theme-color" content="#111217" />
	<title><?=NAME?> &mdash; <?=X::$node?></title>
	<link rel="shortcut icon" href="/favicon.ico">
	<?=X::$view->assets?>
	<script type="text/javascript" src="/assets/js/main.js?v=<?=UPDATE?>"></script>
	<script defer type='text/javascript' src='/assets/js/zeroscript.js?v=<?=UPDATE?>'></script>
</head><body>
	<input type="checkbox" id="nav__open" class="nav__open switch" hidden>
	<label for="nav__open" class="overlay nav__overlay switch__zone" hidden></label>
	<label for="nav__open" id="nav__expand" class="icon-menu-vert icon_size_b icon_hover drown nav__expand" hidden></label>
	<nav class="nav menu" id="nav">
		<header class="nav__header menu__item">
			<a href="/" class="nav__link menu__link nav__logo title"><img src="/assets/i/logo.png" alt="Nullnyan"></a>
			<label for="nav__open" id="nav__close" class="nav__close"><i class="icon_size_b icon_hover_light icon-cancel"></i></label>
		</header>
		<main class="nav__main menu__item switch__zone"><?php X::$view->insert("kt/menu", true); ?></main>
		<footer class="nav__footer menu__item switch__zone">
            <?=X::$view->pagelist()?>
            <?php if(IO::granted()): ?>
            <a href="/io/out" class="nav__link menu__link"><i class="icon icon-logout"></i>&nbsp;Exit</a>
            <?php else: ?>
            <a href="/io/in" class="nav__link menu__link"><i class="icon icon-login"></i>&nbsp;Enter</a>
            <?php endif; ?>
        </footer>
	</nav>
	<main class="main">
		<header class="header"><h1 class="title"><?=X::$node?></h1></header>
		<aside class="message__wrapper"><?=Note::redeem()?></aside>
		<aside form><?php X::$view->postform(); ?></aside>
		<div class="page"><?php X::$view->content(); ?></div>
	</main>
</body></html>
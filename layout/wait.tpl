<!DOCTYPE html><html><head><title><?=$percent?>%</title>
	<meta charset="utf-8" />
	<meta name="author" content="anonymous" />
	<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
	<meta name="robots" content="noindex, nofollow" />
	<meta http-equiv="cache-control" content="private, max-age=10800, pre-check=10800" />
	<meta http-equiv="pragma" content="private" />
	<meta http-equiv="expires" content="<?=date(DATE_RFC822,strtotime("+2 day"))?>" />
	<meta name="theme-color" content="#111" />
    <base href="<?=FQDN?>" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<style type="text/css">
		@import '/assets/css/null.css';
		html,body{width:100%;height:100%;margin:0;padding:0}html{background:#111217;color:#bfbfbf}body{padding-top:96px;box-sizing:border-box}
        div{padding:16px 0 32px 15%;background:#0d0e12}em{display:block}output{font-size:26px}samp{font-size:52pt}
        div,em,output,samp{font-family:"Trebuchet MS",Helvetica,sans-serif}
        img{position:absolute;bottom:0;max-width:50%;max-height:50%;z-index:9}
		img.left{left:0} img.right{right:0}
        #wrap{position:absolute;bottom:0;left:0}
        audio{display:none} canvas{display:block}
	</style>
</head><body>
    <div>
        <output><?=$status?></output>
        <em><?=$message?></em>
    </div>
    <img class="left" src="/assets/i/qts/l/<?=mt_rand(1,52)?>.png" alt />
    <img class="right" src="/assets/i/qts/r/<?=mt_rand(1,73)?>.png" alt />
    <aside notes><?=Note::redeem()?></aside>
    <aside id="wrap"></aside>
    <script src="/assets/js/main.js" type="text/javascript"></script>
    <script src="/assets/js/flow.js?v=<?=TIME?>" type="text/javascript"></script>
    <script>document.addEventListener('DOMContentLoaded',function(e){(new Flow()).init(_.id('wrap'))},false)</script>
</body></html>
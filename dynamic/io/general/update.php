<?php if($update_available): ?>
Version <?=$version_available?> is available<br><small>(currently installed: v<?=X::VERSION?>)</small>
<a href="#" class="highlight" target="_blank">Что нового</a><br>
<footer>
    <?php if(IO::granted(true)): ?>
    <button class="button update__button button_primary"><a href="/io/assembly/update">Update</a></button>
    <?php else: ?>
    <button disabled class="button update__button button_primary">Update</button>
    <?php endif; ?>
    </button>
</footer>
<?php else: ?>
Up to date<br><small>(v<?=X::VERSION?>)</small>
<footer><button disabled class="button update__button button_primary">No update available</button></footer>
<?php endif; ?>
<header class="header"><h1 class="title">NXT General - System information</h1></header>
<aside class="message__wrapper"><?=Note::redeem()?></aside>
<div class="block block__grid">
    <fieldset>
        <legend>Hostname</legend>
        <span><?=IO::info__hostname()?></span>
    </fieldset>
</div>
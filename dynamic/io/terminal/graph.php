<header class="header"><h1 class="title">NXT Terminal - Graph</h1></header>
<aside class="message__wrapper"><?=Note::redeem()?></aside>
<div class="block block__grid">
    <fieldset>
        <legend>Say hello to all the worlds</legend>
        <form id="io_login" class="sign-in" action="/io/terminal/graph" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="echo">
            <div class="sign-in__item sign-in__submit">
                <button class="button button_primary" tabindex="1">
                    <span class="button__text">Send an ECHO</span>
                </button>
            </div>
            <cite>Sending an ECHO will notify the nodes from a public announcement list about your presence in the Noosphere network. 
            This will include your instance in this announcement list and making your existence visible.</cite>
        </form>
    </fieldset>
</div>
<div class="block block__grid">
    <fieldset>
        <legend>Connect to a world</legend>
        <form id="io_login" class="sign-in" action="/io/terminal/graph" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="echo">
            <div class="sign-in__item sign-in__submit">
                <button class="button button_primary" tabindex="1">
                    <span class="button__text">Send a CALL to</span>
                </button>
                <select class="button button_primary" tabindex="2">
                    <?=Terminal::graph__public_world_list('option')?>
                </select>
            </div>
            <cite>Sending a CALL request to a particular world of Noosphere will form a clear mutual bound between you.
            This allows to share some resources, make some portals and stabilizes the Noosphere in general. "Noosphere - connecting people".</cite>
        </form>
    </fieldset>
</div>
<div class="block block__grid">
    <fieldset>
        <legend>Close the connection</legend>
        <form id="io_login" class="sign-in" action="/io/terminal/graph" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="echo">
            <div class="sign-in__item sign-in__submit">
                <?php if(Terminal::graph__instance_has_connections()): ?>
                <button class="button button_primary" tabindex="1">
                    <span class="button__text">Disconnect from</span>
                </button>
                <select class="button button_primary" tabindex="2">
                    <?=Terminal::graph__connected_world_list('option')?>
                </select>
                <?php else: ?>
                <span>You currently don't have any open connections to other worlds.</span>
                <?php endif;?>
            </div>
            </fieldset>
            <cite>Disconnecting (from) a world will break the connection between both of you. 
            It will be impossible to recreate it until you revoke the connection denial.</cite>
        </form>
    </fieldset>
</div>
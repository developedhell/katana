<header class="header"><h1 class="title">NXT <i class="icon-right"></i> Access</h1></header>
<div class="nii blocks">
    <div class="nii row">
        <div class="nii four columns block">
            <fieldset class="nii block-space">
                <legend>Enter IO</legend>
                <form id="io_login" class="nii form" action="/io/in" method="post">
                    <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
                    <input type="hidden" name="io" value="in">
                    <div class="nii form labeled field">
                        <label>Agent numeric identity:</label>
                        <input type="text" class="input" name="user" minlength="1" maxlength="64" tabindex="1" placeholder="Username" autofocus required />
                    </div>
                    <div class="nii form labeled field">
                        <label>Agent password:</label>
                        <input type="password" class="input password__input" name="pass" minlength="8" maxlength="64" tabindex="2" placeholder="Password" required />
                    </div>
                    <div class="nii row block-space align right">
                        <button class="button" tabindex="3">Enter</button>
                    </div>
                </form>
            </fieldset>
        </div>
    </div>
</div>
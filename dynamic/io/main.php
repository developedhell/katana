<header class="header"><h1 class="title">NXT <i class="icon-right"></i> Main</h1></header>
<div class="nii blocks">
    <div class="nii row">
        <div class="column column-60"><div class="nii block">
            <fieldset class="nii block-space">
                <legend>Information</legend>
                <cite>Showing physical properties of the host machine.</cite>
                <span><i class="icon-calendar"></i>System uptime is <?=IO::info__uptime()?> (today is <?=IO::info__date()?>)</span>
                <span><i class='icon-sys-distribution'></i>Instance size is <?=IO::info__instance_size()?></span>
                <span><i class='icon-sys-distribution'></i>Disk space used <?=IO::info__disk_usage(ROOT)?> out of <?=IO::info__disk_total(ROOT)?></span>
                <span><i class='icon-sys-indicator'></i>Memory used <?=IO::info__memory_usage_used()?> out of <?=IO::info__memory_usage_total()?></span>
                <span>Running on the <code><?=IO::info__hostname()?></code> host</span>
            </fieldset>
            <div class="nii row block-space">
                <cite class="nii nine columns align left">You can get some additional information about system running by pressing "More".</cite>
                <span class="nii three columns align right"><a href="/io/generics/sysinf" class="button">More</a></span>
            </div>
        </div></div>
        <div class="column column-40"><div class="nii block">
            <fieldset class="nii block-space">
                <legend>IO panel</legend>
                <cite>Information about the current user logged in.</cite>
                <span>Remote-logged in as <code><?=IO::user__name_code()?></code></span>
                <span>Your operational level is <code><?=IO::user__right_level()?></code> and rank of <code><?=IO::user__rank()?></code></span>
                <span><a href="<?=IO::$API?>/in"><i class="icon-globe"></i>Switch to IO center</a></span>
            </fieldset>
            <div class="nii row block-space align right">
                <a class="button" href="/io/out"><i class="icon-logout"></i>End session</a>
            </div>
        </div></div>
    </div>
    <div class="nii row">
        <div class="column column-70"><div class="nii block">
            <fieldset class="nii block-space">
                <legend>General</legend>
                <cite>You can add or change some pages of your KT instance.</cite>
                <div class="row">
                    <div class="four columns">
                        <span><a href="/io/generics/edit/news"><i class="icon-edit-page"></i>News</a></span>
                        <span><a href="/io/generics/edit/todo"><i class="icon-edit-page"></i>Todo</a></span>
                    </div>
                    <div class="four columns">
                        <span><a href="/io/generics/edit/help"><i class="icon-edit-page"></i>Help</a></span>
                        <span><a href="/io/generics/edit/main"><i class="icon-edit-page"></i>Main page</a></span>
                    </div>
                    <div class="four columns">
                        <span><a href="/io/generics/edit/description"><i class="icon-edit"></i>Description</a></span>
                        <span><a href="/io/generics/edit/errors"><i class="icon-edit"></i>Errors</a></span>
                    </div>
                </div>
            </fieldset>
        </div></div>
        <div class="column column-30"><div class="nii block">
            <fieldset class="nii block-space">
                <legend>Cleanup</legend>
                <cite>Calling a garbage collector will result in deleting unused files or a session cleanup.</cite>
                <div class="row">
                    <div class="four columns">
                        <span><a href="/io/generics/clear/sessions"><i class="icon-sys-cleanup"></i>Sessions</a></span>
                    </div>
                    <div class="four columns">
                        <span><a href="/io/generics/clear/files"><i class="icon-sys-cleanup"></i>Files</a></span>
                    </div>
                    <div class="four columns">
                        <span><a href="/io/generics/clear/all"><i class="icon-sys-cleanup"></i>All</a></span>
                    </div>
                </div>
            </fieldset>
        </div></div>
    </div>
    <div class="nii row">
        <div class="column column-40"><div class="nii block">
            <fieldset class="nii block-space">
                <legend>Operations</legend>
                <cite><i class="icon-warning"></i>You must own admin-level privileges to make global operations.</cite>
                <span><a href="/io/generics/core/update"><i class="icon-arrow-up"></i>Check for updates</a></span>
                <span><a href="/io/generics/core/update"><i class="icon-sys-backup"></i>Make a backup</a></span>
                <span><a href="/io/generics/core/shutdown"><i class="icon-sys-power"></i>Shutdown the instance</a></span>
                <span><a href="/io/generics/core/reboot"><i class="icon-refresh"></i>Reboot the server</a></span>
            </fieldset>
        </div></div>
    </div>
</div>
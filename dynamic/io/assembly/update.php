<header class="header">
    <h1 class="title">Разделы</h1>
    <input type="checkbox" id="search__open" class="switch" hidden>
    <label for="search__open" class="drown search__open search__button switch__button">
        <i class="icon icon-search"></i>
    </label>
    <form class="search switch__zone" hidden>
        <button class="button search__button drown">
            <i class="icon icon-search"></i>
        </button>
        <input type="search" name="modlog__search" class="input search__input" placeholder="Поиск по таблице..." maxlength="128" tabindex="1" autofocus>
        <label for="search__open" class="search__button drown search__close">
            <i class="icon icon-close"></i>
        </label>
    </form>
</header>
<nav class="tabs">
    <a href="/t/posts.html" class="tabs__link">Посты</a>
    <a href="/t/bans.html" class="tabs__link">Баны</a>
    <a href="/t/boards.html" class="tabs__link tabs__link_active">Разделы</a>
    <a href="/t/users.html" class="tabs__link">Пользователи</a>
</nav>
<form class="table">
    <header class="table__header drown">
        <button class="button button_clear table__header-item">
            <i class="icon icon-add button_primary icon_size_b"></i>
        </button>
        <span class="menu__container table__header-item">
            <i class="icon icon_size_b icon-settings menu__icon button_secondary"></i>
            <menu class="menu menu_hidden popup">
                <div class="menu__item">
                    <button class="button button_clear menu__link">
                    Показывать по 20</button>
                    <button class="button button_clear menu__link">
                    Показывать по 50</button>
                    <button class="button button_clear menu__link">
                    Показывать по 100</button>
                </div>
            </menu>
        </span>
    </header>
    <div class="table__row drown">
        <div class="table__cell table__thin">
            <label class="label" title="Выделить всё" locked>
                <input type="checkbox" class="checkbox" name="#p8888">
                <i class="icon icon-checkbox"></i>
            </label>
        </div>
        <div class="table__cell">
            <button class="button button_clear table__title" title="Сортировать по">Раздел
                <i class="icon icon-arrow_drop_down table__sort"></i>
            </button>
        </div>
        <div class="table__cell">
            <button class="button button_clear table__title" title="Сортировать по">Заголовок</button>
        </div>
        <div class="table__cell">
            <button class="button button_clear table__title" title="Сортировать по">Скрытая</button>
        </div>
        <div class="table__cell">
            <button class="button button_clear table__title" title="Сортировать по">Страниц</button>
        </div>
        <div class="table__cell">
            <button class="button button_clear table__title" title="Сортировать по">Капча</button>
        </div>
        <div class="table__cell table__thin"></div>
    </div>
    <label class="table__row table__label">
        <div class="table__cell table__thin">
            <input type="checkbox" class="checkbox" name="#p8888">
            <i class="icon icon-checkbox"></i>
        </div>
        <div class="table__cell">/b/</div>
        <div class="table__cell">Бардак</div>
        <div class="table__cell">
            <input type="checkbox" class="checkbox" name="#p8888" disabled>
            <i class="icon icon-checkbox"></i>
        </div>
        <div class="table__cell">41</div>
        <div class="table__cell">
            <input type="checkbox" class="checkbox" name="#p8888" checked disabled>
            <i class="icon icon-checkbox"></i>
        </div>
        <div class="table__cell table__thin">
            <span class="menu__container">
                <i class="icon icon-more_horiz menu__icon post__icon"></i>
                <menu class="menu menu_hidden popup">
                    <div class="menu__item">
                        <button class="button button_clear menu__link">Настроить</button>
                        <button class="button button_clear menu__link">Спрятать</button>
                        <button class="button button_clear menu__link">Удалить</button>
                    </div>
                </menu>
            </span>
        </div>
    </label>
</form>
<footer class="footer pagination drown">
    <a href="" class="pagination__link">
        <i class="icon icon-prev"></i>
        Назад
    </a>
    <div class="pagination__pages">
        <a href="#" class="pagination__link">0</a>
        <a class="pagination__link pagination__link_active">1</a>
        <a href="#" class="pagination__link">2</a>
        <a href="#" class="pagination__link">3</a>
        <a href="#" class="pagination__link">4</a>
        <a href="#" class="pagination__link">5</a>
        <a class="pagination__link pagination__link_disable">...</a>
        <a href="#" class="pagination__link">32</a>
    </div>
    <a href="" class="pagination__link">Вперёд<i class="icon icon-next"></i></a>
</footer>
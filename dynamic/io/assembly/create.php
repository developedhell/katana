<header class="header"><h1 class="title">Create new node</h1></header>
<form id="io_login" class="sign-in" action="/io/login" method="post" enctype="multipart/form-data">
    <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
    <input type="hidden" name="act" value="in">

    <fieldset>
        <legend><abbr title="Short alphanumeric reference to the created node (e.g. /b, /x, /0). Literal must not exist yet in the W/Net space.">Node literal</abbr></legend>
        <input type="text" class="input" name="literal" minlength="1" maxlength="16" tabindex="1" placeholder="Literal" autofocus required />
    </fieldset>

    <fieldset>
        <legend><abbr title="Node name representing and descripting the point of this space (e.g. /v for 'Vendetta').">Node name</abbr></legend>
        <input type="text" class="input" name="name" minlength="1" maxlength="32" tabindex="2" placeholder="Name" required />
    </fieldset>

    <fieldset>
        <legend><abbr title="Various limitations and/or extensions applied to this node space (check to enable)">Characteristics</abbr></legend>
        <div class="">
            <label><input type="checkbox" name="seal[]" value="post" /> Posting is allowed <abbr title="You can change this later"><i class="icon icon-info"></i></abbr></label>
            <label><input type="checkbox" name="seal[]" value="name" /> Names and tripcodes</label>
            <label><input type="checkbox" name="seal[]" value="file" /> File uploading</label>
            <label><input type="checkbox" name="seal[]" value="link" /> URL processing</label>
            <label><input type="checkbox" name="seal[]" value="hide" /> Not listed <abbr title="This means node will not be visible in public lists and in /all feed"><i class="icon icon-info"></i></abbr></label>
            <label><input type="checkbox" name="seal[]" value="custom" /> Enable customization</label>
        </div>
    </fieldset>

    <fieldset>
        <legend><abbr title="File types allowed to upload (note that this have sense only if file uploading is enabled in characteristics)">File types</abbr></legend>
        <label><input type="checkbox" name="file[]" value="img" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
        <label><input type="checkbox" name="file[]" value="aud" /> <abbr title="Includes .MP3, .OGG, .FLAC, .WebA formats">Audio</abbr></label>
        <label><input type="checkbox" name="file[]" value="vid" /> <abbr title="Includes .MP4 and .WebM formats">Video</abbr></label>
        <label><input type="checkbox" name="file[]" value="fla" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
        <label><input type="checkbox" name="file[]" value="txt" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
        <label><input type="checkbox" name="file[]" value="arc" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
        <label><input type="checkbox" name="file[]" value="doc" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
        <label><input type="checkbox" name="file[]" value="add" /> <abbr title="Includes .JPG, .PNG, .GIF, .BMP and .WebP formats">Images</abbr></label>
    </fieldset>

    <fieldset>
        <legend><abbr title="">Privilege set</abbr></legend>
    </fieldset>

    <div class="sign-in__item sign-in__submit">
        <button class="button button_primary" tabindex="3">
            <span class="button__text">Enter</span>
        </button>
    </div>
</form>
<header class="header">Add existing node</h1></header>
<form id="io_login" class="sign-in" action="/io/login" method="post" enctype="multipart/form-data">
    <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
    <input type="hidden" name="act" value="in">
    <div class="sign-in__item sign-in__login">
        <input type="text" class="input" name="login" minlength="1" maxlength="64" tabindex="1" placeholder="Username" autofocus required />
    </div>
    <div class="password sign-in__item sign-in__password">
        <input type="password" class="input password__input" name="password" minlength="8" maxlength="64" tabindex="2" placeholder="Password" required />
    </div>
    <div class="sign-in__item sign-in__submit">
        <button class="button button_primary" tabindex="3">
            <span class="button__text">Enter</span>
        </button>
    </div>
</form>
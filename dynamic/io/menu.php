<details class="nav__details nav__board">
    <summary class="nav__link menu__link nav__summary">Nodes<i class="icon icon_size_b icon-add nav__summary-icon"></i></summary>
    <?php insert("nodelist"); ?>
</details>
<details class="nav__details nav__pages">
    <summary class="nav__link menu__link nav__summary">Pages
        <i class="icon icon_size_b nav__summary-icon"></i>
    </summary>
    <?php insert("pagelist"); ?>
</details>
<?php if(IO::granted()): ?>
<details class="nav__details" open>
    <summary class="nav__link menu__link nav__summary">Control panel
        <i class="icon icon_size_b nav__summary-icon"></i>
    </summary>
    <hr />
    <?php if(IO::granted(true)): ?>
    <a href="/io/assembly" class="nav__link menu__link">Assemblage</a>
    <a href="/io/commands" class="nav__link menu__link">Orders</a>
    <a href="/io/firewall" class="nav__link menu__link">Firewall</a>
    <a href="/io/moderate" class="nav__link menu__link">Moderation</a>
    <hr />
    <a href="/io/worldnet" class="nav__link menu__link">IO network</a>
    <?php else: ?>
    <a href="/modlog" class="nav__link nav__link_active menu__link">Modlog</a>
    <?php endif;?>
</details>
<?php endif; ?>
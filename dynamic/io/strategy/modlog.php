<header class="header">
	<h1 class="title">Moderation log</h1>
</header>
<form class="modlog">
	<div class="search">
		<button class="button search__button">
			<i class="icon-search"></i>
		</button>
		<input type="search" name="modlog__search" class="input search__input" placeholder="Search" tabindex="1">
	</div>
	<main>
		<table>
			<tr class="table__row table__header">
				<th class="table__cell"><input type="checkbox" class="checkbox" name="modlog" title="Выделить все" locked></th>
				<th class="table__cell"><a href="#" class="table__link">Post №<i class="icon-down-cut"></i></a></th>
				<th class="table__cell"><a href="#" class="table__link">Status</a></th>
				<th class="table__cell table__wide"><a href="#" class="table__link">Reason</a></th>
				<th class="table__cell"><a href="#" class="table__link">Date</a></th>
				<th class="table__cell"><a href="#" class="table__link">Ends</a></th>
				<th class="table__cell"><a href="#" class="table__link">Issued by</a></th>
				<th class="table__cell"></th>
			</tr>
			<?php IO\Moderate::log(); ?>
		</table>
	</main>
	<footer class="table__footer">Show by
		<select class="select modlog__select">
			<option selected>16</option>
			<option>32</option>
			<option>64</option>
			<option>128</option>
		</select>
	</footer>
</form>
<footer class="pagination">
	<a href="/io/moderate/log/prev" class="pagination__link"><i class="icon-left"></i></a>
	<div class="pagination__pages"><?php IO\Moderate::pagination_log(); ?></div>
	<a href="/io/moderate/log/next" class="pagination__link"><i class="icon-right"></i></a>
</footer>
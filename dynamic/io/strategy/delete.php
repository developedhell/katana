<header class="header"><h1 class="title">NXT Strategy - Deletion</h1></header>
<aside class="message__wrapper"><?=Note::redeem()?></aside>
<div class="block block__grid">
    <fieldset>
        <legend>Point out</legend>
        <form id="io_login" class="sign-in" action="/io/strategy/delete" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="solo">
            <div class="form-field form-field_line">
                <span>Delete the post</span>
                <input placeholder="#xxx" name="post" type="number" min="1" required />
            </div>
            <div class="form-field form-field_submit">
                <button type="submit" class="button button_primary" tabindex="1">
                    <span class="button__text">Delete</span>
                </button>
            </div>
            <cite>Deleting a wire ("thread") will result in deletion of all the posts which belonged to it.</cite>
        </form>
    </fieldset>
</div>
<div class="block block__grid">
    <fieldset>
        <legend>Mass effect</legend>
        <form id="io_login" class="sign-in" action="/io/strategy/delete" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="many">
            <div class="form-field form-field_line">
                <span>Delete multiple posts from</span>
                <input placeholder="#xxx" name="from" type="number" min="1" required />
                <span>to</span>
                <input placeholder="#yyy" name="to" type="number" min="1" required />
            </div>
            <div class="form-field form-field_block">
                <label for="wireonly"><input type="checkbox" name="wireonly" id="wireonly"/> Delete if op-post only</label>
            </div>
            <div class="sform-field form-field_submit">
                <button type="submit" class="button button_primary" tabindex="1">
                    <span class="button__text">Delete</span>
                </button>
            </div>
            <cite>Deleting only wires ("threads") will leave other posts in other wires untouched (but be sure about posts which belongs to these deleted wires).</cite>
        </form>
    </fieldset>
</div>
<div class="block block__grid">
    <fieldset>
        <legend>Key removal</legend>
        <form id="io_login" class="sign-in" action="/io/strategy/delete" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="text">
            <div class="form-field form-field_block">
                <span>Delete all posts containing exactly the following text:</span>
                <textarea name="text" placeholder="text"></textarea>
            </div>
            <div class="form-field form-field_line">
                <span>in past interval from</span>
                <input type="datetime" name="datefrom" />
                <span>to</span>
                <input type="datetime" name="dateto" />
            </div>
            <div class="form-field form-field_submit">
                <button type="submit" class="button button_primary" tabindex="1">
                    <span class="button__text">Delete</span>
                </button>
            </div>
            <cite>You can either specify only text (will search by text), or either only interval (will search by date), or both.</cite>
        </form>
    </fieldset>
</div>
<div class="block block__grid">
    <fieldset>
        <legend>I see dead people</legend>
        <form id="io_login" class="sign-in" action="/io/strategy/delete" method="post" enctype="multipart/form-data">
            <input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
            <input type="hidden" name="io" value="many">
            <div class="form-field form-field_line">
                <span>Delete all posts of particular user by this</span>
                <select name="by">
                    <option>tripcode</option>
                    <option>ip-hash</option>
                    <option>digisign</option>
                    <option>black spot</option>
                </select>
                <span>:</span>
            </div>
            <div class="form-field form-field_block">
                <textarea name="text" placeholder="text"></textarea>
            </div>
            <div class="form-field form-field_submit">
                <button type="submit" class="button button_primary" tabindex="1">
                    <span class="button__text">Delete</span>
                </button>
            </div>
            <cite>This will reflect on your karma. Be sure your sacrifices fits in your aim.</cite>
        </form>
    </fieldset>
</div>
<header><?=[
	'Fastest cat in the galaxy.',
	'The king is Nyan, God bless Cat king!',
	'Of the Nullchan Confederation.',
	'Running in the 2090`s.',
	'Welcome aboard, %catname%!',
	'Your classic orbital bar.',
	'Space Exploration Echelon.',
	'Powered by Schrödinger.',
	'Poglad\' kota, cyka!',
	'Hakase Laboratories.',
	'The Cat Syndicate.',
	'Some serious cat business.',
	'Nullchan\'s NERV.',
	'Mein Nyan.',
	'Pomidorka 9000.',
	'United as one. Divided by zero.',
	'The cat is alive, all\'s right with the Nullnyan.',
	'Enyargy, unyaty, brothermeow, nyancore.',
	'NYAGRIZOLICH!',
	'Spacecat-X by Nylon Myask.',
	'Cosmonyauts in t3h open space.',
	'Yes we nyan!',
	'Cat the Longest.',
	'Цифра любимая моя.',
	'Godspeed you Nyan the Stranger ~',
	'The cat is (not) dead.',
	'Vodka, whores &amp; Internets.',
	'Per aspera ad astra.',
	'Star CATS.',
	'We declare peace.',
	'SSV Normyandy SR-42.',
	'&Omega;'
][random_int(0,31)]?></header>
<main noselect>
	<aside class="first"><h3>Nyan!</h3>~ Dear Anonymous,<br/>Спасибо, что ты есть.<br/>Лучшие кошки галактики, отважные бойцы с коварной и жестокой системой, продолжают лететь навстречу приключениям в новом году. Что их встретит и куда они прилетят, какие планеты повидают, лучи каких солнц будут ласкать их шёрстку и вода каких океанов будет стекать со скафандров бесстрашных исследователей - кто знает. Но главное остаётся у нас в сердцах, навеки - мы едины, ибо кто, кроме нас?<br/>Оставайтесь собой и до встречи!<!--em>Нижеподписавшиеся: Anonymous K, Karen Kujō-sama, 510. При поддержке KI, RA, Синдиката и Нигматы.</em--></aside>
	<aside class="last"><div class="first" style="background-image:url('/assets/i/2018/<?=random_int(1,16)?>.png')"></div><div class="last"></div></aside>
	<menu>
		<a href="/b">BARDUCK</a>
		<a href="/t">THEMATIC</a>
		<a href="/k">KONTENT</a>
		<a href="/int">INTERNATIONAL</a>
		<a href="/e">POLYGON</a>
		<a href="/n">NULLNYAN</a>
	</menu>
</main>
<audio autoplay loop><source src="/assets/media/2018/<?=[
	'avto-88',
	'bezdna',
	'BEZHOGNM',
	'clokx',
	'es_rappelt_im_karton',
	'fuck the flowers',
	'lavanda',
	'odyssey',
	'off_the_charge',
	'rezero',
	'sally',
	'ttgl',
	'wunschpunsch',
	'ファースト・デイト',
	'唯蟲',
	'intention'
][random_int(0,14)]?>.ogg" type="audio/ogg"></audio>
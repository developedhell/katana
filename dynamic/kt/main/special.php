<aside bckg unlocked id="bckgDefault" style="background-image:url('/assets/i/specials/witchhouse.jpg')"></aside>
<aside bckg locked id="bckgContainer"></aside>
<input type="checkbox" id="menu" name="menu" />
<header noselect><a logo href="/"></a><h1 text>Ｎ　　　Ｙ　　　　Ａ　　　Ｎ</h1><em desc><?=Katana::motto()?></em></header>
<main noselect><label for="menu"></label><div id="interact"></div></main>
<footer noselect><label for="menu"><i class="icon-cancel"></i></label>
	<a href="/b">BARDUCK</a>
	<a href="/t">THEMATIC</a>
	<a href="/k">KONTENT</a>
	<a href="/int">INTERNATIONAL</a>
	<a href="/e">POLYGON</a>
	<a href="/n">NULLNYAN</a>
</footer>
<audio autoplay loop><source src="/assets/media/ambience/specials/<?=[
	'carpenter','heavenly','ladansmacabre','ostorozhno_krutoj_trap','saёns','workout','wunschpunsch'
][random_int(0,6)]?>.ogg" type="audio/ogg"></audio>
<script type="text/javascript">
	var interact=_.id('interact'),
		iw=null,ih=null,
		background={
		container:_.id('bckgContainer'),
		default:_.id('bckgDefault'),
		loader:new Image(),
		shakalize:function(event){
			_.w=document.documentElement.clientWidth||document.body.clientWidth;
			_.h=document.documentElement.clientHeight||document.body.clientHeight;
			/*
1920x1080 : 0,5625	16:9	wp 
1024x768  : 0,75	4:3		display
1080x1920 : 1,(7)	9:16	mobile
			*/
			var waspect=_.h/_.w, iaspect=ih/iw;
			console.log(`waspect ${waspect} / iaspect ${iaspect}`);
			background.container.style.backgroundSize = (waspect > iaspect ? "auto 100%" : "100% auto");
		}
	};
	background.loader.onload=function(event){
		iw=this.width; ih=this.height;
		background.container.style.backgroundImage='url('+background.loader.src+')';
		background.shakalize();
		background.container.classList.add('ready');
	};
	window.addEventListener('resize',background.shakalize,false);
	(function(){
		background.loader.src='/assets/i/specials/witchhouse.jpg';
	})();
</script>
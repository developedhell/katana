<div class="postform">
    <input type="checkbox" id="open-postform" class="switch" hidden>
    <label for="open-postform" class="button postform__open"><?=(X::$wire?"Answer":"New thread")?></label>
	<div class="postform__container normal switch__zone" draggable>
		<form name="postform" id="postform" class="postform__form" action="/<?=X::$node?>/create" method="post" enctype="multipart/form-data" draghandler>
			<input type="hidden" name="MAX_FILE_SIZE" value="<?=(X::$conf['kt']['filesize'])?>" />
			<input type="hidden" name="rl" value="<?=X::$wire?>" />
			<input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
			<input type="hidden" name="pass" value="<?=htmlspecialchars(X::$opts['PASS'])?>" />
			<input type="hidden" name="noko" value="<?=(X::$wire ? 1 : 0)?>" />
			<header class="postform__header postform__item" draghandler>
				<div class="postform__left-side">
					<span class="postform__title">New thread</span>
					<label class="label postform__label postform__sage" title="Don't bump">
						<input id="postform__sage-input" type="checkbox" class="checkbox_hidden" name="sage" hidden />
						<i class="icon icon_size_b icon-sage icon_hover postform__sage-icon"></i>
					</label>
				</div>
                <div class="postform__right-side">
                    <?php X::$view->helper(); ?>
                    <label for="open-postform" class="postform__label">
                        <i class="icon icon-cancel icon_hover icon_size_b postform__close"></i>
                    </label>
                </div>
			</header>
			<main class="postform__main">
				<div class="postform__item">
					<div class="postform__left-side postform__subject">
                        <?php if(X::$node == "all" || X::$node == "fav"): ?>
						<label class="select label postform__boards">
							<select class="select__input" name="lt"><?=X::$view->nodelist('option')?></select>
							<i class="icon icon-down select__icon drown"></i>
                        </label>
                        <?php else: ?>
                        <input type="hidden" name="lt" value="<?=X::$node?>" />
                        <?php endif; ?>
						<input type="text" class="input postform__input postform__subject-input" name="subj" placeholder="Subject" maxlength="64" tabindex="1" />
					</div>
					<div class="postform__right-side password postform__password">
						<input type="password" id="postform__password-input" class="input postform__input password-input" name="pass" placeholder="Password" maxlength="64" />
					</div>
				</div>
				<textarea id="postform__textarea" class="textarea postform__textarea" name="text" maxlength="<?=X::$conf['kt']['textsize']?>" tabindex="2" autofocus></textarea>
				<kbd></kbd>
				<footer class="postform__footer postform__item">
					<?php if(X::$mode&Katana\Seal::FILE): ?>
					<div class="postform__left-side postform__file">
						<button type="button" id="postform__file-label" name="filefake" class="icon button-clear" locked>
							<i class="icon icon-attach"></i>
						</button>
					</div>
					<?php endif; ?>
					
					<?php if(X::$conf['kt']['postcode']): ?>
					<div class="postform__left-side postform__captcha"><input type="text" name="code" placeholder="CAT/CHA" autocomplete="off" /></div>
					<?php endif; ?>
					<div class="postform__right-side">
						<button id="submit" type="submit" name="send" tabindex="4">
							<span class="button__text">Send</span>
						</button>
					</div>
				</footer>
			</main>
			<footer class="postform__footer-preview">
				<div id="postform__file-preview" locked></div>
				<div id="postform__file-collection">
					<input type="file" id="postform__file0" name="files[]" class="file-input postform__file-input" />
					<input type="file" id="postform__file1" name="files[]" class="file-input postform__file-input" />
					<input type="file" id="postform__file2" name="files[]" class="file-input postform__file-input" />
					<input type="file" id="postform__file3" name="files[]" class="file-input postform__file-input" />
				</div>
			</footer>
		</form>
	</div>
</div>
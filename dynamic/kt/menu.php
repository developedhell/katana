<input type="checkbox" id="nav__open" class="nav__open" checked hidden />
<label for="nav__open" id="nav__expand" class="icon-menu-vert icon_hover drown nav__expand checkbox-off"></label>
<nav class="nav menu checkbox-on" id="nav">
	<header class="nav__header">
		<a href="/" class="nav__link nav__logo"><img src="/assets/i/nightly.png" alt="Katana"></a>
		<label for="nav__open" id="nav__close" class="nav__close"><i class="icon_size_b icon_hover_light icon-cancel"></i></label>
	</header>
	<main class="nav__main menu__item switch__zone"><?php X::$view->nodelist(); ?></main>
	<footer class="nav__footer menu__item switch__zone">
		<?php X::$view->pagelist(); ?>
		<?php if(IO::granted()): ?>
		<a href="/io/out" class="nav__link menu__link"><i class="icon icon-logout"></i>&nbsp;Exit</a>
		<?php else: ?>
		<a href="/io/in" class="nav__link menu__link"><i class="icon icon-login"></i>&nbsp;Enter</a>
		<?php endif; ?>
	</footer>
</nav>
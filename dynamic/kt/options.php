<?php $check = function($value){ return isset(X::$opts[$value]) && X::$opts[$value] ? 'value="1" checked' : 'value="0"'; }; ?>
<section><form name="options" id="options" method="post" action="/options/save">
	<input type="hidden" name="token" value="<?=NCC::token(session_id())?>" />
	<input type="hidden" name="layt" value="n2o" />
	<div><span>Environment: </span>
		<select name="schm">
			<?php
				foreach(decode('assets/data/colorscheme') as $theme => $themedata){
					echo "<option "
						.(X::$opts['schm'] == $theme ? "selected " : "")
						."value='{$theme}' title='{$themedata['tone']}'>{$themedata['name']}</option>";
				}
			?>
		</select>
	</div>
	<div><label class="label">
		<input toggle type="checkbox" class="checkbox" name="smth" <?=$check('smth')?> />
		<i class="checkbox-on icon-check-in"></i><i class="checkbox-off icon-check"></i><span>CSS-3 smoothness</span>
	</label></div>
	<div><label class="label">
		<input toggle type="checkbox" class="checkbox" name="play" <?=$check('play')?> />
		<i class="checkbox-on icon-check-in"></i><i class="checkbox-off icon-check"></i><span>Gallery and custom players</span>
	</label></div>
	<div><label class="label">
		<input toggle type="checkbox" class="checkbox" name="infi" <?=$check('infi')?> />
		<i class="checkbox-on icon-check-in"></i><i class="checkbox-off icon-check"></i><span>Infinite scroll</span>
	</label></div>
	<div><label class="label">
		<input toggle type="checkbox" class="checkbox" name="kbdi" <?=$check('kbdi')?> />
		<i class="checkbox-on icon-check-in"></i><i class="checkbox-off icon-check"></i><span>Keyboard hotkeys</span>
	</label></div>
	<div><label class="label">
		<input toggle type="checkbox" class="checkbox" name="ajax" <?=$check('ajax')?> />
		<i class="checkbox-on icon-check-in"></i><i class="checkbox-off icon-check"></i><span>AJAX posting, etc.</span>
	</label></div>
	<div><button class="button button_primary" type="submit" name="send" value="save"><i class="icon-save"></i>Save</button></div>
</form></section>
<?/*
	"nyanon":{"tone":"dim gray with strong turquoise","name":"Nyanon"},
	"heon":{"tone":"soft green tones with pastel beige and white","name":"Heon"},
	"nightfire":{"tone":"forest green with sky blue and wooden brown","name":"Nightfire"},
	"twilight":{"tone":"royal purple on dark base with reds and whites","name":"Twilight"},
	"shinonome":{"tone":"dark-gray, wheat, white and soft turquoise","name":"Shinonome"},
	"arcticice":{"tone":"light-gray with turquoise and marine","name":"Arctic Ice"},
	"moonlight":{"tone":"steel sky monotones with yellow","name":"Moonlight"},
	"mahoushoujo":{"tone":"soft rose and white","name":"Mahou Shoujo"}
*/?>
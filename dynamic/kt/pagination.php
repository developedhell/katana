<?php if($total > 1 && X::$page >= 1): ?>
<a class="button" href='/<?=$it_address?>/<?=(X::$page-1)?>'><i class='icon-left'></i>Previous</a>
<?php else: ?>
<a class="button" disabled><i class='icon-left'></i>Previous</a>
<?php endif;?>
<div class='pagination__pages'>
<?php
    $divider = "<a class='pagination__link pagination__link_disable'>...</a>";
    $format = function($i) use ($it_address){
        return "<a href='/{$it_address}/{$i}' class='pagination__link".($i == X::$page ? " pagination__link_active" : "")."'>{$i}</a>";
    };
    echo $format(0);
    if($total > 1){
        if($total > 10){ # 6+ pages
            $divider = '...';
            $median = X::$page;
            $from = $median - 2;
            $to = $median + 2;
            if($from < 1){
                $from = 1;
                $median = $from + 2;
                $to = $from + 4;
            }
            if($to > $total - 1){
                $to = $total - 1;
                $median = $to - 2;
                $from = $to - 4;
            }
            if($from > 2){ echo $divider; }
            for($i = $from; $i < $to; $i++){ echo $format($i); }
            if($to < $total - 1){ echo $divider; }
        } else {
            if($total > 2){
                for($i = 1; $i < $total - 1; $i++){ echo $format($i); }
            }
        }
        echo $format($total - 1);
    }
?>
</div>
<?php if($total > 1 && X::$page < $total - 1): ?>
<a class="button" href='/<?=$it_address?>/<?=(X::$page+1)?>'>Next<i class='icon-right'></i></a>
<?php else: ?>
<a class="button" disabled>Next<i class='icon-right'></i></a>
<?php endif;?>
<footer class="footer">
	<div class="nii row footer-space">
		<div class="nii align left nine columns">NXT control panel is delivered as a remote IO console and is a product of <i class="icon-p2p"></i> NC institute and is powered by <i class="icon-hakase"></i> Hakase Technology.</div>
		<div class="nii align right three columns">2012&nbsp;&mdash;&nbsp;<i class="icon-infinity"></i></div>
	</div>
</footer>
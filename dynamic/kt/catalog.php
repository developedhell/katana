<?php

if(null===X::$cata) throw new Note('Catalist not ready');
elseif(empty(X::$cata)) echo "<h5>Nothing to display yet.</h5>";
else {
	$offset = X::$page * 12;
	$limit = $offset + 12;
	$path = 'content/'.X::$node;
	$go = function() use ($offset,$limit,$path){
		foreach(array_slice(X::$cata,$offset,$limit,true) as $id => $thread)
			if(file_exists($f = $path."/head/{$id}.htm"))
				yield $id;
			else new Note("Could not find head #{$id}", Note::WARNING);
	};
	foreach($go() as $id){
		X::$temp = explode('.',X::$cata[$id][1]);
		echo "<article><samp>".X::$temp[0]." posts (".X::$temp[1]." files, ".X::$temp[2]." links)"
			."<a href='/".X::$node."/thread/{$id}'>Open <i class='icon-right'></i></a></samp><section id='T{$id}'>"
			.file_get_contents($path."/head/{$id}.htm")
			."<hr/></section></article>";
	}
}
<?php

if(empty(X::$cata)){
    echo "<h4>No threads were found on this node.</h4>";
} else {
	$offset = X::$page * X::$conf['kt']['maxthrds'];
	$path = 'static/node/'.X::$node;
	$go = function() use ($offset,$path){
		foreach(array_slice(X::$cata,$offset,X::$conf['kt']['maxthrds'], true) as $id => $thread)
			if(file_exists($f = $path."/chunk/{$id}.htm"))
				yield $id;
			else new Note("Could not find chunk #{$id}",Note::WARNING);
    };
	foreach($go() as $id){
		echo "<section class='thread'>";
		readfile($path."/chunk/{$id}.htm");
		echo "</section>";
	}
}

?>
<aside class="banner" hidden><?=X::$view->banner()?></aside>
<aside class="disclaimer"><?php X::$view->disclaimer(); ?></aside>
<footer class="pagination drown"><?=Katana::pagination_index()?></footer>
<i class="icon icon-info icon_size_b postform__help-icon"></i>
<ul class="postform__help popup popup_has-tail">
	<li>
		<a href="/help" target="_blank" class="highlight">Help and rules</a>
	</li>
	<li>Supported
		<a href="/<?=X::$node?>/info#filetype" target="_blank" class="highlight">file types</a>
	</li>
	<li>Upload limit is <?=Chrona::bytes(intval(X::$conf['kt']['filesize']))?></li>
	<li>Maximum message length is <?=X::$conf['kt']['textsize']?> symbols</li>
	<li>Formatting <a href="/help#format" target="_blank" class="highlight">rules</a></li>
</ul>
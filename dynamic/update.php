<?php exit;

function translate_file_data($node_id, $literal, $tid, $pid){
	$file = SQL::kq("SELECT `file`, `orig`, `info` FROM `node_{$literal}` WHERE `id` = {$pid}", SQL::ONE | SQL::KEY);
	if(empty($file["file"])) return;

	$d_node = $node_id;
	$d_wire = $tid;
	$d_post = $pid;
	$d_name = $file['file'];
	$d_orig = $file['orig'];
	$d_size = 0;
	$d_info = $file['info'];

	if(preg_match('/[\w\s]+\((.+,\s)?[\d\.]+\s(Ki|Mi)?B\)/', $d_orig)){
		$temp = $d_orig;
		$d_orig = $d_info;
		$d_info = $temp;
	}

	if(preg_match('/\w+\s\(.+,\s([\d\.]+)\s(\w+)\)/', $d_info, $matches)){
		if(isset($matches[1], $matches[2])){
			$d_size = floatval($matches[1]);
			if($matches[2] == "KiB") $d_size = ceil($d_size * 1024);
			if($matches[2] == "MiB") $d_size = ceil($d_size * 1024 * 1024);
			$d_size = intval($d_size);
		}
	}

	if(preg_match('/title=\'(.+)\'/', $d_orig, $matches)){ # <icon audio title='AiB - Мертвы'></icon>
		if(!empty($matches[1])){
			$d_orig = $matches[1];
		}
	}

	if(preg_match('/^(\d{2}:\d{2})?.*(\s\w+,)+\s(.+\..+)$/', $d_orig, $matches)){ # 04:34, mp3, vp8, Datura - Feel frei.mp3
		if(!empty($matches[3])){
			$d_orig = $matches[3];
		}
		if(!empty($matches[1])){
			$d_info = $matches[1];
		}
	}

	if(preg_match('/\((\d+x\d+)\s?,/', $d_info, $matches)){ # JPG (123x983, 12.18 KiB)
		if(isset($matches[1])){
			$d_info = $matches[1];
		}
	}

	if(preg_match('/\w+\s\((x,\s)?(\d+[x:]\d+)[,\)]/', $d_info, $matches)){ # MP3 (00:24, 537.39 KiB)
		if(isset($matches[2])){
			$d_info = $matches[2];
		}
	}

	if(preg_match('/^(.+)\s\(/', $d_info, $matches)){ # ZIP archive (12.12 KiB)
		if(!empty($matches[1])){
			$d_info = $matches[1];
		}
	}
	

	$d_name = strtolower($d_name);
	$d_orig = addslashes($d_orig);
	if(empty($d_orig)) $d_orig = $d_name;

	$d_name = strip_tags($d_name);
	$d_orig = strip_tags($d_orig);
	$d_info = strip_tags($d_info);

	if(preg_match('/class=.icon/', $d_name)){ echo "d_name:"; dump($d_name); }
	if(preg_match('/class=.icon/', $d_orig)){ echo "d_orig:"; dump($d_orig); }
	if(preg_match('/class=.icon/', $d_info)){ echo "d_info:"; dump($d_info); }

	/*if(!preg_match('/^\d+[x:]\d+$/', $d_info)){
		var_dump($file['file'], $file['orig'], $file['info']);
		echo '<hr/>';
		var_dump($d_name, $d_orig, $d_info);
		exit;
	}*/

	$q = "INSERT INTO `files` (`node`, `wire`, `post`, `name`, `orig`, `size`, `info`) VALUES ({$d_node}, {$d_wire}, {$d_post}, '{$d_name}', '{$d_orig}', {$d_size}, '{$d_info}')";
	SQL::q($q);
}

$literal = "rf";
if(empty(X::$tree[$literal])){
	exit('fuck');
}
$node_id = X::$tree[$literal]['id'];
#dump($node_id);

$threads = SQL::kq("SELECT `id` FROM `node_{$literal}` WHERE `rl` = 0", SQL::ARR | SQL::KEY);
if(!empty($threads)) foreach($threads as $thread){
	$tid = intval($thread['id']);
	$posts = SQL::kq("SELECT `id` FROM `node_{$literal}` WHERE `rl` = {$thread['id']} ", SQL::ARR | SQL::KEY);
	if(!empty($posts)) foreach($posts as $post){
		$pid = intval($post['id']);
		translate_file_data($node_id, $literal, $tid, $pid);
	}
	translate_file_data($node_id, $literal, $tid, $tid);
}
